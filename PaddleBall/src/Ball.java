/* 
* CSCI1301.java 
* Author: Kosi Uzodinma 
* Submission Date: April 26, 2014
* 
* Purpose: A brief paragraph description of the 
* program. What does it do? How does it do it? 
* 
* Statement of Academic Honesty: 
* 
* The following code represents my own work. I have neither 
* received nor given inappropriate assistance. I have not copied 
* or modified code from any source other than the course webpage 
* or the course textbook. I recognize that any unauthorized 
* assistance or plagiarism will be handled in accordance with 
* the University of Georgia's Academic Honesty Policy and the 
* policies of this course. I recognize that my work is based 
* on a programming project created by the Department of 
* Computer Science at the University of Georgia. Any publishing 
* of source code for this project is strictly prohibited without 
* written consent from the Department of Computer Science. 
*/ 
public class Ball {

	//Declare instance variables
	private int x;
	private int y;
	private int color;
	private int radius;
	private int speed;
	private int xDir;
	private int yDir;
	private int courtWidth;
	private int courtHeight;
	//Constructor to assign instance variables
	public Ball(int x, int y,  int speed, int xDir, int yDir, int courtWidth, int courtHeight) 
	{
		//Assigned color between 0-6
		color = (int)Math.random()*6;
		//radius is a number between 15-20
		this.radius = (int)(Math.random()*5)+15;
		//if statement to make sure valid inputs are made for the following variables 
		if( ((xDir > 1) || (xDir < -1)) || ((yDir > 1) || (yDir < -1)) || (speed<1) || (speed>5) )
		{
			this.xDir = 1;
			this.yDir = 1;
			this.speed = 1;
		}
		else
		{
			this.xDir = xDir;
			this.yDir = yDir;
			this.speed = speed;
		}
		//decision statements to make sure valid input was passed in
		if( (courtWidth < 0) || (courtHeight < 0) || (courtWidth <= (this.radius*2)) || (courtHeight <= (this.radius*2)) )
		{
			this.courtWidth = 700; 
			this.courtHeight = 300;
		}
		else
		{
			this.courtHeight = courtHeight;
			this.courtWidth = courtWidth;
		}
		//Decision statements to make sure valid input was passed in
		if ( (x < 0) || (y < 0) || (x > this.courtWidth)|| (y > this.courtHeight)  )
		{
			this.x = this.courtWidth/2;
			this.y = this.courtHeight/2;
		}
		else
		{
			this.x = x;
			this.y = y;
		}
		
	}
	//Another constructor with less variables to pass in
	public Ball(int x, int y, int radius, int courtWidth, int courtHeight) 
	{
		//Assigned the four variables to the default values.
		color = (int)(Math.random()*6);
		xDir = 1;
		yDir = 1;
		speed = 1;
		//Made sure all that all the variables had valid input for the program to run correctly
		if( (courtWidth < 0) || (courtHeight < 0) || (courtWidth <= (this.radius*2)) || (courtHeight <= (this.radius*2)) )
		{
			this.courtWidth = 700; 
			this.courtHeight = 300;
		}
		else
		{
			this.courtHeight = courtHeight;
			this.courtWidth = courtWidth;
		}
		if ( (x < 0) || (y < 0) || (x > this.courtWidth)|| (y > this.courtHeight) )
		{
			this.x = this.courtWidth/2;
			this.y = this.courtHeight/2;
		}
		else
		{
			this.x = x;
			this.y = y;
		}
		
		if (radius < 15)
		{
			this.radius = 15;
		}
		else
		{
			this.radius = radius;
		}
		
	
	}
	//getter method for radius
	public int  getRadius() 
	{
		return radius;
	}
	//getter method for x
	public int  getX() 
	{
		return x;
	}
	//getter method for y
	public int  getY() 
	{
		return y;
	}
	//getter method for color
	public int getColor() 
	{
		return color;
	}	
	//getter method for speed
	public int getSpeed() 
	{
		return speed;	
	}
	//getter method for x direction
	public int getXDir() 
	{
		return xDir;
	}
    //getter method for y direction
	public int getYDir() 
	{
		return yDir;
	}
    //setter method for color
	public void setColor(int color) 
	{
		//make sure the color passed in the range allowed
		if ( (color>=0) && (color <=6) )
		{
			this.color = color;
		}
		else
		{
			this.color = this.color;
		}
	}
	//mutator method for x variable
	public void setX(int x) 
	{
		//decision statement to make sure passed in x value is valid
		if ( (x>0) && (x<courtWidth) )
		{
			this.x = x;
		}
	}
	//mutator method for y variable
	public void setY(int y) 
	{
		//decision statement for the passed in value is valid
		if ( (y<courtHeight) && (y>0))
		{
			this.y = y;
		}
	}
	//Method changes the direction of the y direction
	public void flipYDir() 
	{
		this.yDir = -(this.yDir);
	}
	//Method changes the direction of the x direction
	public void flipXDir() 
	{

		this.xDir = -(this.xDir);
		
	}
	//Method determines how the ball will move.
	public void move() 
	{
		//decision statement of what happens when it hits the right or left side of the court
		if( (this.x >= courtWidth-this.radius ) || (this.x <= this.radius) )
		{
			this.flipXDir();
		}
		//decision statement of what happens when it hits the top or bottom of the court.
		if( this.y >= (courtHeight-this.radius) || (this.y <= this.radius) )
		{
			this.flipYDir();
		}
		//Changes where the x and y coordinate goes
		this.x += speed*this.xDir;
		this.y += speed*this.yDir;
		System.out.println("Speed" + speed);
		System.out.println("X" + this.x);
		
		
	}
	//Method determines when there is a collision between balls
	public boolean isCollision(Ball anotherBall) 
	{
		if (  ( Math.sqrt( (Math.pow( anotherBall.x-this.x, 2) ) + (Math.pow( anotherBall.y-this.y, 2) ) ) )  <= (this.radius+anotherBall.radius)   )
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	//Method increases speed
	public void increaseSpeed() 
	{
		if(speed<5)
		{
		speed++;
		}
	}
	//Method decreases speed
	public void decreaseSpeed() 
	{	
		if (speed == 1)
		{
			return;
		}
		speed--;
	}
	//Defining a string method for ball class
	public String toString() 
	{
		String ball = "";
		ball = "\nx: " + this.x + "\ny: " + this.y + "\ncolor: " + this.color + "\nradius: " + this.radius + "\nspeed: " + this.speed + "\nxDir: " + xDir + 
				"\nyDir: " + yDir + "\ncourtWidth: " + courtWidth + "\ncourtHeight: " + courtHeight;
		return ball;
	}
    
}
