/*
CSCI1301.java 
* Author: Kosi Uzodinma 
* Submission Date: April 26, 2014
* 
* Purpose: A brief paragraph description of the 
* program. What does it do? How does it do it? 
* 
* Statement of Academic Honesty: 
* 
* The following code represents my own work. I have neither 
* received nor given inappropriate assistance. I have not copied 
* or modified code from any source other than the course webpage 
* or the course textbook. I recognize that any unauthorized 
* assistance or plagiarism will be handled in accordance with 
* the University of Georgia's Academic Honesty Policy and the 
* policies of this course. I recognize that my work is based 
* on a programming project created by the Department of 
* Computer Science at the University of Georgia. Any publishing 
* of source code for this project is strictly prohibited without 
* written consent from the Department of Computer Science. 
*/ 
public class Paddle {
	//Declaring variables for paddles class
	public int x;
	public int width;
	public int height;
	public int speed;
	public int direction;
	public int courtWidth;
	//Constructor for Paddle class that only allows one passed in value
	public Paddle (int courtWidth)  
	{
		//Assinging the default values for the variables
		width = 100;
		height = 10;
		direction = 0;
		speed = 0;
		//decision statements for value passed in value
		if (courtWidth >= width)
		{
			this.courtWidth = courtWidth;
		}
		else
		{
			this.courtWidth = 700;
		}
		this.x = ( (this.courtWidth-this.width)/2);
	}
	//Constructor with two passed in values
	public Paddle (int width, int courtWidth)  
	{
		//Assigning the default variables for variables
		height = 10;
		direction = 0;
		speed = 0;
		//Decision statements for value input for the passed in value
		if ( (courtWidth < this.width) || (courtWidth < 0) )
		{
			this.courtWidth = 700;
		}
		else
		{
			this.courtWidth = courtWidth;
		}
		if (width < 0)
		{
			this.width = 100;
		}
		else
		{
			this.width = width;
		}
		this.x = ( (this.courtWidth-this.width)/2);
			
	}
	//Accessor method for x
	public int getX() 
	{ 
		return x;
	}  
	//Accessor method for direction
	public int getDirection() 
	{
		return direction;
	}
	//Getter method for speed
	public int getSpeed() 
	{
		return speed;
	}
	//Getter method for width
	public int getWidth() 
	{
		return width;
	}
	//Getter method for height
	public int getHeight() 
	{
		return height;
	}  
	//Setter method for direction
	public void setDirection(int direction)   
	{
		//Decision statemet to make sure the passed in value is valid
		if( (direction < -1) || (direction >1) )
		{
			this.direction = 0;
		}
		
		else
		{
			this.direction = direction;
		}
	}
	//Setter method for speed
	public void setSpeed(int speed)   
	{
		//Decision statement to make sure that the values were accurate
		if (speed >= 0)
		{
			this.speed = speed;
		}
		else 
		{
			this.speed = 0;
		}
	}
	//Method that defines how the ball moves
	public void move() 
	{
		if ( (this.x <= 0) )
		{
			this.setDirection(1);	
		}
		if ( (this.x+this.width) >= this.courtWidth) 
		{
			this.setDirection(-1);
		}
			this.x += speed* this.direction;
		
		
	}
	//Method that defined what happened when there was a collision when the ball hit the paddle
	public boolean isCollision(Ball aBall) 
	{
		if (  ((aBall.getX()-aBall.getRadius()) >= this.x) && ((aBall.getX()-aBall.getRadius()) <= (this.x + this.width) ) && (aBall.getY()-aBall.getRadius()) <= this.height )  
		{
			
			return true;
		}
		
		
			return false;
		
	}
	//Centered the paddle in the middle of the court
	public void center()  
	{
		this.x = (courtWidth-width)/2;
	
	}
	//Defining a string method for the paddle
	public String toString() 
	{
		String paddleInformation = "";
		paddleInformation = "\nx: " + this.x + "\nwidth: " + this.width + "\nheight: " + this.height + "\nspeed: " + 
		this.speed + "\ndirection: " + this.direction + "\ncourtWidth: " + this.courtWidth;
		return paddleInformation;
	}

} 
