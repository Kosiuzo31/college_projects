import java.util.Scanner;

/* 
* CSCI1301.java 
* Author: Kosi Uzodinma 
* Submission Date: January 31, 2014 
* 
* Purpose: Writing a program that will help with credit card payment. It will prompt the user to fill in information, 
* that will tell the user the months needed to pay off, total amount paid, total interest paid, and over payment. 
* 
* Statement of Academic Honesty: 
* 
* The following code represents my own work. I have neither 
* received nor given inappropriate assistance. I have not copied 
* or modified code from any source other than the course webpage 
* or the course textbook. I recognize that any unauthorized  
* assistance or plagiarism will be handled in accordance with 
* the University of Georgia's Academic Honesty Policy and the 
* policies of this course. I recognize that my work is based 
* on an assignment created by the Department of Computer 
* Science at the University of Georgia. Any publishing 
* or posting of source code for this project is strictly 
* prohibited unless you have written consent from the Department 
* of Computer Science at the University of Georgia. 
*/

public class CreditCardPayOff 
{

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		//Declaring variables for the project
		double principal, annualInterestRate, monthlyPayment, originalMonthsNeededToPayOff, totalAmountPaid, totalInterestPaid, overpayment;
		Scanner keyboard = new Scanner(System.in);
		int monthsNeededToPayOff;
		
		//Prompting user to put in principal
		System.out.print("Principal:\t\t\t ");
		principal = keyboard.nextDouble();
		
		//Prompting user to put in double for Interest
		System.out.print("Annual Interest Rate (%):\t ");
		annualInterestRate = keyboard.nextDouble();
		
		//Prompting user to put in monthly payment
		System.out.print("Monthly Payment:\t\t " );
		monthlyPayment = keyboard.nextDouble();
		System.out.println("");
		
		//Initializing two variables for months needed to pay off
		System.out.print("Months Needed To Pay Off:\t ");
		originalMonthsNeededToPayOff = (Math.log(monthlyPayment)-(Math.log(monthlyPayment-(annualInterestRate/1200.0)*principal)))/(Math.log((annualInterestRate/1200.0)+1.0));
		monthsNeededToPayOff = (int)Math.ceil(originalMonthsNeededToPayOff);
		System.out.println(monthsNeededToPayOff);
		
		//Initializing variable for total amount paid
		totalAmountPaid = monthlyPayment*monthsNeededToPayOff;
		System.out.printf("Total Amount Paid:\t\t $%1.2f", totalAmountPaid);
		System.out.println("");
		
		//Initializing variable for total interest paid
		totalInterestPaid = totalAmountPaid-principal;
		System.out.printf("Total Interest Paid:\t\t $%1.2f", totalInterestPaid);
		System.out.println("");
		
		//Making an algorithm for over payment
		/* How to find overpayment
		 * 1.Find the amount for the rounded months needed to pay of the bill multiplied by the monthly payment.
		 * 2.Find the amount for the original months needed to pay off the bill multiplied by the monthly payment.
		 * 3.Subtract the first amount minus the second amount. 
		 */
		overpayment = (monthsNeededToPayOff*monthlyPayment)-(originalMonthsNeededToPayOff*monthlyPayment);
		System.out.printf("Overpayment:\t\t\t $%1.2f", overpayment);
	
	}

}
