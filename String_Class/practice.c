#include <stdio.h>

int length(const char *s)
{
int length=0;
while(*(s+length) != '\0')
	length++;
return length;
}
int compare(const char *s1, const char *s2)
{
int i, equal =0;
if(length(s1) != length(s2))
	return equal;
else
	equal = 1;
while(*(s1+i) != '\0')
{
	if(s1[i] != s2[i])
	equal =0;
	i++;
}
return equal;
}

int main()
{
	char foo1[] = "testings";
	char foo2[] = "testings";
	char *s1 = &foo1;
	char *s2 = &foo2;
	int testing = compare(s1, s2);
	printf("Testing to use compare function value: %d\n", testing);
	return 0;
}
