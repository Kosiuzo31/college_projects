#include <stdio.h>
#include "strings.h"
#include <string.h>
#include <stdlib.h>
/*
* Author: Kosi Uzodinma
* * * Submission Date: March 1, 2015
* * * Class Purpose: To learn systems Programming and practice string manipulation
* * * Statement of Academic Honesty:
* * *
* * *  The following code represents my own work. I have neither
* * *  received nor given inappropriate assistance. I have not copied
* * *  or modified code from any source other than the course webpage
* * *  or the course textbook. I recognize that any unauthorized
* * *  assistance or plagiarism will be handled in accordance with
* * *  the University of Georgia's Academic Honesty Policy and the
* * *  policies of this course. I recognize that my work is based
* * *  on an assignment created by the Department of Computer
* * *  Science at the University of Georgia. Any publishing
* * *  or posting of source code for this project is strictly
* * *  prohibited unless you have written consent from the Department
* * *  of Computer Science at the University of Georgia.
* * * */

int main(int argc, const char* argv[])
{
	char testcase1[] = "APPLE";
	int testcase1Length = 5;
	if(testcase1Length = length(&testcase1))
		printf("Length function success of Apple equals 5\n");
	char testcase2[] = "H";
	printf("Testcase2 Length of H: %d\n", length(&testcase2));
	char testcase3 = NULL;
	printf("Test3 Length equals 0 because it is null: %d\n", length(&testcase3)); 
	char testcase4[] = "HaaPPY";
	reverse(&testcase4);
	printf("Testing reverse function of word HaaPPY: %s\n",testcase4);
	char testcase5[]= "Stppp";
	char *testcase6 = clone(&testcase5);
	printf("Testing cloning function of testcase5 -Stppp to testcase6\n", testcase6);
	printf("Printing testcase6:%s\n", testcase6);
	char testcase7[]= "Team";
	char testcase8[]= "Team";
	char testcase7_1[] = "Con";
	char testcase8_1[] = "Cat";
	char *testcase9_1 = concat(&testcase7_1, &testcase8_1);
	printf("Testing concat function with string:%s and string:%s\n%s\n", testcase7_1, testcase8_1, testcase9_1);
	if(compare(&testcase7, &testcase8))
		printf("%s and %s are equal\n", testcase7, testcase8);
	else
		printf("%s and %s are not equal\n", testcase7, testcase8);
	char testcase9[]= "Team";
	char testcase10[]= "TeaM";
	if(compare(&testcase9, &testcase10))
		printf("%s and %s are equal\n", testcase9, testcase10);
	else
		printf("%s and %s are not equal\n", testcase9, testcase10);
	char testcase11[] = "Only";
	char testcase12[] ="O";
	if(search(&testcase11, &testcase12))
		printf("%s is a substring of %s\n", testcase12, testcase11);
	else
		printf("%s is not substring of %s\n", testcase12, testcase11);
	char testcase13[] ="ly";
	if(search(&testcase11, &testcase13))
		printf("%s is a substring of %s\n", testcase13, testcase11);
	else
		printf("%s is not substring of %s\n", testcase13, testcase11);
	char testcase14[] ="Only";
	if(search(&testcase11, &testcase14))
		printf("%s is a substring of %s\n", testcase14, testcase11);
	else
		printf("%s is not substring of %s\n", testcase14, testcase11);
	char testcase15[] ="nlyt";
	if(search(&testcase11, &testcase15))
		printf("%s is a substring of %s\n", testcase15, testcase11);
	else
		printf("%s is not substring of %s\n", testcase15, testcase11);
	char testcase16[50]= "I am here for you ";
	char testcase17[]= "always.";
	printf("Testing insert function at the end of string:%s with string:%s\n", testcase16, testcase17);
	insert(&testcase16,&testcase17, 40);
	printf("After calling insert function: %s\n", testcase16);
	char testcase18[50]= "Awesomeness";
	char testcase19[]= "always.";
	printf("Testing insert function at the beginning of string:%s with string:%s\n", testcase18, testcase19);
	insert(&testcase18,&testcase19, 0);
	printf("After calling insert function: %s\n", testcase18);
	char testcase20[50]= "Awesomeness";
	char testcase21[]= "always.";
	printf("Testing insert function in the middle at space 3 of string:%s with string:%s\n", testcase20, testcase21);
	insert(&testcase20,&testcase21, 3);
	printf("After calling insert function: %s\n", testcase20);
	char testcase22[] = "yes;I;am;testing;this;function";
	int i, count1 =0;
	char **testcase23 = tokenize(&testcase22, ';', &count1);
	printf("Testing token function for string: %s with delimiter ;\n", testcase22);
	printf("String list\n");
	for(i=0; i<count1; i++)
		printf("String%d: %s\n", i, testcase23[i]);
	int row;
	int count2=0;
	char testcase24[] = "testing^fooling^fooling^happy";
	char **testcase25 = tokenize(&testcase24, '^', &count2);
	printf("Testing token function for string: %s with delimiter^\n", testcase24);
	printf("String list\n");
	for(i=0; i<count2; i++)
		printf("String%d: %s\n", i, testcase25[i]);
	int count3=0;
	char testcase26[] = "1yest7fear7only7f";
	char **testcase27 = tokenize(&testcase26, '7', &count3);
	printf("Testing token function for string: %s with delimiter 7\n", testcase26);
	printf("String list\n");
	for(i=0; i<count3; i++)
		printf("String%d: %s\n", i, testcase27[i]);
char **test;
   char *testFour;
   int numTokens, *token;
   testFour = (char *)calloc(50, sizeof(char));
   sprintf(testFour, "check it with a space");
   token = &numTokens;
 
   test = tokenize(testFour, ' ', token);
 
   if (compare(test[0], "check"))
     printf("Test 1: Pass\n");
   else
     printf("Test 1: Fail\n");
   if (compare(test[1], "it"))
     printf("Test 2: Pass\n");
   else
     printf("Test 2: Fail\n");
   if (compare(test[4], "space"))
     printf("Test 3: Pass\n");
   else
    printf("Test 3: Fail\n");
	 char insertString[50];
	 sprintf(insertString, "1730 is amazing!");
	printf("%s", insertString);
	insert(insertString, "CSCI ", -10);
	if(strcmp(insertString, "CSCI 1730 is amazing!") == 0)
		printf("Insert test 1 passed.\n");
		else
		printf("Insert test 1 failed.  -5 points \n");
	printf("%s", insertString);
	//Freeing memory inside testing function
	free(testcase6);
	free(testcase9_1);
	for(row=0;row<count1; row++)
		free(testcase23[row]);
	free(testcase23);
	for(row=0;row<count2; row++)
		free(testcase25[row]);
	free(testcase25);
	for(row=0;row<count3; row++)
		free(testcase27[row]);
	free(testcase27);
	return 0;
}

