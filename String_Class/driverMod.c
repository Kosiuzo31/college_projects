#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "strings.h"

void testLength();
void testReverse();
void testClone();
void testCompare();
void testConcat();
void testSearch();
void testInsert();
void testTokenize();

int main(int argc, char *argv[])
{
   testLength(); //5 points	
	testReverse(); //10 points
	testClone(); //10 points
   testCompare(); //10 points
   testConcat(); //10 points
	testSearch(); //10 points
	testInsert(); //20 points
	testTokenize(); //25 points
	
   return 0;
}

/* Code to test the length function */
void testLength()
{
	if(length("Hello World!") != 12)
		printf("Length test 1 failed.  -3 points \n");
	else
		printf("Length test 1 passed.\n");
	if(length("") != 0)
		printf("Length test 2 failed.  -2 points \n");
	else
		printf("Length test 2 passed.\n");
}

/* Code to test the reverse function */
void testReverse()
{
	int result;
	char reverseString[30];
	sprintf(reverseString, "Hello World");
	reverse(reverseString);
	//strcmp will return a 0 if the two strings are equal.
	result = strcmp(reverseString, "dlroW olleH");
	if(result != 0)
	   printf("Reverse test 1 failed.  -5 points \n");
	else
		printf("Reverse test 1 passed.\n");
	
	sprintf(reverseString, "String methods are cool!");
	reverse(reverseString);
	result = strcmp(reverseString, "!looc era sdohtem gnirtS");
	if(result != 0)
	   printf("Reverse test 1 failed.  -5 points \n");
	else
		printf("Reverse test 2 passed.\n");
}

/* Code to test the clone function */
void testClone()
{
	int result;
   char cloneString[35];
	sprintf(cloneString, "Duplicate Me");
	char *duplicate = clone(cloneString);
	result = strcmp(duplicate, "Duplicate Me");
	//If the values are not equal or the addresses are the same, the test fails.
	if(result != 0 || duplicate == cloneString)
	{
	   printf("Clone test 1 failed.  -5 points \n");
	   free(duplicate);
	}
	else
	{
	   //We should be able to free the cloned string as clone should malloc space.
	   free(duplicate); 
		printf("Clone test 1 passed.\n");
	}
	strcpy(cloneString, "");
	duplicate = clone(cloneString);
	result = strcmp(duplicate, "");
	//If the values are not equal or the addresses are the same, the test fails.
	if(result != 0 || duplicate == cloneString)
	{
	   printf("Clone test 2 failed.  -5 points \n");
	   free(duplicate);
	}
	else
	{
	   //We should be able to free the cloned string as clone should malloc space.
	   free(duplicate); 
		printf("Clone test 2 passed.\n");
	}
}

/* Code to test the compare function */
void testCompare()
{
   char string1[25];
	char string2[25];
	sprintf(string1, "It's fun to test strings!");
	sprintf(string2, "It's fun to test strings!");
	if(compare(string1, string2) == 1)
		printf("Compare test 1 passed. \n");
	else
		printf("Compare test 1 failed.  -2 points. \n");
	strcpy(string2, "");
	if(compare(string1, string2) != 1 && compare(string2, string1) != 1)
		printf("Compare test 2 passed. \n");
	else
		printf("Compare test 2 failed.  -2 points. \n");
	if(compare("hello", "lo") != 1 )
		printf("Compare test 3 passed. \n");
	else
		printf("Compare test 3 failed.  -2 points. \n");
	if(compare("hello", "helloooo") != 1 )
		printf("Compare test 4 passed. \n");
	else
		printf("Compare test 4 failed.  -2 points. \n");
	if(compare("hello", "ze") != 1 )
		printf("Compare test 5 passed. \n");
	else
		printf("Compare test 5 failed.  -2 points. \n");
}

/* Code to test the concat function */
void testConcat()
{
	int result;
	char string1[10];
	char string2[10];
	sprintf(string1, "Concat Me");
	sprintf(string2, " Please");
	char *resultString = concat(string1, string2);
	result = strcmp(resultString, "Concat Me Please");
	//If the values are not equal or the length is incorrect, the test fails.
	if(result != 0 || strlen(resultString) != 16)
	{
	   printf("Concat test 1 failed.  -5 points \n");
	   free(resultString);
	}
	else
	{
	   //We should be able to free the cloned string as clone should malloc space.
	   free(resultString); 
		printf("Concat test 1 passed.\n");
	}
	strcpy(string1, "");
	resultString = concat(string1, string2);
	result = strcmp(resultString, " Please");
	//If the values are not equal or the length is not correct, the test fails.
	if(result != 0 || strlen(resultString) != 7)
	{
	   printf("Concat test 2 failed.  -5 points \n");
		free(resultString);
	}
	else
	{
	   //We should be able to free the cloned string as clone should malloc space.
	   free(resultString); 
		printf("Concat test 2 passed.\n");
	}
}

/* Code to test the search function */
void testSearch()
{
   if(search("hello world", "llo world") != 1)
	   printf("Search test 1 failed.  -2 points \n");
	else
		printf("Search test 1 passed.\n");
   if(search("hello world", "") != 1)
	   printf("Search test 2 failed.  -2 points \n");
	else
		printf("Search test 2 passed.\n");
   if(search("hello world", NULL) != 1)
	   printf("Search test 3 failed.  -2 points \n");
	else
		printf("Search test 3 passed.\n");
   if(search("Hello", "Helloo") == 1)
	   printf("Search test 4 failed.  -2 points \n");
	else
		printf("Search test 4 passed.\n");
   if(search("lloaaaaaaaaaa", "loaaaaa") != 1)
	   printf("Search test 5 failed.  -2 points \n");
	else
		printf("Search test 5 passed.\n");
}

/* Code to test the insert function */
void testInsert()
{
   char insertString[50];

	sprintf(insertString, "1730 is amazing!");
	insert(insertString, "CSCI ", -10);

   if(strcmp(insertString, "CSCI 1730 is amazing!") == 0)
		printf("Insert test 1 passed.\n");
	else
	   printf("Insert test 1 failed.  -5 points \n");

	sprintf(insertString, "1730 is amazing!");
	insert(insertString, "seriously ", 8);

   if(strcmp(insertString, "1730 is seriously amazing!") == 0)
		printf("Insert test 2 passed.\n");
	else
	   printf("Insert test 2 failed.  -5 points \n");

	sprintf(insertString, "ade");
	insert(insertString, "bc", 1);
   if(strcmp(insertString, "abcde") == 0)
		printf("Insert test 3 passed.\n");
	else
	   printf("Insert test 3 failed.  -5 points \n");

	sprintf(insertString, "ade");
	insert(insertString, "bc", 1);
   if(strcmp(insertString, "abcde") == 0)
		printf("Insert test 4 passed.\n");
	else
	   printf("Insert test 4 failed.  -5 points \n");

	sprintf(insertString, "abcde");
	insert(insertString, " .", 100);
   if(strcmp(insertString, "abcde .") == 0)
		printf("Insert test 5 passed.\n");
	else
	   printf("Insert test 5 failed.  -5 points \n");
}

/* Code to test the tokenize function */
void testTokenize()
{
	int i=0;
	int numTokens=0, numTokens1=0, numTokens2=0, numTokens3=0, numTokens4=0;
	char **tokenizedString, **tokenizedString1, **tokenizedString2, **tokenizedString3, **tokenizedString4;
	tokenizedString = tokenize("a;b;c", ';', &numTokens);
	if(strcmp(tokenizedString[0], "a") == 0 && strcmp(tokenizedString[1], "b") == 0 &&
		strcmp(tokenizedString[2], "c") == 0 && numTokens == 3)
		printf("Tokenize test 1 passed.\n");
	else
		printf("Tokenize test 1 failed.  -5 points \n");
	tokenizedString1 = tokenize("a;b;c;", ';', &numTokens1);
	if(strcmp(tokenizedString1[0], "a") == 0 && strcmp(tokenizedString1[1], "b") == 0 &&
		strcmp(tokenizedString1[2], "c") == 0 && strcmp(tokenizedString1[3], "") == 0 && numTokens1 == 4)
		printf("Tokenize test 2 passed.\n");
	else
		printf("Tokenize test 2 failed.  -5 points \n");
	tokenizedString2 = tokenize("abcd", ';', &numTokens2);
	if(strcmp(tokenizedString2[0], "abcd") == 0 && numTokens2 == 1)
		printf("Tokenize test 3 passed.\n");
	else
		printf("Tokenize test 3 failed.  -5 points \n");
	tokenizedString3 = tokenize("a;b;c;ab;cd;efgh;a;abcd", ';', &numTokens3);
	if(strcmp(tokenizedString3[0], "a") == 0 && strcmp(tokenizedString3[1], "b") == 0 &&
		strcmp(tokenizedString3[2], "c") == 0 && strcmp(tokenizedString3[3], "ab") == 0 && 
		strcmp(tokenizedString3[4], "cd") == 0 && strcmp(tokenizedString3[5], "efgh") == 0 &&
		strcmp(tokenizedString3[6], "a") == 0 && strcmp(tokenizedString3[7], "abcd") == 0 && numTokens3 == 8)
		printf("Tokenize test 4 passed.\n");
	else
		printf("Tokenize test 4 failed.  -5 points \n");
	tokenizedString4 = tokenize("abc def; ;hij", ' ', &numTokens4);
	if(strcmp(tokenizedString4[0], "abc") == 0 && strcmp(tokenizedString4[1], "def;") == 0 &&
		strcmp(tokenizedString4[2], ";hij") == 0 && numTokens4 == 3)
		printf("Tokenize test 5 passed.\n");
	else
		printf("Tokenize test 5 failed.  -5 points \n");
	for(i = 0; i < numTokens; i++)
		free(tokenizedString[i]);
	free(tokenizedString);
	for(i = 0; i < numTokens1; i++)
		free(tokenizedString1[i]);
	free(tokenizedString1);
	for(i = 0; i < numTokens2; i++)
		free(tokenizedString2[i]);
	free(tokenizedString2);
	for(i = 0; i < numTokens3; i++)
		free(tokenizedString3[i]);
	free(tokenizedString3);
	for(i = 0; i < numTokens4; i++)
		free(tokenizedString4[i]);
	free(tokenizedString4);

	//tokenizedString = tokenize(NULL, ';', &numTokens);
	//tokenizedString = tokenize(stringToTokenize, ';', &numTokens);
}
