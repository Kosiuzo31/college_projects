#include <stdio.h>
#include "strings.h"
/*
* Author: Kosi Uzodinma 
* * Submission Date: March 1, 2015  
* * Class Purpose: To learn systems Programming and practice string manipulation  
* * Statement of Academic Honesty: 
* * 
* *  The following code represents my own work. I have neither 
* *  received nor given inappropriate assistance. I have not copied 
* *  or modified code from any source other than the course webpage 
* *  or the course textbook. I recognize that any unauthorized  
* *  assistance or plagiarism will be handled in accordance with 
* *  the University of Georgia's Academic Honesty Policy and the 
* *  policies of this course. I recognize that my work is based 
* *  on an assignment created by the Department of Computer 
* *  Science at the University of Georgia. Any publishing 
* *  or posting of source code for this project is strictly 
* *  prohibited unless you have written consent from the Department 
* *  of Computer Science at the University of Georgia. 
* * */

int length(const char *s)
{
	int length=0;
	if( *(s+length) == NULL )
		return length;
	while(*(s+length) != '\0')
		length++;
	return length;

}

void reverse(char *s)
{
	int rightIndex=length(s);
	//Doing this so it starts at the end of the string
	rightIndex--;
	int leftIndex =0;
	char temp;
	while( leftIndex <= rightIndex)
	{
		temp = *(s+leftIndex);
		*(s+leftIndex) = *(s+rightIndex);
		*(s+rightIndex) = temp;
		leftIndex++;
		rightIndex--;
	}
}
char* clone(const char *s)
{
	int total = length(s) +1;
	char *clonedString = (char *)malloc(total);
	int i=0;
	while(*(s+i) != '\0')
	{
		clonedString[i] = s[i];
		i++;
	}
	return clonedString;
	

}
int compare(const char *s1, const char *s2)
{
	int i =0;
	if(length(s1) != length(s2))
		return 0;
	while(*(s1+i) != '\0')
	{
		if(s1[i] != s2[i])
			return 0;
		i++;
	}
	return 1;
}
char* concat(const char *s1, const char *s2)
{
	//Added one so it can have \0 at the end
	int total = length(s1) + length(s2) +1 ;
	char *concatString = (char *)malloc(total);
	int i,j;
	for(i=0; i<length(s1); i++)
		*(concatString +i) = *(s1+i);
	for(j=0; j<length(s2); j++)
	{
		*(concatString +i) = *(s2+j);
		i++;
	}
	*(concatString +i) = '\0';
	return concatString; 
	
}
int search(const char *s1, const char*s2)
{
	if(s2==NULL)
		return 1;
	if(length(s1) < length(s2))
		return 0;
	int valid,j,k, i =0, lengthS2 = length(s2);

	while(s1[i] != '\0')
	{
		valid =0;
		k=i;
		for(j=0; j<lengthS2; j++)
		{
			if(s1[k] == '\0')
				return 0;
			if(s2[j] == s1[k])
				valid++;
			k++;
		}
		if(valid == lengthS2)
			return 1;
		i++;
	}
}

void insert(char *s1, const char *s2, int n)
{
	if(n<=0)
	{
		int i =0;
		char *temp = clone(s1);
		while(s2[i] != '\0')
		{
			s1[i]=s2[i];
			i++;
		}
		n=0;
		while(temp[n] !='\0')
		{
			s1[i] = temp[n];
			i++;
			n++;
		}
		s1[i]='\0';
		free(temp);
	}
	else if(n>=length(s1))
	{
		int i =0, n = length(s1);
		while(s2[i] != '\0')
		{
			s1[n]=s2[i];
			n++;
			i++;
		}
		s1[n]='\0';

	}
	else
	{
		int i =0,j=0;
		j=n;
		char *temp = clone(s1);
		while(s2[i] != '\0')
		{
			s1[j]=s2[i];
			i++;
			j++;
		}
		while(temp[n] !='\0')
		{
			s1[j] = temp[n];
			j++;
			n++;
		}
		s1[j]='\0';
		free(temp);
	}
	
}

char** tokenize(const char *s1, char delimiter, int *numTokens)
{
   *numTokens=0;
	if(s1[0]==NULL)
	return NULL;
	int i=0, endOfString=length(s1);
	while(i<endOfString)
	{
		if(s1[i] == delimiter)
			*numTokens +=1;
		i++;
	}
	*numTokens +=1;
	char **tokenizedString = (char**)calloc(*numTokens, sizeof(char*));
	i=0;
	int a=0, tokenLength=0, j=0;
	while(i<*numTokens)
	{
		tokenLength=0;
		while(1)
		{
		if( (s1[a] == delimiter) || (s1[a] == '\0') )
			break;
		
			tokenLength++;
			a++;
		}
		tokenizedString[i] = (char*)calloc((tokenLength+1), sizeof(char));
		a++;
		i++;
	}
	i=a=0;
	while(i<*numTokens)
	{
		j=0;
		while(1)
		{
		if(s1[a] == delimiter || s1[a] == '\0' )
			break;	
			tokenizedString[i][j] = s1[a]; 
			j++;
			a++;
		}
		tokenizedString[i][j] = '\0';
		a++;
		i++;
	}
	return tokenizedString;

}

