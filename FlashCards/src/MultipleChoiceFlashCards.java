import java.util.Scanner; 

/* 
* CSCI1301.java 
* Author: Kosi Uzodinma 
* Submission Date: January 31, 2014 
* 
* Purpose: A program that will aid in studying programming concepts 
* 
* Statement of Academic Honesty: 
* 
* The following code represents my own work. I have neither 
* received nor given inappropriate assistance. I have not copied 
* or modified code from any source other than the course webpage 
* or the course textbook. I recognize that any unauthorized  
* assistance or plagiarism will be handled in accordance with 
* the University of Georgia's Academic Honesty Policy and the 
* policies of this course. I recognize that my work is based 
* on an assignment created by the Department of Computer 
* Science at the University of Georgia. Any publishing 
* or posting of source code for this project is strictly 
* prohibited unless you have written consent from the Department 
* of Computer Science at the University of Georgia. 
*/

public class MultipleChoiceFlashCards 
{

	public static void main(String[] args) 
	{
		System.out.println("Welcome to CSCI 1301 Multiple Choice Flash Cards!");
		System.out.println("");
		Scanner keyboard = new Scanner(System.in);
		String questionAndChoices, choiceA, choiceB, choiceC, choiceD, answer, userAnswer; //Creating strings for the jar file imported will work
		int score = 0;
		
		questionAndChoices = MultipleChoiceFlashCardsZoeyGameEngine001.getQuestionAndChoices(); //Copied this to get the information for the jar file
		choiceA = MultipleChoiceFlashCardsZoeyGameEngine001.getChoiceA(); 
		choiceB = MultipleChoiceFlashCardsZoeyGameEngine001.getChoiceB(); 
		choiceC = MultipleChoiceFlashCardsZoeyGameEngine001.getChoiceC(); 
		choiceD = MultipleChoiceFlashCardsZoeyGameEngine001.getChoiceD(); 
		answer = MultipleChoiceFlashCardsZoeyGameEngine001.getAnswer();
		
		System.out.println(questionAndChoices); //printed out the questions for the flash card game
		System.out.print("Enter your answer (a-d): ");
		userAnswer = keyboard.nextLine();
		userAnswer = userAnswer.toUpperCase();
		userAnswer = userAnswer.substring(0,1);
		
		if ((userAnswer.equalsIgnoreCase("A")) || (userAnswer.equalsIgnoreCase("B")) || (userAnswer.equalsIgnoreCase("C")) || (userAnswer.equalsIgnoreCase("D"))) 
			// Set the user input as a-d and made it disregard case
			{	
				if ( ( (userAnswer.equals("A")) && (choiceA.equalsIgnoreCase(answer)) ) | ( (userAnswer.equals("B")) && (choiceB.equalsIgnoreCase(answer)) )  |
				     ( (userAnswer.equals("C")) && (choiceC.equalsIgnoreCase(answer)) ) | ( (userAnswer.equals("D")) && (choiceD.equalsIgnoreCase(answer)))	)
					// Matched the user answer to the correct answer
	
				{	
					System.out.println("Correct Answer");
					score +=1;
					System.out.println("Your score: " + score + " out of 1 points(s)");
					System.out.println();
					// Prompted the user if the answer was correct, added one to the user's score, and reported what their score.
				}
				
				else
				{
					System.out.println("Incorrect Answer!");
					System.out.println("Your score: " + score + " out of 1 points");
					System.out.println();
					// Prompted the user if the answer was incorrect and told the user what their score was.
				}
			}
		else 
		{
			System.out.println("Invalid Answer.");
			System.out.println("An answer must begin with a, A, b, B, c, C, D, or d."); 
			System.out.println("Game over!");
			System.exit(0);
			// Prompted the user that they inputed an invalid response and terminated the program
		}
			// Repeated all the steps I did above to get the second question. I also changed 1 points to 2 points.
		questionAndChoices = MultipleChoiceFlashCardsZoeyGameEngine001.getQuestionAndChoices(); 
		choiceA = MultipleChoiceFlashCardsZoeyGameEngine001.getChoiceA(); 
		choiceB = MultipleChoiceFlashCardsZoeyGameEngine001.getChoiceB(); 
		choiceC = MultipleChoiceFlashCardsZoeyGameEngine001.getChoiceC(); 
		choiceD = MultipleChoiceFlashCardsZoeyGameEngine001.getChoiceD(); 
		answer = MultipleChoiceFlashCardsZoeyGameEngine001.getAnswer();
		
		System.out.println(questionAndChoices);
		System.out.print("Enter your answer (a-d): ");
		userAnswer = keyboard.nextLine();
		userAnswer = userAnswer.toUpperCase();
		userAnswer = userAnswer.substring(0,1);
		
		if ((userAnswer.equalsIgnoreCase("A")) || (userAnswer.equalsIgnoreCase("B")) || (userAnswer.equalsIgnoreCase("C")) || (userAnswer.equalsIgnoreCase("D")))
			{	
				if ( ( (userAnswer.equals("A")) && (choiceA.equalsIgnoreCase(answer)) ) | ( (userAnswer.equals("B")) && (choiceB.equalsIgnoreCase(answer)) )  |
				     ( (userAnswer.equals("C")) && (choiceC.equalsIgnoreCase(answer)) ) | ( (userAnswer.equals("D")) && (choiceD.equalsIgnoreCase(answer)))	)
	
				{	
					System.out.println("Correct Answer");
					score +=1;
					System.out.println("Your score: " + score + " out of 2 points(s)");
					System.out.println();
				}
				
				else
				{
					System.out.println("Incorrect Answer!");
					System.out.println("Your score: " + score + " out of 2 points");
					System.out.println();
				}
			}
		else 
		{
			System.out.println("Invalid Answer.");
			System.out.println("An answer must begin with a, A, b, B, c, C, D, or d."); 
			System.out.println("Game over!");
			System.exit(0);
		}
			//Repeated the steps above to get the third question. I also changed 2 points to 2 points
		questionAndChoices = MultipleChoiceFlashCardsZoeyGameEngine001.getQuestionAndChoices(); 
		choiceA = MultipleChoiceFlashCardsZoeyGameEngine001.getChoiceA(); 
		choiceB = MultipleChoiceFlashCardsZoeyGameEngine001.getChoiceB(); 
		choiceC = MultipleChoiceFlashCardsZoeyGameEngine001.getChoiceC(); 
		choiceD = MultipleChoiceFlashCardsZoeyGameEngine001.getChoiceD(); 
		answer = MultipleChoiceFlashCardsZoeyGameEngine001.getAnswer();
		
		System.out.println(questionAndChoices);
		System.out.print("Enter your answer (a-d): ");
		userAnswer = keyboard.nextLine();
		userAnswer = userAnswer.toUpperCase();
		userAnswer = userAnswer.substring(0,1);
		
		if ((userAnswer.equalsIgnoreCase("A")) || (userAnswer.equalsIgnoreCase("B")) || (userAnswer.equalsIgnoreCase("C")) || (userAnswer.equalsIgnoreCase("D")))
			{	
				if ( ( (userAnswer.equals("A")) && (choiceA.equalsIgnoreCase(answer)) ) | ( (userAnswer.equals("B")) && (choiceB.equalsIgnoreCase(answer)) )  |
				     ( (userAnswer.equals("C")) && (choiceC.equalsIgnoreCase(answer)) ) | ( (userAnswer.equals("D")) && (choiceD.equalsIgnoreCase(answer)))	)
	
				{	
					System.out.println("Correct Answer");
					score +=1;
					System.out.println("Your score: " + score + " out of 3 points(s)");
				}
				
				else
				{
					System.out.println("Incorrect Answer!");
					System.out.println("Your score: " + score + " out of 3 points");
				}
			}
		else 
		{
			System.out.println("Invalid Answer.");
			System.out.println("An answer must begin with a, A, b, B, c, C, D, or d."); 
			System.out.println("Game over!");
			System.exit(0);
		}
		//Told the user what their final score was
		System.out.println();
		System.out.println("Your final score: " + score + " out of 3 points");
		
		// Determined how good their computer wit is based on their score and displayed a message to the user about their computer science wit
		if (score == 0)
		{
			System.out.println("Your computer science wit needs sharpening");
			System.out.println("Game Over!");
		}
		if (score == 1)
		{
			System.out.println("Your computer science wit needs sharpening");
			System.out.println("Game Over!");
		}
		if (score == 2)
		{
			System.out.println("Your computer science wit is sharp");
			System.out.println("Game Over!");
		}
		if (score == 3)
		{
			System.out.println("Your computer science wit is as sharp as Occam's razor");
			System.out.println("Game Over!");
		}
	}
	
}


	
