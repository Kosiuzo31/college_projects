#include <iostream>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <cstring>
#include <string.h>
#include "Tree.h"
#include <array>
using namespace std;
using std::cout;
using std::endl;
using std::array;
using std::max;
int main(int argc, char* argv[])
{
	Tree<int> *tt = new Tree<int>();
	string line; //String for getline
	char command[20];
	string filename = argv[1];
	int i;
	ifstream myfile (filename);
	if (myfile.is_open())
		{
		while ( getline (myfile,line) ) //Goes all the way to the end of the file
		{
		sscanf(line.c_str(), "%s %d",command, &i); //Get the word and the number 
		if(strcmp(command, "insert") == 0) //If insert use insert method
			tt->insert(i);
		else if(strcmp(command, "delete") == 0)//If delete use the delete method
			tt->deleteNode(i);
		}
		myfile.close();
		}

	else cout << "Unable to open file"<<endl; //If you can't open the file
	cout <<"Number of nodes in the bst: " << tt->getSize() <<endl; //Prints nodes in the bst
	cout <<"Height of the bst: "<<tt->height(tt->getRoot())<<endl; //Prints height
	cout <<"Pre-order traversal of the bst: ";
	tt->preorder(tt->getRoot()); //The post order of the tree
	cout <<endl;
	cout <<"In-order traversal of the bst: ";
	tt->inorder(tt->getRoot()); //The inorder of the tree
	cout <<endl;
	cout <<"Post-order traversal of the bst: "; //The post-order of the tree
	tt->postorder(tt->getRoot());
	cout <<endl;
	delete(tt); 
	return 0;
}