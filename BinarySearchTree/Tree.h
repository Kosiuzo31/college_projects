#ifndef TREE_H
#define TREE_H
#include <iostream>
#include <algorithm>
using std::max;
using std::cout;
using std::endl;
using std::string;
// forward declation of Tree
template<typename T>
class Tree;
/**
 * Represents a node in a doubly-linked list.
 */
template<typename T>
class Node {
    friend class Tree<T>;
    T data = 0;
    bool iAmZero = false; //To say when there is a zero node and to not treat as a regular node
    Node<T> *parent;
    Node<T> *left;
    Node<T> *right;
    Node()
    {
    	parent = nullptr; //Parent of the node
        left = nullptr; //Left child of the node
        right = nullptr; //Right child of the node
    }
}; // Node
/**
 * A doubly-linked list data structure.
 */
template<typename T>
class Tree {
public:
    Node<T> *root; //The root of the tree 
    int zero_count =0; //To see if zero was input into the tree
    int size =0; //TO keep the size of the tree
    
    //Default constructor 
    Tree()
    {
        //Creating root and setting everything to null
        root = new Node<T>();
        root->parent =nullptr;
        root->left = nullptr;
        root->right = nullptr;
       
    }
    //Decontructor
    ~Tree()
    {
        clearTree(getRoot()); //Deallocating all nodes in the tree
        
    }
    void clearTree(Node<T> *x) //Recursively freeing all nodes in the tree
    {
        if (x == nullptr) return;
        clearTree(x->left); 
        clearTree(x->right);
        delete(x);
    }
    Node<T>* setRoot() //Setting the root back to normal if it was deleted
    {
        root = new Node<T>();
        root->parent =nullptr;
        root->left = nullptr;
        root->right = nullptr;
        return root;
    }
    int height(Node<T>* x) //Retrives the height of the tree recursively
    {
        if(x == nullptr) return 0;
        if(isLeaf(x)) return 0;
        return 1 + max(height(x->left), height(x->right));
    }
    void inorder(Node<T>*x) //Prints the tree inorder
    {
        if(x!=nullptr)
        {
            inorder(x->left);
            cout<< x->data << " ";
            inorder(x->right);
        }
    }
    void preorder(Node<T>*x) //Prints the tree in preorder
    {
        if(x!=nullptr)
        {
            cout<< x->data << " ";
            preorder(x->left);
            preorder(x->right);
        }

    }
    void postorder(Node<T>*x) //Print the tree in postorder
    {
        if(x!=nullptr)
        {
            postorder(x->left);
            postorder(x->right);
            cout<< x->data << " ";
        }
    }
    bool isLeaf(Node<T>* x) //Checks to see if the node is a leaf
    {
        if(x->left ==nullptr && x->right ==nullptr)
            return true;
        else 
            return false;
    }
    Node<T>* getRoot() //Returns the root of the tree for other methods 
    {
        return root;
    }
    int getSize() //Returns the size of the tree
    {
        return size;
    }
    Node<T>* treeMinimum(Node<T>* x) //Get the minimum of the tree
    {
        Node<T> *temp = x;
        while (temp->left !=nullptr)
        {
            temp = temp->left;
        }
        return temp;

    }
    void transplant(Node<T> *x, Node<T> *y) //Transplant for the delete method
    {
        if(x->parent == nullptr) //Replacing the root 
            root = y;
        else if(x==x->parent->left)
                x->parent->left =y;
        
        else 
            x->parent->right = y;
        if (y !=nullptr)
            y->parent = x->parent;
    }
    void deleteNode(T data) //Deletes a node from the tree 
    {

        Node<T> *x = getRoot();
        while(1) //Finds the node to delete 
        {
            if(x ==nullptr)
                return;
            if(data == x->data)
                break;
            else if(data <x->data)
                x = x->left;
            else if(data >x->data)
                x = x->right;
        }
        if(size ==1) //check if its deleting the node
        {
            setRoot();
        }

        else if(x->right == nullptr) //Case 2 of delete methods
            transplant(x, x->left); 
        else //Modified Case 1, Case 3 and 4
        {   
              
                Node<T> *y = treeMinimum(x->right);
            if(y->parent != x)
            {
                transplant(y, y->right);
                y->right = x->right;
                y->right->parent = y;
            }
            transplant(x,y);
            y->left = x->left;
            if(y->left !=nullptr)
                y->left->parent =y;
            
        }
        size--;

    }
    void insert(T data) //Insert into the tree 
    {
        //if the input is zero and there has already been a zero
        if(data == 0 && zero_count >0)
            return;

        //checking to see if the root is empty
        if(size == 0)
        {
            //counter to see if zero was inputted 
            if(data == 0)
                zero_count++;

    		root->data = data;
            size++;
            return;
        }

        

        //Do nothing if the root has the same value trying to be inserted 
        if(root->data == data)
            return;

        else
        {
                Node<T> *temp = root;
                Node<T> *temp_2 = temp; //To keep track of parent
                //Keeps going until it encounters a break
                while(1)
                {
                
                if(data < temp->data)
                {
                    if(temp->left ==nullptr)
                    {
                        temp->left = new Node<T>;
                    }
                    temp = temp->left;
                    //Making the left child point back to the parent
                    temp->parent =temp_2;
                    //Making temp_2 point to temp so I can continue to make children point back to parent
                    temp_2 = temp;

                    //Do nothing for inserting the same number
                    if(temp->data == data && data !=0)
                        return;

                    //Do nothing if the number 0 has already been inserted
                    if(temp->data == data && zero_count >0)
                    {
                        return;
                    }
                    if(temp->data ==0)
                    {
                        if(temp->iAmZero)
                            continue;
                        if(data ==0)
                        {
                            zero_count++;
                            temp->iAmZero = true;
                        }
                        temp->data = data;
                        size++;
                        return;
                    }

                }

                else 
                {
                    if(temp->right ==nullptr)
                    {
                        temp->right = new Node<T>;
                    }
                    temp = temp->right;
                    //Making the right child point back to the parent
                    temp->parent =temp_2;
                    //Making temp_2 point to temp so I can continue to make children point back to parent
                    temp_2 =temp;

                    //Do nothing for inserting the same number
                    if(temp->data == data && data !=0)
                        return;

                    //Do nothing if the number 0 has already been inserted
                    if(temp->data == data && zero_count >0)
                    {
                        return;
                    }
                    //If the node is empty insert node 
                    if(temp->data ==0)
                    {
                        if(temp->iAmZero)
                            continue;
                        if(data ==0)
                        {
                            zero_count++;
                            temp->iAmZero = true;
                        }
                        temp->data = data;
                        size++;
                        return;
                    }
                }

                }   
        }

    }
    
    /* post order without recursion
    // void postorder()
    // {
    //     Node<T> *temp = root;
    //     T rootNum = temp->data;
        
    //     //Array to see if number has already been printed and to print afterwards
    //     T treeArray[size];

    //     treeArray[0] = rootNum;
    //     int i = 1; //Starts at one because I put root into array already
       

    //     while(i<size)
    //     {
            
            
    //         if(temp->left !=nullptr && !insideArray(treeArray, temp->left->data,i))
    //         {
    //                 temp = temp->left;
    //                 treeArray[i] = temp->data;
    //                 i++;
    //                 continue;
    //         }

    //         else if(temp->right !=nullptr && !insideArray(treeArray, temp->right->data,i))
    //         {
    //             temp = temp->right;
    //                 treeArray[i] = temp->data;
    //                 i++;
    //                 continue;
                
    //         }
    //         else
    //         {
    //             temp = temp->parent;
    //         }
        

    //     }
        
    //     cout << "Pre-order traveral: ";
    //     for(int a = 0; a<size; a++)
    //     {
    //         cout << treeArray[a] << " ";
    //     }
    //     cout <<endl;

    // }
    
    bool insideArray(T input[], T value, int lastIndexOfArray)
    {
        //So you wont have to compare as many numbers is why lastIndexOfArray

        //int arraySize = lastIndexOfArray;
        for(int i =0; i<lastIndexOfArray; i++)
        {
            if(input[i] == value)
                return true;
        }
            return false;
    }
    */ //End of postorder without recursion 
    
    
    
    
}; // Tree
#endif /** TREE_H */

