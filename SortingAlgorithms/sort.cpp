#define ARRAY_SIZE(array) (sizeof((array))/sizeof((array[0])))
#include <cstdlib>
#include <stdio.h>
#include "SortingAlgorithms.h"
#include <string>
#include <stdlib.h>
#include <cstring>
using namespace std;
using std::cout;
using std::endl;
using std::string;
int main(int argc, char* argv[])
{
	//Stop the program if you dont have enough input
	if(argc<3)
	{
		cout << "You didn't input enough arguments"<<endl;
		return 0;
	}
	//Array from command line array
	long inputArray[argc-2]; //size of the array from input

	int i =0; //iterator for array
	string num; //String to be converted 
	SortingAlgorithms sa; //Objects for the sorting algorithms 
	string command = argv[1]; //command for which algorithm to use 

	//Putting all the numbers in the arrays 
	while(i+2<argc)
	{
		num = argv[i+2];
		inputArray[i] = atol(num.c_str());
		i++;
	}
	//Determing to which algorithm 
	if(command == "-i")
	{
		//Sorting the array using insertion sort
		sa.insertionSort(inputArray,ARRAY_SIZE(inputArray));
		cout << "Insertion sort: ";
		//Printing out the array after being sorted 
		for(int elem: inputArray)
		cout<< elem << " ";
		cout <<endl;
		//Printing out the size 
		cout << "Input size: " << ARRAY_SIZE(inputArray) <<endl;
		//Printing out the comparisons 
		cout << "Total # comparisons: " << sa.getInsertionCount() <<endl;
	}
	else if(command == "-m")
	{
		//Sorting the array using merge sort
		sa.mergeSort(inputArray,0, ARRAY_SIZE(inputArray)-1);
		cout << "Merge sort: ";
		//Printing out the array after being sorted 
		for(int elem: inputArray)
		cout<< elem << " ";
		cout <<endl;
		//Printing out the size 
		cout << "Input size: " << ARRAY_SIZE(inputArray) <<endl;
		//Printing out the comparisons 
		cout << "Total # comparisons: " << sa.getMergeCount() <<endl;
	}
	else if(command == "-q")
	{
		//Sorting the array using quick sort
		sa.quickSort(inputArray,0,ARRAY_SIZE(inputArray)-1);
		cout << "Quick sort: ";
		//Printing out the array after being sorted 
		for(int elem: inputArray)
		cout<< elem << " ";
		cout <<endl;
		//Printing out the size 
		cout << "Input size: " << ARRAY_SIZE(inputArray) <<endl;
		//Printing out the comparisons 
		cout << "Total # comparisons: " << sa.getQuickCount() <<endl;
	}
	else 
	{
		cout << "You must input either -i, -m, or -q" <<endl;
	}
	


	return 0;
}
