#ifndef SortingAlgorithms_H
#define SortingAlgorithms_H
#include <iostream>
//Forward declation of Sorting Algorithm
class SortingAlgorithms;

class SortingAlgorithms{


private:
	long quickSort_Counter=0;
	long mergeSort_Counter=0;
	long insertionSort_Counter=0;
public:
	//Method to do Insertion sort
	void insertionSort(long input[], int size)
	{
		long key;
		int x;
		for(int i =1; i<size; i++)
		{
			key = input[i];
			x = i-1;
			while (x>=0 && input[x]>key)
			{
				input[x+1] = input[x];
				x = x-1;
				insertionSort_Counter++;
			}
			//To make sure the counter receives the count of comparing to see where it shouldn't go 
			if(input[x]<key)
				insertionSort_Counter++;
			input[x+1] = key;
		}
	}

	//Method to do MergeSort
	void mergeSort(long input[], int beg, int end)
	{
		int mid;
		if(beg<end)
		{
			mid = (beg+end)/2;
			mergeSort(input,beg,mid);
			mergeSort(input,mid+1,end);
			merge(input,beg,mid,end);
		}
	}
	void merge(long input[], int p, int q, int r)
	{
		int i,j,k;
		int left_length = q-p+1;
		int right_length = r-q;
		long left_array[left_length];
		long right_array[right_length];
		for(i=0; i<left_length; i++)
			left_array[i] = input[p+i];
		for(j = 0; j<right_length; j++)
			right_array[j] = input[q+j+1];
		i=j=0;
		k=p;
		while (i < left_length && j < right_length)
    	{
        if (left_array[i] <= right_array[j])
        {
            input[k] = left_array[i];
            i++;
        }
        else
        {
            input[k] = right_array[j];
            j++;
        }
        mergeSort_Counter++;
        k++;
   	 	}

   	 	/* Copy the remaining elements of L[], if there are any */
    	while (i < left_length)
    	{
        input[k] = left_array[i];
        i++;
        k++;
    	}
 
    /* Copy the remaining elements of R[], if there are any */
    	while (j < right_length)
    	{
        input[k] = right_array[j];
        j++;
        k++;

		}

	}

	//Method to do QuickSort
	void quickSort(long input[], int beg, int end)
	{
		int key;
		if (beg<end)
		{
			key = Partition(input,beg, end);
			quickSort(input, beg, key-1);
			quickSort(input, key+1, end);
		}
	}
	int Partition (long input[], int beg, int end)
	{
		int x,i,temp;
		x = input[end];
		i = beg-1;
		for (int j =beg; j<=end-1; j++)
		{
			//Making sure everything is to left lower and to the right greater
			if(input[j]<=x)
			{
				i = i+1;
				temp = input[j];
				input[j] = input[i];
				input[i] = temp;
			}
			quickSort_Counter++;

		}
		//Putting the key in the correct place 
		temp = input[end];
		input[end] = input[i+1];
		input[i+1] = temp;
		return i+1;
	} 

	int getInsertionCount()
	{
		return insertionSort_Counter;
	}

	int getMergeCount()
	{
		return mergeSort_Counter;
	}

	int getQuickCount()
	{
		return quickSort_Counter;
	}
	void resetCounter()
	{
		mergeSort_Counter=quickSort_Counter=insertionSort_Counter=0;
	}

};
#endif /* SortingAlgorithms_H */