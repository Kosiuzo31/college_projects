#define ARRAY_SIZE(array) (sizeof((array))/sizeof((array[0])))
#include "SortingAlgorithms.h"
#include <stdlib.h>
#include <time.h>
using namespace::std;
using std::endl;
using std::cout;
int main(int argc, char* argv[])
{
	//Creating seed to randomize random number
	long seed;
	//Making sorting object
	SortingAlgorithms sa;

	

	 

	

	//Quick Sort counting 91000
	sa.resetCounter();
	for(int a =0; a<50; a++)
	{
		//Getting random number to change seed
		seed = rand();

		//Array of size 91000
		long input[91000];
		
		//Making sure the random seed is different so it doesnt generate the same number
		srand(time(NULL)+seed);
		
		for(int i =0; i<91000; i++)
		{
			input[i] = rand();
		}

		sa.quickSort(input,0, ARRAY_SIZE(input)-1);
		
	}

	//Average Count for Quick sort
	cout << "Quick Average Count for 91000: " << sa.getQuickCount()/50 <<endl; 

	//Quick Sort counting 92000
	sa.resetCounter(); //reset for the next test
	for(int a =0; a<50; a++)
	{
		//Getting random number to change seed
		seed = rand();

		//Array of size 92000
		long input[92000];
		
		//Making sure the random seed is different so it doesnt generate the same number
		srand(time(NULL)+seed);
		
		for(int i =0; i<92000; i++)
		{
			input[i] = rand();
		}

		sa.quickSort(input,0, ARRAY_SIZE(input)-1);
		
	}
	//Average Count for Quick sort
	cout << "Quick Average Count for 92000: " << sa.getQuickCount()/50 <<endl; 

	//Quick Sort counting 93000
	sa.resetCounter(); //reset for the next test
	for(int a =0; a<50; a++)
	{
		//Getting random number to change seed
		seed = rand();

		//Array of size 2000
		long input[93000];
		
		//Making sure the random seed is different so it doesnt generate the same number
		srand(time(NULL)+seed);
		
		for(int i =0; i<93000; i++)
		{
			input[i] = rand();
		}

		sa.quickSort(input,0, ARRAY_SIZE(input)-1);
		
	}

	//Average Count for Quick sort
	cout << "Quick Average Count for 93000: " << sa.getQuickCount()/50 <<endl; 

	//Quick Sort counting 94000
	sa.resetCounter(); //reset for the next test
	for(int a =0; a<50; a++)
	{
		//Getting random number to change seed
		seed = rand();

		//Array of size 2000
		long input[94000];
		
		//Making sure the random seed is different so it doesnt generate the same number
		srand(time(NULL)+seed);
		
		for(int i =0; i<94000; i++)
		{
			input[i] = rand();
		}

		sa.quickSort(input,0, ARRAY_SIZE(input)-1);
		
	}

	//Average Count for Quick sort
	cout << "Quick Average Count for 94000: " << sa.getQuickCount()/50 <<endl; 

	//Quick Sort counting 95000
	sa.resetCounter(); //reset for the next test
	for(int a =0; a<50; a++)
	{
		//Getting random number to change seed
		seed = rand();

		//Array of size 2000
		long input[95000];
		
		//Making sure the random seed is different so it doesnt generate the same number
		srand(time(NULL)+seed);
		
		for(int i =0; i<95000; i++)
		{
			input[i] = rand();
		}

		sa.quickSort(input,0, ARRAY_SIZE(input)-1);
		
	}

	//Average Count for Quick sort
	cout << "Quick Average Count for 95000: " << sa.getQuickCount()/50 <<endl; 

	//Quick Sort counting 96000
	sa.resetCounter(); //reset for the next test
	for(int a =0; a<50; a++)
	{
		//Getting random number to change seed
		seed = rand();

		//Array of size 2000
		long input[96000];
		
		//Making sure the random seed is different so it doesnt generate the same number
		srand(time(NULL)+seed);
		
		for(int i =0; i<96000; i++)
		{
			input[i] = rand();
		}

		sa.quickSort(input,0, ARRAY_SIZE(input)-1);
		
	}

	//Average Count for Quick sort
	cout << "Quick Average Count for 96000: " << sa.getQuickCount()/50 <<endl; 

	//Quick Sort counting 97000
	sa.resetCounter(); //reset for the next test
	for(int a =0; a<50; a++)
	{
		//Getting random number to change seed
		seed = rand();

		//Array of size 2000
		long input[97000];
		
		//Making sure the random seed is different so it doesnt generate the same number
		srand(time(NULL)+seed);
		
		for(int i =0; i<97000; i++)
		{
			input[i] = rand();
		}

		sa.quickSort(input,0, ARRAY_SIZE(input)-1);
		
	}

	//Average Count for Quick sort
	cout << "Quick Average Count for 97000: " << sa.getQuickCount()/50 <<endl; 

	//Quick Sort counting 98000
	sa.resetCounter(); //reset for the next test
	for(int a =0; a<50; a++)
	{
		//Getting random number to change seed
		seed = rand();

		//Array of size 2000
		long input[98000];
		
		//Making sure the random seed is different so it doesnt generate the same number
		srand(time(NULL)+seed);
		
		for(int i =0; i<98000; i++)
		{
			input[i] = rand();
		}

		sa.quickSort(input,0, ARRAY_SIZE(input)-1);
		
	}

	//Average Count for Quick sort
	cout << "Quick Average Count for 98000: " << sa.getQuickCount()/50 <<endl; 

	//Quick Sort counting 99000
	sa.resetCounter(); //Reset for the next test
	for(int a =0; a<50; a++)
	{
		//Getting random number to change seed
		seed = rand();

		//Array of size 99000
		long input[99000];
		
		//Making sure the random seed is different so it doesnt generate the same number
		srand(time(NULL)+seed);
		
		for(int i =0; i<99000; i++)
		{
			input[i] = rand();
		}

		sa.quickSort(input,0, ARRAY_SIZE(input)-1);
		
	}

	//Average Count for Quick sort
	cout << "Quick Average Count for 99000: " << sa.getQuickCount()/50 <<endl;

	//Quick Sort counting 100000
	sa.resetCounter(); //Reset for the next counter
	for(int a =0; a<50; a++)
	{
		//Getting random number to change seed
		seed = rand();

		//Array of size 100000
		long input[100000];
		
		//Making sure the random seed is different so it doesnt generate the same number
		srand(time(NULL)+seed);
		
		for(int i =0; i<100000; i++)
		{
			input[i] = rand();
		}

		sa.quickSort(input,0, ARRAY_SIZE(input)-1);
		
	}

	//Average Count for insertion sort
	cout << "Quick Average Count for 100000: " << sa.getQuickCount()/50 <<endl;

// 	//END OF Quick SORTING 
	return 0;
} //End of main 