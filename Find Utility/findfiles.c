#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
void search(char* file, const char* directoryInput)
{
	DIR *directory;
	struct dirent *entry;
	directory=opendir(directoryInput);
	if(directory == NULL)
	{
		printf("Unable to open directory. Exiting program...\n");
		exit(0);
	}
	while(1)
	{
		entry=readdir(directory);
		chdir(directoryInput);
		if (entry == NULL)
			break;
		if(entry->d_type == DT_DIR && strcmp(entry->d_name, ".") !=0 && strcmp(entry->d_name, "..") !=0)
		{
		char path[PATH_MAX +1];
		char *pathName=realpath(entry->d_name,path);
		search(file,pathName);
		}
		if(strcmp(entry->d_name, file) == 0)
			{
			char *currentDirectory;
			currentDirectory = getcwd(currentDirectory,PATH_MAX);
		printf("File %s found in directory %s\n", file,currentDirectory);
			}
	}
	closedir(directory);
}
int main( int argc, char *argv[])
{
	if(argc !=3)
	{
		printf("usage: findfile <filename> <starting directory path>\n");
		exit(0);
	}
	search(argv[1],argv[2]);
	return 0;
}
