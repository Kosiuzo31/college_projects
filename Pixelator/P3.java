import java.util.*;
import java.io.*;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
public class P3{
	public static void main(String args[])
	{
	//Creating frame to take in pane
	JFrame frame = new JFrame("Pixelator");
	//Making sure that the user does not input and parameters 
	if (args.length >0)
	{
		System.out.println("They entered too many parameters");
		JOptionPane.showMessageDialog(frame,"You cannot enter parameters with this program","Parameter Warning",JOptionPane.ERROR_MESSAGE);
		System.exit(0);
	}
	//Setting the program to close on exit 
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	//Contructors for panel
	GUI panel = new GUI();
	
	//Putting the menuBar into the frame
	frame.setJMenuBar(panel.getMenu());
	frame.getContentPane().add(panel);

	//Making the frame visible and fitting any screen
	frame.pack();
	frame.setVisible(true);

	}
}
