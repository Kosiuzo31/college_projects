import java.awt.*;
import javax.swing.*;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.image.*;
import java.awt.image.BufferedImage;
import java.awt.image.*;
import java.io.File;
import javax.imageio.*;
import java.awt.event.*;
import javax.swing.JDialog;
public class GUI extends JPanel
{
	//Boolean to control repaint for getImage
	private Boolean on = false;

	//Button Group for radio button
	ButtonGroup group;

	//RadioButton for buttonGroup
	private JRadioButton five, ten, fifteen, random10;

	//Button for reset image
	private JButton reset;

	//File Chooser and file extension filter for png and JPG files
	private JFileChooser chooser = new JFileChooser();
	FileNameExtensionFilter filter = new FileNameExtensionFilter( "JPG & PNG Images", "jpg", "png");
	
	//Label and image to control displaying the buffered image
	private ImageIcon displayImage;
	private JLabel imageLabel;
	
	//Buffered Image variables 
	private int imageHeight, imageWidth;
	private final int Pixel_Size1 = 5, Pixel_Size2 = 10, Pixel_Size3 =15;

	//BufferedImage and File variables. There is two so you can always have the original
	private BufferedImage pixelImage, originalImage;
	//Created Image objects so I can scale the buffered image
	private Image scaledPImage, scaledOImage;
	private File file;

	//Making menuBar and items
	public JMenuBar menuBar;
	public JMenuItem itemFile, itemQuit, itemHelpStart, itemAbout;
	
	
	//Methods display fileChooser and puts image in bufferedImage
	public void getImage()
	{
		try{
		chooser.setFileFilter(filter);
		int returnVal = chooser.showOpenDialog(null);
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			System.out.println("You opened this file: " + chooser.getSelectedFile().getName());
			file = chooser.getSelectedFile();
			String fileName = file.getName();
			originalImage = ImageIO.read(file);
			scaledOImage = originalImage.getScaledInstance(520,520,Image.SCALE_SMOOTH);
			pixelImage = ImageIO.read(file);
			scaledPImage = pixelImage.getScaledInstance(520,520,Image.SCALE_SMOOTH);
			if(on)
			{
				displayImage = new ImageIcon(scaledOImage);
				group.clearSelection();
				repaint();
			}
		}
		else if(returnVal == JFileChooser.CANCEL_OPTION)
		{
			if(on)
			{
				chooser.cancelSelection();
			}
			else
			{
				System.out.println("The user choose cancel");
				System.exit(0);
			}
		}

		}
		catch(NullPointerException e)
		{
			System.exit(0);
		}
		catch(Exception e){
			System.out.println("There was an error");
			System.exit(0);
		}
	}

	

	//Method returns Menu to be added in the frame
	public JMenuBar getMenu()
	{
	MenuListener listener = new MenuListener();
	menuBar = new JMenuBar();
	JMenu fileMenu = new JMenu("File");
	fileMenu.setMnemonic(KeyEvent.VK_F);
    menuBar.add(fileMenu);
	itemFile = new JMenuItem("Choose an Image File");
    itemFile.setMnemonic(KeyEvent.VK_C);
	itemFile.addActionListener(listener);
	itemQuit = new JMenuItem("Quit");
	itemQuit.addActionListener(listener);
	itemQuit.setMnemonic(KeyEvent.VK_Q);
    fileMenu.add(itemFile);
    fileMenu.add(itemQuit);
	JMenu helpMenu = new JMenu("Help");
	helpMenu.setMnemonic(KeyEvent.VK_H);
	menuBar.add(helpMenu);
	itemHelpStart = new JMenuItem("Get Started");
	itemHelpStart.setMnemonic(KeyEvent.VK_G);
	itemHelpStart.addActionListener(listener);
	itemAbout = new JMenuItem("About Options");
	itemAbout.setMnemonic(KeyEvent.VK_A);
	itemAbout.addActionListener(listener);
	helpMenu.add(itemHelpStart);
	helpMenu.add(itemAbout);
	return menuBar;
	}

	private class MenuListener implements ActionListener 
	{
		public void actionPerformed(ActionEvent event)
		{
			Object source = event.getSource();
			if(source == itemQuit)
				System.exit(0);
			else if(source == itemFile)
			{
				on = true;
				getImage();
			}
			else if(source == itemHelpStart)
			{
				JOptionPane.showMessageDialog(null, "To start pick one of the pixelation choices and press reset to undo any pixelation choices." + 
					"\n\nMENUBAR OPTIONS\nFile: Lets you choose a new file \nQuit: Exits the program" +
					 "\nGetStarted: Tell you information on how to use the program\n" + 
					 "About Options: tells you about the program", "Getting Started", JOptionPane.INFORMATION_MESSAGE);
				
			}
			else 
				JOptionPane.showMessageDialog(null, "Version: 1.00\nAuthor: Kosi Uzodinma" +
				 "\nCreated with JSWING and Awt", "About Options", JOptionPane.INFORMATION_MESSAGE);
			
			
		}
	}


	//Displays the orginal image when the user presses reset
	public void displayOriginal()
	{
		displayImage = new ImageIcon(scaledOImage); 
		repaint();
	}

	//Causes the image to be pixelated by 5
	public void displayPixel5()
	{
		Color inColor, outColor;
		int red, blue, green, outRGB;
		//int pixelRGB = 0;
		imageHeight = originalImage.getHeight();
		imageWidth = originalImage.getWidth();
		//pixelImage = originalImage;
		int minX = originalImage.getMinX();
		int minY = originalImage.getMinY();
		
		for(int x = minX ; x<imageWidth; x+=Pixel_Size1)
		{
			for(int y = minY; y<imageHeight; y+=Pixel_Size1)
			{
				int pixelRGB=0;
				int count =0;
				red = 0;
				blue = 0;
				green =0;
				outRGB = 0;
				//Inner loop to get the average
				
				for(int xa = x; (xa<x+Pixel_Size1 && xa<imageWidth); xa++)
				{
					for(int ya = y; (ya<y+Pixel_Size1 && ya<imageHeight); ya++)
					{
						inColor = new Color(originalImage.getRGB(xa,ya));
						//pixelRGB += originalImage.getRGB(xa,ya);
						red +=inColor.getRed();
						blue +=inColor.getBlue();
						green +=inColor.getGreen();
						count++;
						
					}
					
				}//End of double loop
					red = red/count;
					blue = blue/count;
					green = green/count;
					outColor = new Color(red,green,blue);
					outRGB = outColor.getRGB();
					//System.out.println(outRGB);
				//Loop to set the color to the average
				for(int xa = x; (xa<x+Pixel_Size1 && xa<imageWidth); xa++)
				{
					for(int ya = y; (ya<y+Pixel_Size1 && ya<imageHeight); ya++)
					{
						pixelImage.setRGB(xa,ya,outRGB);
						
					}
					
				}//End of double loop
				

			}
		}//End of double loop
		scaledPImage = pixelImage.getScaledInstance(520,520,Image.SCALE_SMOOTH);
		displayImage = new ImageIcon(scaledPImage); 
		repaint();

	}

	//Causes pixelation by 10
	public void displayPixel10()
	{
		Color inColor, outColor;
		int red, blue, green, outRGB;
		//int pixelRGB = 0;
		imageHeight = originalImage.getHeight();
		imageWidth = originalImage.getWidth();
		//pixelImage = originalImage;
		int minX = originalImage.getMinX();
		int minY = originalImage.getMinY();
		
		for(int x = minX ; x<imageWidth; x+=Pixel_Size2)
		{
			for(int y = minY; y<imageHeight; y+=Pixel_Size2)
			{
				int pixelRGB=0;
				int count =0;
				red = 0;
				blue = 0;
				green =0;
				outRGB = 0;
				//Inner loop to get the average
				
				for(int xa = x; (xa<x+Pixel_Size2 && xa<imageWidth); xa++)
				{
					for(int ya = y; (ya<y+Pixel_Size2 && ya<imageHeight); ya++)
					{
						inColor = new Color(originalImage.getRGB(xa,ya));
						//pixelRGB += originalImage.getRGB(xa,ya);
						red +=inColor.getRed();
						blue +=inColor.getBlue();
						green +=inColor.getGreen();
						count++;
						
					}
					
				}//End of double loop
					red = red/count;
					blue = blue/count;
					green = green/count;
					outColor = new Color(red,green,blue);
					outRGB = outColor.getRGB();
				//Loop to set the color to the average
				for(int xa = x; (xa<x+Pixel_Size2 && xa<imageWidth); xa++)
				{
					for(int ya = y; (ya<y+Pixel_Size2 && ya<imageHeight); ya++)
					{
						pixelImage.setRGB(xa,ya,outRGB);
						
					}
					
				}//End of double loop
				

			}
		}//End of double loop
		scaledPImage = pixelImage.getScaledInstance(520,520,Image.SCALE_SMOOTH);
		displayImage = new ImageIcon(scaledPImage);
		repaint();

	}

	//Causes pixelation by 10
	public void displayPixel15()
	{
		Color inColor, outColor;
		int red, blue, green, outRGB;
		//int pixelRGB = 0;
		imageHeight = originalImage.getHeight();
		imageWidth = originalImage.getWidth();
		//pixelImage = originalImage;
		int minX = originalImage.getMinX();
		int minY = originalImage.getMinY();
		
		for(int x = minX ; x<imageWidth; x+=Pixel_Size3)
		{
			for(int y = minY; y<imageHeight; y+=Pixel_Size3)
			{
				int pixelRGB=0;
				int count =0;
				red = 0;
				blue = 0;
				green =0;
				outRGB = 0;
				//Inner loop to get the average
				
				for(int xa = x; (xa<x+Pixel_Size3 && xa<imageWidth); xa++)
				{
					for(int ya = y; (ya<y+Pixel_Size3 && ya<imageHeight); ya++)
					{
						inColor = new Color(originalImage.getRGB(xa,ya));
						//pixelRGB += originalImage.getRGB(xa,ya);
						red +=inColor.getRed();
						blue +=inColor.getBlue();
						green +=inColor.getGreen();
						count++;
						
					}
					
				}//End of double loop
					red = red/count;
					blue = blue/count;
					green = green/count;
					outColor = new Color(red,green,blue);
					outRGB = outColor.getRGB();
				//Loop to set the color to the average
				for(int xa = x; (xa<x+Pixel_Size3 && xa<imageWidth); xa++)
				{
					for(int ya = y; (ya<y+Pixel_Size3 && ya<imageHeight); ya++)
					{
						pixelImage.setRGB(xa,ya,outRGB);
						
					}
					
				}//End of double loop
				

			}
		}//End of double loop
		scaledPImage = pixelImage.getScaledInstance(520,520,Image.SCALE_SMOOTH);
		displayImage = new ImageIcon(scaledPImage);
		repaint();

	}
	//Causes multiple colors to get on the image. Very cool effect when used with a sword
	public void randomEffect10()
	{
		Color inColor, outColor;
		int red, blue, green, outRGB;
		int pixelRGB = 0;
		imageHeight = originalImage.getHeight();
		imageWidth = originalImage.getWidth();
		//pixelImage = originalImage;
		int minX = originalImage.getMinX();
		int minY = originalImage.getMinY();
		
		for(int x = minX ; x<imageWidth; x+=Pixel_Size2)
		{
			for(int y = minY; y<imageHeight; y+=Pixel_Size2)
			{
				pixelRGB=0;
				//Inner loop to get the average
				
				for(int xa = x; (xa<x+Pixel_Size2 && xa<imageWidth); xa++)
				{
					for(int ya = y; (ya<y+Pixel_Size2 && ya<imageHeight); ya++)
					{
						//inColor = new Color(originalImage.getRGB(xa,ya));
						pixelRGB += originalImage.getRGB(xa,ya)/10;
						// red +=inColor.getRed();
						// blue +=inColor.getBlue();
						// green +=inColor.getGreen();
						//count++;
						
					}
					
				}//End of double loop
					// red = red/count;
					// blue = blue/count;
					// green = green/count;
					outColor = new Color(pixelRGB);
					outRGB = outColor.getRGB();
				//Loop to set the color to the average
				//pixelRGB = pixelRGB/5;
				for(int xa = x; (xa<x+Pixel_Size2 && xa<imageWidth); xa++)
				{
					for(int ya = y; (ya<y+Pixel_Size2 && ya<imageHeight); ya++)
					{
						pixelImage.setRGB(xa,ya,outRGB);
						
					}
					
				}//End of double loop
				

			}
		}//End of double loop
		scaledPImage = pixelImage.getScaledInstance(520,520,Image.SCALE_SMOOTH);
		displayImage = new ImageIcon(scaledPImage);
		repaint();

	}
	//Contructor for the GUI that controls all the logic and actions to happen
	public GUI()
	{
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		getImage();
		displayImage = new ImageIcon(scaledOImage);
		imageLabel = new JLabel(displayImage);
		add(imageLabel);
		imageLabel.setPreferredSize(new Dimension(520,520));
		imageLabel.setMaximumSize(new Dimension(520,520));
		setRadioButton();
		add(five);
		add(ten);
		add(fifteen);
		add(random10);
		makeButton();
		add(reset);
		setPreferredSize(new Dimension(700,660));

	}
	//Overriding the paintcompent so I can use repaint 
	public void paintComponent(Graphics page)
	{
		super.paintComponent(page);
		imageLabel.setIcon(displayImage);
	}
	//Methods set up radio buttons
	public void setRadioButton()
	{
		//Making Radio buttons
		five= new JRadioButton("5 Pixel Pixelation");
		ten= new JRadioButton("10 Pixel Pixelation");
		fifteen= new JRadioButton("15 Pixel Pixelation");
		random10 = new JRadioButton("Random Effect");

		//Creating button group
		group = new ButtonGroup();
		group.add(five);
		group.add(ten);
		group.add(fifteen);
		group.add(random10);
		RadioListener listener = new RadioListener();
		//Adding listener to radioButton
		five.addActionListener(listener);
		ten.addActionListener(listener);
		fifteen.addActionListener(listener);
		random10.addActionListener(listener);

	}

	//Listener for radio buttons
	public class RadioListener implements ActionListener
	{
		//Set the image to desired pixelation

		public void actionPerformed(ActionEvent event)
		{
			Object source = event.getSource();
			
			if(source == five)
				displayPixel5();
			else if(source == ten)
				displayPixel10();
			else if(source == fifteen)
				displayPixel15();
			else if(source == random10)
				randomEffect10();
		}
	}

	//Makes Reset button
	public void makeButton()
	{
		reset = new JButton("Reset");
		reset.setMnemonic(KeyEvent.VK_R);
		reset.setToolTipText("Resets the image back to normal");
		ButtonListener listener = new ButtonListener();
		reset.addActionListener(listener);
	}
	//Listener for Reset Button
	public class ButtonListener implements ActionListener
	{
		//Resets image back to the original 
		public void actionPerformed(ActionEvent event)
		{
			Object source = event.getSource();
			if(source == reset)
			{
				group.clearSelection();
				displayOriginal();
			}
		}
	}

	
}
