/* Author: Kosi Uzodinma 
* Submission Date: February 11, 2015  
* Class Purpose: To learn systems Programming and practice string manipulation  
* Statement of Academic Honesty: 
* 
* The following code represents my own work. I have neither 
*  received nor given inappropriate assistance. I have not copied 
*  or modified code from any source other than the course webpage 
*  or the course textbook. I recognize that any unauthorized  
*  assistance or plagiarism will be handled in accordance with 
*  the University of Georgia's Academic Honesty Policy and the 
*  policies of this course. I recognize that my work is based 
*  on an assignment created by the Department of Computer 
*  Science at the University of Georgia. Any publishing 
*  or posting of source code for this project is strictly 
*  prohibited unless you have written consent from the Department 
*  of Computer Science at the University of Georgia. 
* */
#include <stdio.h>
int main()
{
	//Variable to control welcome statement
	int firstRun=0;
	while(1)
	{
			//Declaring variables needed for this project
			char operator = '0', userInput[50], outputString[10], leftString[10], rightString[10], exitString[] = "exit";
			int exitSuccess =0, capitalError=0, missingOperation_Error=0, count_Error =0,leftSideCount =0, correctString =0, rightSideCount =0, leftSide =1, rightSide=0, CorrectFormat=0, numOfOperations =0, operation_Error=0,successfulCount=0, i=0;	
			
			if(firstRun)
			{
			//Intro statement to the user
			printf("Welcome!! to String Math to play you follow a couple simple rules. Input must be 1-9 lowercase letters on both sides with a math operation(*,/,+,-) in between.\n");
			printf("Sample inputs: dog*cat, turtle/frog, aa+bb, apple-basic \nGive it a try if not type exit to leave the program\n");
			}
			printf("Input: ");	

			//scanning input from user
			scanf("%s", &userInput);

			//Loop to keep getting input
			while(!correctString)
			{
				int o=0;
			if(userInput[0] == 'e')
			{
				while(exitString[o] !='\0')
				{
					if(userInput[o] == exitString[o])
						exitSuccess = 1;
					else
					{
						exitSuccess =0;
						exitSuccess = 0;
						break;
					}
					o++;
				}
			}
			if(exitSuccess)
			{
				printf("You have exited the program\n");
				return 0;
			}
			//Loop that checks to make sure that string is correctly inputted
			while(userInput[i] != '\0')
			{
				//Catching error of captial Letter
				if( (userInput[i]>64 && userInput[i]<91) && !capitalError)
					capitalError = 1; 
				
				//Counting operations to make sure it is one
				else if( (userInput[i] == '*') || (userInput[i] == '-') ||  (userInput[i] == '/') || (userInput[i] == '+') )
					{
					operator = userInput[i];
					numOfOperations++;
					}
				
				//Making sure string has correct format of SOS
				if(numOfOperations == 1 && !CorrectFormat)
				{
					if(userInput[i+1]>96 && userInput[i+1]<123)
						
						CorrectFormat =1;
				}
				
				//Catching error of too many operations
				else if(numOfOperations>1 && !operation_Error)  
					operation_Error =1;
				
				//Making sure there are 1-9 letters on both sides
				if(CorrectFormat && !successfulCount)
				{
					int j=0;
					while( userInput[j] != '\0') 
					{
						if ((userInput[j] == '*') || (userInput[j] == '-') ||  (userInput[j] == '/') || (userInput[j] == '+'))
							{
							rightSide=1;
							leftSide=0;
							j++;
							}
						if(leftSide)
							leftSideCount++;
						if(rightSide)
							rightSideCount++;
							j++;
					}
					if(leftSideCount<=9 && rightSideCount<=9)
						successfulCount =1;
					else
						count_Error = 1;
				}

				i++;

			}//End of while loop checking for errors
			if(numOfOperations==0)
				missingOperation_Error =1;
			if(successfulCount && CorrectFormat && !capitalError && !operation_Error)
				correctString = 1;
			else
			{
				if(capitalError)
					printf("Bad Input - Captial Letter Error\n");
				if(operation_Error)
					printf("Bad Input - Too many operations\n");
				if(count_Error)
					printf("Bad Input - Too many letters\n");
				if(numOfOperations == 0)
					printf("Bad Input - Missing math operation\n");
				if((leftSideCount ==0 || rightSideCount == 0) && !CorrectFormat)
					printf("Bad Input - Missing string on either side\n");
				printf("Input: ");
				scanf("%s", &userInput);
					
				//Resetting variables to recheck errors
				numOfOperations =0, i=0, capitalError =0, CorrectFormat =0, operation_Error=0, leftSideCount =0, rightSide =0,leftSide=1, successfulCount =0, rightSideCount=0;
			}

			}//End of while loop for input
			
			//Variables to keep track of arrays in while loop
			int j =0, k =0;
			
			//resetting so it can keep track of which array to input letters into
			leftSide =1, rightSide=0;

			//Loop to put letters into separate arrays
			while( userInput[j] != '\0') 
			{
				if ((userInput[j] == '*') || (userInput[j] == '-') ||  (userInput[j] == '/') || (userInput[j] == '+'))
				{
				rightSide=1;
				leftSide=0;
				k=0;
				j++;
				}
				if(leftSide)
				{
					leftString[k]=userInput[j];
					k++;
				}

				if(rightSide)
				{
					rightString[k]=userInput[j];
					k++;
				}

				j++;
			}//End of while loop of putting letters into arrays
			
			//Variables for controlling string math
			int x =0, leftNumber =0, rightNumber =0, finalCount =0, value =0;
			
			//Decision of how long final output string will be
			if(leftSideCount>rightSideCount)
				finalCount = leftSideCount;
			else
				finalCount = rightSideCount;
			//Loop to add strings
			while( x<finalCount)
			{
				leftNumber = leftString[x]-96;
				rightNumber = rightString[x]-96;
				if(leftSideCount == rightSideCount)
				{	
				if(operator == '/')
				{
					value = leftNumber/rightNumber;
					if(value>0 && value<27)
						outputString[x]= value +96;
					else
						outputString[x]= (leftNumber +96)-32;

				}
				else if(operator == '+')
				{
					value = leftNumber+rightNumber;
					if(value>0 && value<27)
						outputString[x]= value +96;
					else
						outputString[x]= (leftNumber +96)-32;
				}
				else if(operator == '-')
				{	
					value = leftNumber-rightNumber;
					if(value>0 && value<27)
						outputString[x]= value +96;
					else
						outputString[x]= (leftNumber +96)-32;
				}
				else if(operator == '*')
				{
					value = leftNumber*rightNumber;
					if(value>0 && value<27)
						outputString[x]= value +96;
					else
						outputString[x]= (leftNumber +96)-32;
				}
				}

				else if(leftSideCount>rightSideCount)
				{
					
				if(operator == '/')
				{
					value = leftNumber/rightNumber;
					if(x<rightSideCount)
					{
					if(value>0 && value<27)
						outputString[x]= value +96;
					else
						outputString[x]= (leftNumber +96)-32;
						
					}
					else
						outputString[x]= (leftNumber +96);

				}
				else if(operator == '+')
				{
					value = leftNumber+rightNumber;
					if(x<rightSideCount)
					{
					if(value>0 && value<27)
						outputString[x]= value +96;
					else
						outputString[x]= (leftNumber +96)-32;
					}
					else
						outputString[x]= (leftNumber +96);
				}
				else if(operator == '-')
				{	
					value = leftNumber-rightNumber;
					if(x<rightSideCount)
					{
					if(value>0 && value<27)
						outputString[x]= value +96;
					else
						outputString[x]= (leftNumber +96)-32;
					}
					else
						outputString[x]= (leftNumber +96);
				}
				else if(operator == '*')
				{
					value = leftNumber*rightNumber;
					if(x<rightSideCount)
					{
					if(value>0 && value<27)
						outputString[x]= value +96;
					else
						outputString[x]= (leftNumber +96)-32;
					}
					else
						outputString[x]= (leftNumber +96);
				}
				}
				else if(rightSideCount>leftSideCount)
				{

				if(operator == '/')
				{
					value = leftNumber/rightNumber;
					if(x<leftSideCount)
					{
					if(value>0 && value<27)
						outputString[x]= value +96;
					else
						outputString[x]= (leftNumber +96)-32;
					}
					else
						outputString[x]= (rightNumber +96);

				}
				else if(operator == '+')
				{
					value = leftNumber+rightNumber;
					if(x<leftSideCount)
					{
					if(value>0 && value<27)
						outputString[x]= value +96;
					else
						outputString[x]= (leftNumber +96)-32;
					}
					else
						outputString[x]= (rightNumber +96);
				}
				else if(operator == '-')
				{	
					value = leftNumber-rightNumber;
					if(x<leftSideCount)
					{
					if(value>0 && value<27)
						outputString[x]= value +96;
					else
						outputString[x]= (leftNumber +96)-32;
					}
					else
						outputString[x]= (rightNumber +96);
				}
				else if(operator == '*')
				{
					value = leftNumber*rightNumber;
					if(x<leftSideCount)
					{
					if(value>0 && value<27)
						outputString[x]= value +96;
					else
						outputString[x]= (leftNumber +96)-32;
					}
					else
						outputString[x]= (rightNumber +96);
				}
				}
				x++; 
			}
			outputString[x] = '\0';
			printf("Output: %s\n", outputString);
			firstRun =0;
		}

		return 0;
}
