Author: Kosi Uzodinma 
This program allows you to use string math of the letters from 1-26. Every letter corresponds to its value from 1-26.

How to use program:
To run: make
To compile: make compile
To clean: make clean

