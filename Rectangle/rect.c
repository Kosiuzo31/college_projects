#include <stdio.h>
#include <stdlib.h>
#include "rect.h"
void InitRect(struct rect *rectangle)
{
	int input_1,input_2;
 	printf("Please enter the South West Coordinates separated by a space: " );	
 	scanf("%d", &input_1);
	scanf("\n%d", &input_2);
	rectangle->sw.x=input_1;
	rectangle->sw.y=input_2;
 	printf("Please enter the North West Coordinates separated by a space: " );	
 	scanf("%d", &input_1);
	scanf("\n%d", &input_2);
	rectangle->nw.x=input_1;
	rectangle->nw.y=input_2;
 	printf("Please enter the North East Coordinates separated by a space: " );	
 	scanf("%d", &input_1);
	scanf("\n%d",&input_2);
	rectangle->ne.x=input_1;
	rectangle->ne.y=input_2;
 	printf("Please enter the South East Coordinates separated by a space: " );	
 	scanf("%d", &input_1);
	scanf("\n%d",&input_2);
	rectangle->se.x=input_1;
	rectangle->se.y=input_2;
}
int ValidateRect(struct rect rectangle)
{
	int invalid = 0;
	if(rectangle.ne.y != rectangle.nw.y)
		invalid =1;
	else if(rectangle.se.y != rectangle.sw.y)
		invalid =1;
	else if(rectangle.sw.x != rectangle.nw.x)
		invalid =1;
	else if(rectangle.se.x != rectangle.ne.x)
		invalid =1;
	if(invalid)
		return 0;
	else
		return 1;

}
int RectArea(struct rect rectangle)
{
 	int length, width;
 	length = abs(rectangle.se.x-rectangle.sw.x);
	width = abs(rectangle.nw.y-rectangle.sw.y);
	return length*width;
}
void RotateRect(struct rect *rectangle)
{
	int x,y;
	x = (*rectangle).ne.x;
	y = (*rectangle).ne.y;
	(*rectangle).ne.x = (*rectangle).nw.x;
	(*rectangle).ne.y = (*rectangle).nw.y;
	(*rectangle).nw.x = (*rectangle).sw.x;
	(*rectangle).nw.y = (*rectangle).sw.y;
	(*rectangle).sw.x = (*rectangle).se.x;
	(*rectangle).sw.y = (*rectangle).se.y;
	(*rectangle).se.x = x;
	(*rectangle).se.y = y;
}
void PrintRect(struct rect rectangle)
{
	printf("\nSouth West Coordinates: (%d, %d)", rectangle.sw.x, rectangle.sw.y);
	
	printf("\nNorth West Coordinates: (%d, %d)", rectangle.nw.x, rectangle.nw.y);
	printf("\nNorth East Coordinates: (%d, %d)", rectangle.ne.x, rectangle.ne.y);
	printf("\nSouth East Coordinates: (%d, %d)\n", rectangle.se.x,
	rectangle.se.y);
}
