#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int generateRandom(int inputDif);
void playGame(int choiceInput);

//Variables I will need for multiple functions
int answerInput=0,outputSecretNum =0, guesses=0, orig_Divisor=0, min=0,max=0, numDigits=0, againInput=0;

//Funtion to play game
int main ()
{
	while(1)
	{
		//Setting variables that will be needed throughout function	
		int diffInput=0,userGuess=0,secretNum=4,testingMode=1,sum=0,numCorrect=0,divisor=0,secret_Val =0, user_Val=0;
		//Function to introduct gam
		if(againInput ==0)
		playGame(againInput);
		//User can choose difficulty and make sure it is valid choice
		printf("Please enter the level of difficulty (beginner (1), intermediate (2), advanced (3)) " );
		scanf("%d", &diffInput); 
		while(diffInput!=1 && diffInput!=2 && diffInput!=3)
		{		
			printf("Sorry, this is not a Correct level. ");
			printf("Please enter the level of difficulty (beginner (1), intermediate (2), advanced (3)) " );
			scanf("%d", &diffInput);	
		}
		//Gets secret number from secret number function
		secretNum = generateRandom(diffInput);
		//Allows the user to keep guessing a number
		while(secretNum != userGuess)
		{
			//Testing mode turn on to display number
			if(testingMode == 1)
			{	
			printf("Testing Mode - the %d digits number: %d\n", numDigits, secretNum);	
			}
			//Get the guess from the user and make sure it is valid
			printf("Please guess the %d %s ",  numDigits, " digits number: ");
			scanf("%d", &userGuess);
			while((userGuess < min)	|| (userGuess>max))
			{
				printf("Your guess you have entered is ill-formed.\nPlease guess the %d digits number: ", numDigits);
				scanf("%d", &userGuess);
			}
			//Decretment guesses from level difficulty 
			guesses--;
			//Set the variables to handles the logic of comparing the number
			secret_Val=secretNum, user_Val=userGuess, sum=numCorrect=0,divisor=orig_Divisor;
			//If they get the number right the user win and the loop breaks to play again
			if(secretNum==userGuess)
			{
				printf("You won!!!\n");
				againInput=1;
				break;
			}
			//Compares the numbers and get the sum and number of correct digits
			while(divisor>0)
			{
				if(secret_Val/divisor == user_Val/divisor)
				{
					sum +=secret_Val/divisor;
					numCorrect++;
				}
				secret_Val=secret_Val%divisor;
				user_Val=userGuess%divisor;
				divisor= divisor/10;
			}
			
			//Prints the number correct and the sum of those numbers
			printf("Number of correct digits: %d\nSum: %d\n", numCorrect, sum);
			
			//Tells the user they have losted from running out guesses
			if(guesses == 0)
			{
				printf("Sorry, you have lost!!!\n");
				againInput=1;
				break;
			}
		
		}//The end of the guess loop

	playGame(againInput);

	}//The end of controlling while statement 

}//The end of the main function


//Function generates a random number for the secret number
int generateRandom(int inputDif)
{
	int rand(void);
	time_t t;
	srand((unsigned) time(&t));
	if(inputDif ==1)
	{
		outputSecretNum = rand() % 900 + 100, guesses = 30, orig_Divisor = 100, min = 100, max=999,numDigits =3;
	}

	else if(inputDif ==2)
	{
		outputSecretNum = rand() % 9000 + 1000, guesses = 20, orig_Divisor = 1000, min =1000, max =9999, numDigits = 4;
	} 

	else if(inputDif ==3)
	{
		outputSecretNum = rand() % 90000 + 10000, guesses = 10, orig_Divisor = 10000, min =10000, max = 99999,  numDigits = 5;
	}
	return outputSecretNum;
}//End of function

//Function takes input to decide which play game statement to display
void playGame(int choiceInput)
{
	if(againInput == 0)
	{
		printf("Would you like to play Mastermind (Enter 1 for yes, 0 for no)? ");
		scanf("%d", &answerInput);

		while(answerInput!=0 && answerInput!=1)
	{		
		printf("You can only enter 0 or 1\n");
		printf("Would you like to play Mastermind (Enter 1 for yes, 0 for no)? ");
		scanf("%d", &answerInput);	
	}

	if(answerInput == 0)
	{
		printf("The game has terminated.   Goodbye.\n");
		exit(0);
	}

	}
	else
	{
		printf("Would you like to play again (Enter 1 for yes, 0 for no)? ");	
		scanf("%d", &answerInput);
		while(answerInput!=0 && answerInput!=1)
		{		
			printf("You can only enter 0 or 1\n");
			printf("Would you like to play again (Enter 1 for yes, 0 for no)? ");
			scanf("%d", &answerInput);	
		}
		if(answerInput == 0)
		{
			printf("The game has terminated.   Goodbye.\n");
			exit(0);
		}
	}

}//End of function
