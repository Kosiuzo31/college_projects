/*
        Filename: rot13lib.c
        Description: rot13 implementation
*/

#include "rot13lib.h"

// rot13 cipher
void rot13(char * _input)
{
	int i=0;
	int charValue =0;
	while(*(_input+i) != '\0')
	{
		charValue = *(_input+i);
		if(charValue >64 && charValue<91)
		{
			if( (charValue+13) < 91)
			*(_input+i) =charValue+13;
			else
			*(_input+i) =65+ ((charValue+13)-91);

		}
		else if( charValue >96 && charValue <123)
		{
			if( (charValue+13) < 123)
			*(_input+i) = charValue+13;
			else
			*(_input+i) =97+ ((charValue+13)-123);

		}
		i++;
	}
}


