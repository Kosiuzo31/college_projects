#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
int main()
{
void ignore(int);
signal(SIGINT,ignore);
//int loopCount =0;
int bytes_read, nbytes=375;
char *input;
int index =0;
input = (char*)malloc(nbytes+1);
while(1)//Erase this line when you are done make while 1
{

	printf(">");
	bytes_read = getline(&input,&nbytes,stdin);
	int i=0, working=0;
	int numTokens=0;
	int background=0;
	while(i<=bytes_read)
	{
		//For the & right after the command
		if((input[i]>32 && input[i]<127) && input[i+1] == '&')
			{
				background =1;
				numTokens++;
				break;
			}
		
		//For the background process after arguments
		if(input[i] == '&')
		{
			background =1;
			break;
		}
		//For it will not go out of bounds
		if(i<bytes_read)
			if(input[i] == ' ' && (input[i+1]>32 && input[i+1]<127))
				numTokens++;
		//Finding the null character at the end
		if(input[i] == '\0')
			numTokens++;
		i++;
	}
	char program[26], *args[16];
	int a=0;
	i=0;
	while(a<numTokens)
	{
		char transfer[26];
		int begin =0;
		while(1)
		{
			if(input[i] == '\0')
				break;
			if(input[i] == '&')
				break;
			else if(input[i]>32 && input[i]<127)
				{
				transfer[begin] = input[i];
				begin++;
				}
			if( (input[i] > 32 && input[i] <127) && ((input[i+1] ==' ') || (input[i+1] == '\0') || (input[i+1] == '&')))
				{
					i++;
					break;
				}
				i++;
		}
		transfer[begin] = '\0';
		if(a ==0)
			strcpy(program,transfer);
		args[a] = strdup(transfer);//FREEEEEE THIS MEMORY WITH A LOOP
		a++;
		//Terminate when the user enter 15
		if(bytes_read == 5)
		{
			if(strcmp(program,"exit") ==0)
			{
					for(index =0; index<numTokens; index++)
						free(args[index]);
				free(input);
				return 0;
			}
				
		}

	}
	args[a]=NULL;
	int pid = fork();
	if(pid == 0)
	{
		int temp = getpid();
		working=execvp(program,args);
		if(working == -1)
		{
			printf("Command not found -- Child process exiting\n");
			kill(temp, SIGTERM);
		}
	}
	else
	{
		if(!background)
			wait(NULL);
	}
	index =0;
	for(index =0; index<=numTokens; index++)
		free(args[index]);
}
free(input);

return 0;
}

void ignore(int signum)
{
	printf("Received ctrl-c\n>");
	fflush(stdout);
}
