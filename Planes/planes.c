/* Author: Kosi Uzodinma
 * * Submission Date: April 19, 2015
 * * Class Purpose: To learn systems Programming and practice IPC
 * * Statement of Academic Honesty:
 * *
 * * The following code represents my own work. I have neither
 * *  received nor given inappropriate assistance. I have not copied
 * *  or modified code from any source other than the course webpage
 * *  or the course textbook. I recognize that any unauthorized
 * *  assistance or plagiarism will be handled in accordance with
 * *  the University of Georgia's Academic Honesty Policy and the
 * *  policies of this course. I recognize that my work is based
 * *  on an assignment created by the Department of Computer
 * *  Science at the University of Georgia. Any publishing
 * *  or posting of source code for this project is strictly
 * *  prohibited unless you have written consent from the Department
 * *  of Computer Science at the University of Georgia.
 * * */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>
struct planes
{
int id;
struct planes *next;
};
int fuel;
int bombid;
int refuelid;
struct planes *head;
struct planes *controller;
int main()
{
void parent1(int);
void parent2(int);
void child1(int);
void child2(int);
head = (struct planes *)malloc(sizeof(struct planes));
head->next = 0;
controller = head;
signal(SIGCHLD,parent1);
signal(SIGUSR2,parent2);
int base = getpid();
int planeid;
setbuf(stdout, NULL);
char input[20];
while(1)
{
controller=head;
//Making the controller start on the next node 
if(controller !=0) {
	while(controller->next !=0)
	{
		controller = controller->next;
	}
}
//Prompt for the user
printf("Command:");
scanf("\n%s", &input);
if(strcmp(input, "quit") ==0)
{
	free(head);
	while(head !=NULL)
	{
		if(head->id !=0)
		{
		kill(head->id, SIGTERM);
		free(head);
		}
		head = head->next;
	}
	return 0;
}
//four commands you can use
if(strcmp(input, "launch")!=0 && strcmp(input, "status")!=0 && strcmp(input, "bomb")!=0 && strcmp(input, "refuel")!=0)
{
	printf("You can only use the following commands launch, status, bomb N, refuel N, or quit\n");
}
//launch command
if(strcmp(input, "launch") == 0)
{
//child process started
planeid = fork();
if(planeid == 0)
{
int counter=0;
signal(SIGUSR1,child1);
signal(SIGUSR2,child2);	
fuel=100;
while(fuel>0)
{

if(fuel<50 && counter ==3)
{
	printf("***Bomber %d to base, %d fuel left***\nCommand:", getpid(), fuel);
}
if(counter ==3)
	{
		counter =0;
	}
fuel -=5;
sleep(3);
counter++;
}
if(fuel ==0)
{
	kill(base, SIGUSR2);
	kill(getpid(),SIGTERM);
}

}
else
{
	controller->next=(struct planes *)malloc(sizeof(struct planes));
	controller=controller->next;
	if(controller == 0)
	{
		printf("Out of memory");
		return 0;
	}
	//Making sure the last one is null
	controller->next=0;
	controller->id=planeid;
}


}

else if(strcmp(input, "status") ==0)
{
	controller = head;
	if(controller !=0)
	{
		printf("The current planes are: ");
		while(controller->next !=0)
		{
			if(controller ->id != 0)
			{
				printf("%d ", controller->id);
			}
			controller=controller->next;
		}
		if(controller->id !=0)
		printf("%d\n", controller->id);
		if(controller->id ==0)
			printf("\n");
	}
}
else if(strcmp(input, "bomb")==0)
{
	scanf(" %d",&bombid);
	kill(bombid,SIGUSR1);
}
else if(strcmp(input, "refuel")==0)
{
	scanf(" %d",&refuelid);
	kill(refuelid,SIGUSR2);
}
}
return 0;
}
void parent1(int signum)
{
	int status;
	int end = wait(&status);
	controller = head;
	if(controller !=0)
	{
		while(controller->next !=0)
		{
			if(controller ->id == end)
			{
				controller->id=0;
			}
			controller=controller->next;
		}
	}
}
void parent2(int signum)
{
printf("SOS! Plane has crashed!\nCommand:");
}
void child1(int signum)
{
printf("Bomber %d to base, bombs away!\nCommand:", getpid());
}
void child2(int signum)
{
	fuel=100;
}	

