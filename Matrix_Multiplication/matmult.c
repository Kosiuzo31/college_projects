#include <stdio.h>
#include <stdlib.h>
#include "matmult.h"

int main(int argc, char *argv[])
{
   double **m1, **m2, **mr;
	int row, col, m1_rows, m1_cols, m2_rows, m2_cols, mr_rows, mr_cols;

   //TODO: Take input for the number of rows and columns of matrix 1 and 2
   //Note: the number of columns of m1 must equal the number of rows of m2 for input to be valid
	int failure = 1;
	while(failure)
	{
		printf("Enter the number of rows in matrix 1: " );
		scanf("%d", &m1_rows);
		printf("\nEnter the number of cols in matrix 1: " );
		scanf("%d", &m1_cols);
		printf("\nEnter the number of rows in matrix 2: " );
		scanf("%d", &m2_rows);
		printf("\nEnter the number of cols in matrix 2: " );
		scanf("%d", &m2_cols);
		failure=0;
	if(m1_cols != m2_rows)
	{
		printf("\nThe colums in the first matrix has to equal the number of rows in the second matrix\n");
		failure =1;
	}
	if( (m1_rows>20 || m1_rows<1) || (m1_cols>20 || m1_cols<1) || (m2_rows>20 || m2_rows<1) || (m2_cols>20 || m2_cols<1) )
		{
			printf("\nThe numbers for the rows and columns in the matrices have to be between 1-20\n");
			failure=1;
		}
	}
   //These lines set the number of rows and columns for the result matrix.  Do not remove
   mr_rows = m1_rows;
	mr_cols = m2_cols;

   //TODO: Dynamically allocate space for m1 and m2
	m1=(double**)calloc(m1_rows,sizeof(double*));
	row=0;
	while(row<m1_rows)
	{
		*(m1+row)=(double*)calloc(m1_cols,sizeof(double));
		row++;
	}
	m2 =(double**)calloc(m2_rows,sizeof(double*));
	row=0;
	while(row<m2_rows)
	{
		*(m2+row)=(double*)calloc(m2_cols,sizeof(double));
		row++;	
	}
	double input;
	for(row=0; row<m1_rows; row++)
	{
		printf("Enter the values for row %d if matrix 1 (press enter after each)",row);
		for(col=0; col<m1_cols; col++)
		{
		printf("\nvalue %d:", col);
		scanf("%lf",&input);
		*(*(m1+row)+col)=input; 
		}
	}
	for(row=0; row<m2_rows; row++)
	{
		printf("Enter the values for row %d if matrix 2 (press enter after each)",row);
		for(col=0; col<m2_cols; col++)
		{
		printf("\nvalue %d:", col);
		scanf("%lf",&input);
		*(*(m2+row)+col)=input; 
		}
	}
   //TODO: Allocate the 2-D array for the result
	mr = (double**)calloc(m1_rows,sizeof(double*));
	row=0;
	while(row<mr_rows)
	{
		*(mr+row)=(double*)calloc(mr_cols,sizeof(double));
		row++;

	}


	//These two lines perform the matrix multiply and print the result -- Do not remove
	matMult(m1, m2, mr, mr_rows, mr_cols, m1_cols);
	printMat(mr, mr_rows, mr_cols);

	//TODO: deallocate is not currently implemented.  You need to implement this function.
   deallocate(m1, m2, mr, m1_rows, m2_rows, mr_rows); 

	return 0;
}

//This function frees all memory previously allocated for all three matrices
void deallocate(double **m1, double **m2, double **mr, int m1_rows, int m2_rows, int mr_rows)
{
	//TODO: You must implement this function.  It should free all memory allocated in main
	int row =0;
	for(row=0; row<m1_rows; row++)
	{
		free(*(m1+row)); 
	}
	for(row=0; row<m2_rows; row++)
	{
		free(*(m2+row));
	}
	for(row=0; row<mr_rows; row++)
	{
		free(*(mr+row));
	}

	free(m1);
	free(m2);
	free(mr);
		
}

//This function performs the matrix multiplication and stores the result in mr
void matMult(double **m1, double **m2, double **mr, int mr_rows, int mr_cols, int m1_cols)
{
	int i, j, k;
   for (i=0; i<mr_rows; i++)
	{
	   for (j=0; j<mr_cols; j++)
	   {
		   mr[i][j]=0;
		   for (k=0; k<m1_cols; k++)
			   mr[i][j]+=(m1[i][k]*m2[k][j]);
	   }
	}
}

//This function prints the 2-D array, mr (multiply result)
void printMat(double **mr, int mr_rows, int mr_cols)
{
	int i, j;
	for (i=0; i<mr_rows; i++)
	{
		for (j=0; j<mr_cols; j++)
			printf("%lf\t",mr[i][j]);
	   printf("\n");
	}
}
