Author: Kosi Uzodinma

Purpose: To practice I/O and memory allocation 

Commands for the program

To run: make run
You have to have files named file_1 and file_2

To compile: make/ make compile

To clean: make clean

Explanation for the difference in times for each step

Step 1
Step one takes small difference when the files are small because it doesn't
have to go to access the disk that many times.
When the files are large you will see a large difference because buffering is
turned off and you have to access the disk multiple times.
It is just an example of how much time it saves when you leave on buffering.

Step 2
It is quicker both when the files are small and large because it takes the hit
less times when access the disk. It will access the disk and get as much data
as it can then write to the file. Then it will keep doing that process until
the file is completely written to. This steps accesses the less times so it
saves a lot more times. Shows how costly it is in time it is to continually
access the disk. 
