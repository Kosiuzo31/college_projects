/* Author: Kosi Uzodinma
 * * Submission Date: March 24, 2015
 * * Class Purpose: To learn systems Programming and practice I/O and allocation
 * * Statement of Academic Honesty:
 * *
 * * The following code represents my own work. I have neither
 * *  received nor given inappropriate assistance. I have not copied
 * *  or modified code from any source other than the course webpage
 * *  or the course textbook. I recognize that any unauthorized
 * *  assistance or plagiarism will be handled in accordance with
 * *  the University of Georgia's Academic Honesty Policy and the
 * *  policies of this course. I recognize that my work is based
 * *  on an assignment created by the Department of Computer
 * *  Science at the University of Georgia. Any publishing
 * *  or posting of source code for this project is strictly
 * *  prohibited unless you have written consent from the Department
 * *  of Computer Science at the University of Georgia.
 * * */
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/time.h>
int main(int argc, const char* argv[])
{
	
	FILE *fpt_1, *fpt_2;
	double time_in_ms_1, seconds_diff_1,usec_diff_1;
	struct timeval tv_start_1, tv_end_1;
	gettimeofday(&tv_start_1,NULL);
	fpt_1=fopen(argv[1],"r");
	fpt_2=fopen(argv[2],"r");
	//Error checking if there are enough files
	if(argc<3)
	{
		printf("You need two files in the command line");
		return 0;
	}
	if(argc>3)
	{
		printf("You have too many command line arguments");
		return 0;
	}
	//Error checking if the files dont exist
	if(fpt_1 == NULL || fpt_2 == NULL) 
		{
		if (fpt_1 == NULL)
			printf("File 1 does not exist\n");
		if (fpt_2 == NULL)
			printf("File 2 does not exist");
		return 0;
		}
	
	//Begining of first step
	//Turning off buffer for input streams of file 1 and file 2
	setbuf(fpt_1, NULL);
	setbuf(fpt_2, NULL);
	FILE *fpt_3;
	fpt_3=fopen("diffone","w");
	//Turning off buffer for output stream of diffone
	setbuf(fpt_3,NULL);
	int c_1, c_2;
	do
	{
		//Getting the char byte by byte from the files 
		c_1=fgetc(fpt_1);
		c_2=fgetc(fpt_2);
		if(feof(fpt_1))
			break;
		if(feof(fpt_2))
			break;
		//Writing the char byte by byte to diffone if file1 doesn't match file2
		if(c_1 != c_2)
		{
			fputc(c_1, fpt_3);
		}

	}while(1);
	//Loop if file 1 is longer than file 2
	if(feof(fpt_2) && !feof(fpt_1))
		{
			do
			{
				fputc(c_1,fpt_3);
				c_1=fgetc(fpt_1);
				if(feof(fpt_1))
					break;
			}while(1);

		}
	//Closing all the streams
	fclose(fpt_1);
	fclose(fpt_2);
	fclose(fpt_3);
	//Getting the final time it took for step 1
	gettimeofday(&tv_end_1,NULL);
	seconds_diff_1=(tv_end_1.tv_sec -tv_start_1.tv_sec)*1000;
	usec_diff_1=(tv_end_1.tv_usec -tv_start_1.tv_usec)/1000.0;
	time_in_ms_1=seconds_diff_1+usec_diff_1;
	//End of first step
	
	//Begining of second step
	FILE *fpt1_step2, *fpt2_step2,*fpt3_step2;
	double time_in_ms_2, seconds_diff_2,usec_diff_2;
	struct timeval tv_start_2, tv_end_2;
	//Starting the timer for step 2
	gettimeofday(&tv_start_2,NULL);
	struct stat fileinfo_1, fileinfo_2;
	int a, b, length_1, length_2, terminator, i,n, different;
	fpt1_step2=fopen(argv[1],"r");
	fpt2_step2=fopen(argv[2],"r");
	fpt3_step2=fopen("difftwo","w");
	a = stat(argv[1], &fileinfo_1);
	b = stat(argv[2], &fileinfo_2);
	if(a==-1 || b==-1)
	{
		if(a == -1)
			printf("Unable to stat %s\n", argv[1]);
		if(b == -1)
			printf("Unable to stat %s\n", argv[2]);
		return 0;
	}
	char *differenceArray1, *differenceArray2, *outputArray;
	length_1 = fileinfo_1.st_size;
	length_2 = fileinfo_2.st_size;
	//Allocating memory for arrays
	differenceArray1 = (char*)malloc(length_1);
	differenceArray2 = (char*)malloc(length_2);
	//Writing the data into the arrays
	fread(differenceArray1,1, length_1, fpt1_step2);
	fread(differenceArray2,1, length_2, fpt2_step2);
	if(length_1<length_2)
		terminator = length_1;
	else
		terminator = length_2;
	//To check different between two arrays
	for(i=0; i<terminator;i++)
		{
			if(differenceArray2[i] != differenceArray1[i])
					different++;
		}
	//When file_2 is longer than file_1
	if(length_2>length_1)
		{
		while(i<length_2)
		{
			different++;
			i++;
		}
		}
	//Allocating memory for output array
	outputArray = (char*)malloc(different);
	n=0;
	//Write into the array the char that are different
	for(i=0; i<terminator;i++)
		{
			if(differenceArray2[i] != differenceArray1[i])
					{
					outputArray[n] = differenceArray2[i];
					n++;
					}
		}
	//When file_2 is longer than file_1 than write the rest
	if(length_2>length_1)
		{
		while(i<length_2)
		{
			outputArray[n] = differenceArray2[i];
			i++;
			n++;
		}
		}
	//writing the data to difftwo
	fwrite(outputArray,1, different, fpt3_step2);
	//Closing all the streams 
	fclose(fpt1_step2);
	fclose(fpt2_step2);
	fclose(fpt3_step2);
	//Getting time information for how long it took for step two
	gettimeofday(&tv_end_2,NULL);
	seconds_diff_2=(tv_end_2.tv_sec -tv_start_2.tv_sec)*1000;
	usec_diff_2=(tv_end_2.tv_usec -tv_start_2.tv_usec)/1000.0;
	time_in_ms_2=seconds_diff_2+usec_diff_2;
	//Printing out the time it took for both steps.
	printf("The step 1 took %f milliseconds\n", time_in_ms_1);
	printf("The step 2 took %f milliseconds\n", time_in_ms_2);
	//Freeing the memory from all the arrays
	free(outputArray);
	free(differenceArray1);
	free(differenceArray2);
	return 0;
}

