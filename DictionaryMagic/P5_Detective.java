import java.util.Arrays;
import java.util.*;
public class P5_Detective
{
  public static void main(String args[])
  {
    //Making all the strings that will be needed and setting them to null
    String dictionaryFile, word1,word2, canoncialWord1, canoncialWord2;
    canoncialWord1=canoncialWord2=dictionaryFile=word1=word2="";

    //Making the scanner for the project
    Scanner keyboard = new Scanner(System.in);

    //Handling bac cases
    if(args.length>3)
    {
      System.out.println("You have too many parameters");
      System.exit(0);
    }
    if(args.length<3)
    {
      System.out.println("You don't have enough parameters");
      System.exit(0);
    }
    if(!args[0].contains("txt") && !args[1].contains("txt") && !args[2].contains("txt"))
    {
      System.out.println("You did not include a dictionary file");
      System.exit(0);
    }

    //Assinging the args parameters to the variables made earlier
    if(args[0].contains("txt"))
    {

      dictionaryFile = args[0];
      word1 = args[1];
      word2 = args[2];
    }
    else if(args[1].contains("txt"))
    {
      dictionaryFile = args[1];
      word1 = args[0];
      word2 = args[2];
    }
    else if(args[2].contains("txt"))
    {
      dictionaryFile = args[2];
      word1 = args[0];
      word2 = args[1];
    }


    //Making the sorted text document
    DictionaryMagic project = new DictionaryMagic();
    project.alphabeticalSort(dictionaryFile);

    //Getting canoncial form and putting it into DictioaryMagic object
    canoncialWord1 = project.getCanoncialForm(word1);
    canoncialWord2 = project.getCanoncialForm(word2);
    boolean control, control2;
    DictionaryMagic one = new DictionaryMagic();
    DictionaryMagic two = new DictionaryMagic();
    one.setCompareWord(canoncialWord1);
    two.setCompareWord(canoncialWord2);
    control = project.findWord(project.getArray(), one);
    control2 = project.findWord(project.getArray(), two);


    //Checking to see if the words are in the dictionary
    if(!control && control2)
    {
      System.out.println("");

      System.out.println(word1 + " was not found in the dictionary");
      System.out.println("");
      System.out.println("Would you like to add " + word1 + " and its canoncial form to the dictionary?");
      String input = keyboard.next();
      if(!input.equalsIgnoreCase("yes") && !input.equalsIgnoreCase("no"))
      {
      while(!input.equalsIgnoreCase("no") && !input.equalsIgnoreCase("yes"))
      {
          System.out.println("Enter yes or no");
          input = keyboard.next();
      }
      }
      if(input.equalsIgnoreCase("yes"))
      {
        project.alphabeticalSortExtra("sorted-output.txt", word2);
      }
      System.out.println("Enter two more words or enter two -1's to exit the program. \nExample word1 word2 or -1 -1");
    }
    else if(control && !control2)
    {
      System.out.println("");
      System.out.println(word2 + " was not found in the dictionary");
      System.out.println("");
      System.out.println("Would you like to add " + word2 + " and its canoncial form to the dictionary?");
      String input = keyboard.next();
      if(!input.equalsIgnoreCase("yes") && !input.equalsIgnoreCase("no"))
      {
      while(!input.equalsIgnoreCase("no") && !input.equalsIgnoreCase("yes"))
      {
          System.out.println("Enter yes or no");
          input = keyboard.next();
      }
      }
      if(input.equalsIgnoreCase("yes"))
      {
        project.alphabeticalSortExtra("sorted-output.txt", word2);
      }
      System.out.println();
      System.out.println("Enter two more words or enter two -1's to exit the program. \nExample word1 word2 or -1 -1");
    }
    else if(!control && !control2)
    {
      System.out.println();
      System.out.println(word1 + " and " + word2 + " were not found in the dictionary");
      System.out.println();
      System.out.println("Would you like to add " + word1 + " and " + word2 + " and their canoncial form to the dictionary?");
      String input = keyboard.next();
      if(!input.equalsIgnoreCase("yes") && !input.equalsIgnoreCase("no"))
      {
      while(!input.equalsIgnoreCase("no") && !input.equalsIgnoreCase("yes"))
      {
          System.out.println("Enter yes or no");
          input = keyboard.next();
      }
      }
      if(input.equalsIgnoreCase("yes"))
      {
        project.alphabeticalSortExtra("sorted-output.txt", word1, word2);
      }
      System.out.println("Enter two more words or enter two -1's to exit the program. \nExample word1 word2 or -1 -1");
    }

    //Checking to see if they are anagrams of one another
    else if(canoncialWord1.equals(word2))
    {
      System.out.println("");
      System.out.println(word1 + " and "+ word2 + " are anagrams of each other");
      System.out.println("Enter two more words or enter two -1's to exit the program. \nExample word1 word2 or -1 -1");
    }
    else if(!canoncialWord1.equals(word2))
    {
      System.out.println("");
      System.out.println(word1 + " and "+ word2 + " are not anagrams of each other");
      System.out.println("Enter two more words or enter two -1's to exit the program. \nExample word1 word2 or -1 -1");
    }


    //Repeating the process to enter more words
    word1 = keyboard.next();
    word2 = keyboard.next();
    canoncialWord1 = project.getCanoncialForm(word1);
    canoncialWord2 = project.getCanoncialForm(word2);
    one = new DictionaryMagic();
    two = new DictionaryMagic();
    one.setCompareWord(canoncialWord1);
    two.setCompareWord(canoncialWord2);
    control = project.findWord(project.getArray(), one);
    control2 = project.findWord(project.getArray(), two);

    //Loop that controls process to do it again. Only stops with two -1's
    while(!word1.equals("-1") && !word2.equals("-1"))
    {

      //Checking to see if the words are in the dictionary
      if(!control && control2)
      {
        System.out.println("");
        System.out.println(word1 + " was not found in the dictionary");

        System.out.println("");
        System.out.println("Would you like to add " + word1 + " and its canoncial form to the dictionary?");
        String input = keyboard.next();
        if(!input.equalsIgnoreCase("yes") && !input.equalsIgnoreCase("no"))
        {
        while(!input.equalsIgnoreCase("no") && !input.equalsIgnoreCase("yes"))
        {
            System.out.println("Enter yes or no");
            input = keyboard.next();
        }
        }

        if(input.equalsIgnoreCase("yes"))
        {
          project.alphabeticalSortExtra("sorted-output.txt", word2);
        }
        System.out.println("Enter two more words or enter two -1's to exit the program. \nExample word1 word2 or -1 -1");
      }
      else if(control && !control2)
      {
        System.out.println("");
        System.out.println(word2 + " was not found in the dictionary");

        System.out.println("");
        System.out.println("Would you like to add " + word2 + " and its canoncial form to the dictionary?");

        String input = keyboard.next();
        System.out.println();
        if(!input.equalsIgnoreCase("yes") && !input.equalsIgnoreCase("no"))
        {
        while(!input.equalsIgnoreCase("no") && !input.equalsIgnoreCase("yes"))
        {
            System.out.println("Enter yes or no");
            input = keyboard.next();
        }
        }

        if(input.equalsIgnoreCase("yes"))
        {
          project.alphabeticalSortExtra("sorted-output.txt", word2);
        }
        System.out.println();
        System.out.println("Enter two more words or enter two -1's to exit the program. \nExample word1 word2 or -1 -1");
      }
      else if(!control && !control2)
      {
        System.out.println();
        System.out.println(word1 + " and " + word2 + " were not found in the dictionary");
        System.out.println();
        System.out.println("Would you like to add " + word1 + " and " + word2 + " and their canoncial form to the dictionary?");
        String input = keyboard.next();
        if(!input.equalsIgnoreCase("yes") && !input.equalsIgnoreCase("no"))
        {
        while(!input.equalsIgnoreCase("no") && !input.equalsIgnoreCase("yes"))
        {
            System.out.println("Enter yes or no");
            input = keyboard.next();
        }
        }

        if(input.equalsIgnoreCase("yes"))
        {
          project.alphabeticalSortExtra("sorted-output.txt", word1, word2);
        }
        System.out.println("Enter two more words or enter two -1's to exit the program. \nExample word1 word2 or -1 -1");
      }

      //Checking to see if they are anagrams of one another
      else if(canoncialWord1.equals(word2))
      {
        System.out.println("");
        System.out.println(word1 + " and "+ word2 + " are anagrams of each other");
        System.out.println("Enter two more words or enter two -1's to exit the program. \nExample word1 word2 or -1 -1");
      }
      else if(!canoncialWord1.equals(word2))
      {
        System.out.println("");
        System.out.println(word1 + " and "+ word2 + " are not anagrams of each other");
        System.out.println("Enter two more words or enter two -1's to exit the program. \nExample word1 word2 or -1 -1");
      }
      word1 = keyboard.next();
      word2 = keyboard.next();
      canoncialWord1 = project.getCanoncialForm(word1);
      canoncialWord2 = project.getCanoncialForm(word2);
      one = new DictionaryMagic();
      two = new DictionaryMagic();
      one.setCompareWord(canoncialWord1);
      two.setCompareWord(canoncialWord2);
      control = project.findWord(project.getArray(), one);
      control2 = project.findWord(project.getArray(), two);

    }

  }

}
