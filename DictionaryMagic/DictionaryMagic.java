import java.util.regex.Pattern;
import java.lang.Object;
import java.util.regex.Matcher;
import java.util.Scanner;
import java.io.PrintWriter;
import java.io.*;
import java.io.Writer;
import java.io.File;
import javax.imageio.*;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Stack;
import java.lang.Integer;
public class DictionaryMagic implements Comparable<DictionaryMagic>
{
//Declaring instance variables I will need for the class
private DictionaryMagic array[];
private DictionaryMagic wordObject;
private String originalInputString, tempInputString;
private String compareWord, dictionaryWord;
private int count = 0;
private FileHelper helper1, helper2;
private PrintWriter outputPrinter;
private File file;
private Stack<DictionaryMagic> stack = new Stack<DictionaryMagic>();

public DictionaryMagic()
{
  compareWord = "";
  dictionaryWord = "";
}

//Making sure the file is Valid meaning formatted correctly
private void validFile(String fileName)
{
  //Two helper files two open up both of the files I will be using
  FileHelper helper1 = new FileHelper();
  FileHelper helper2 = new FileHelper();
  helper1.openFile(fileName);
  Scanner scan = null;
  originalInputString = "";
  tempInputString = "";


  try
  {
    scan = new Scanner(helper1.getFile());
  }
  catch(FileNotFoundException e)
  {
    System.out.println("File not found");
  }
  //Scanning for words
  while(scan.hasNextLine())
  {
    //Keep track of the count to make sure that more than one word is not on a line
    count = 0;
    originalInputString = scan.nextLine();
    Scanner scan2 = new Scanner(originalInputString);

    //Loop that tracks to see if their more than one word on a line
    while(scan2.hasNext())
    {
      tempInputString = scan2.next();
      count++;
    if(count>1)
    {
      System.out.println("The file you are trying to use is not formatted incorrectly");
      System.exit(0);
    }
  }

  }



}

/** Puts all the elements in a stack so I can put them in a array.
    You need a stack to keep track of the size so you can know how big to make the array.
**/
private void putIntoStack(String fileName)
{
  //Called validFile to put all the words in their canoncial form
  validFile(fileName);
  stack = new Stack<DictionaryMagic>();
  FileHelper helper = new FileHelper();
  Scanner scan = null;
  helper.openFile(fileName);
  try
  {
    scan = new Scanner(helper.getFile());
  }
  catch(FileNotFoundException e)
  {
    System.out.println("File not found");
  }
  //Getting all the words for the document and putting them in the stack
  while(scan.hasNextLine())
  {
    String tempString = scan.nextLine();
    wordObject = new DictionaryMagic();
    //I said cannoncial Word equals the original word because my compareTo uses to OriginalWord to compare
    //This is the only way I can get them to be sorted correctly
    wordObject.dictionaryWord = tempString;

    // Puts all the words in a char array so they can be sorted in alphabetical order to make the canoncial form
    char[] temp = tempString.toCharArray();
    Arrays.sort(temp);

    // Put it back into a string so it can be printed
    String tempString2 = new String(temp);
    wordObject.compareWord = tempString2;
    stack.push(wordObject);
  }
}

//Returning the stack
public Stack<DictionaryMagic> getStack(){
  return stack;
}

//Putting all the elements from the stack into an array
private void putIntoArray(String fileName)
{
  putIntoStack(fileName);
  Stack<DictionaryMagic> tempStack = getStack();
  array = new DictionaryMagic[tempStack.size()];

  //Putting all the elements in the array
  for(int i = 0; i<array.length; i++)
  {
    array[i] = tempStack.pop();
  }
  //Sorting the array
  Arrays.sort(array);
}

//Overloaded putIntoArray method to take in array words to be sorted
private void putIntoArray(String fileName, String... words)
{
  putIntoStack(fileName);

  //Putting the extra elements in the stack
  for(int i =0; i<words.length; i++)
  {

    wordObject = new DictionaryMagic();
    wordObject.dictionaryWord = words[i];
    String temp = getCanoncialForm(words[i]);
    wordObject.compareWord = temp;
    addToStack(wordObject);
  }
  Stack<DictionaryMagic> tempStack = getStack();
  array = new DictionaryMagic[tempStack.size()];

  for(int i = 0; i<array.length; i++)
  {
    array[i] = tempStack.pop();
  }
  //Sorting the array
  Arrays.sort(array);
}

//Printed the alphabetically Sorted array into the sorted-output text file
public void alphabeticalSort(String fileName)
{
  //Calling this method to do all the work
  putIntoArray(fileName);

  //Opening the file where everything will be printed
  FileHelper helper1 = new FileHelper();
  helper1.openFile("sorted-output.txt");
  outputPrinter = null;

  try
  {
    outputPrinter = new PrintWriter(new FileWriter(helper1.getFile()));
  }
  catch(FileNotFoundException e)
  {
    System.out.println("File not found");
  }
  catch(IOException e)
  {
    System.out.println("Input output exception");
  }

  //Printing all the elements in the array in the txt file
  for(int i =0; i<array.length; i++)
  {
    outputPrinter.println(array[i].dictionaryWord);
  }

  //Closing the Printer and prompting the user
  outputPrinter.close();
  System.out.println();
  System.out.println("It was sorted successfully go look at your file sort-output.txt" );

}

//Second Alphabetical sort that adds extra words
//Printed the alphabetically Sorted array into the sorted-output text file
public void alphabeticalSortExtra(String fileName, String... input)
{
  //Calling this method to do all the work
  putIntoArray(fileName, input);

  //Opening the file where everything will be printed
  FileHelper helper1 = new FileHelper();
  helper1.openFile("sorted-output.txt");
  outputPrinter = null;

  try
  {
    outputPrinter = new PrintWriter(new FileWriter(helper1.getFile()));
  }
  catch(FileNotFoundException e)
  {
    System.out.println("File not found");
  }
  catch(IOException e)
  {
    System.out.println("Input output exception");
  }

  //Printing all the elements in the array in the txt file
  for(int i =0; i<array.length; i++)
  {
    outputPrinter.println(array[i].dictionaryWord);
  }

  //Closing the Printer and prompting the user
  outputPrinter.close();
  System.out.println();
  System.out.println("It was sorted successfully go look at your file sort-output.txt" );

}


//Returns true or false if a word is in the dictionary
public boolean findWord(DictionaryMagic[] str, DictionaryMagic input)
{
    int index = Arrays.binarySearch(str, input);
    DictionaryMagic fromArray = new DictionaryMagic();
    if(index>=0)
    {
      fromArray = str[index];
      //Making sure they truly are a match using Strings comparator
      int value = fromArray.compareTo(input);
      if(value == 0)
      return true;
      else
      return false;
    }
    else
      return false;
}

//Gets the canoncial form of a String
public String getCanoncialForm(String inputStr)
{
  char[] temp = new char[inputStr.length()];
  for(int i = 0; i<temp.length;i++)
  {
    temp[i] = inputStr.charAt(i);
  }
  Arrays.sort(temp);
  String dictionaryWord = new String(temp);
  return dictionaryWord;
}

//Returns the Arrays
public DictionaryMagic[] getArray()
{
  return array;
}

//Adds a word to the stack
public void addToStack(DictionaryMagic input)
{
  stack.push(input);
}

//Implementing compareTo of Comparable
public int compareTo(DictionaryMagic input)
{
  //Using String's compareTo because thats all I need to compare
  return this.compareWord.compareTo(input.compareWord);

}

public void setCompareWord(String input)
{
  this.compareWord = input;
}

public void setdictionaryWord(String input)
{
  this.dictionaryWord = input;
}

}
