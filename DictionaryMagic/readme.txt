Name: Kosi Uzodinma

To run the program use the following commands, make compile and make run.
The make compile command turns it into bytecode for your computer.
The make run command excutes the program.
You can not execute the program unless you make compile it first.

How to run the Program

1.make compile (case sensitive)

2.make run (case sensitive)

The following steps are case sensitive you do not need to type in case sensitive.

If you want to clean the bytecode use the command "make clean".
