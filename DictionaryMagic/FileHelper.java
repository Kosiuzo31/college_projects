import java.io.PrintWriter;
import java.io.*;
import java.io.Writer;
import java.io.File;
import javax.imageio.*;
import java.awt.image.BufferedImage;
public class FileHelper {
private PrintWriter printer;
private BufferedImage img;
private File file;

//Sets all the instance variables to null
public FileHelper()
{
	printer = null;
	img = null;
	file = null;
}

//This will open a printWriter
public void openPrinter(String fileName)
{
	try
	{
		printer = new PrintWriter(new FileWriter(fileName, true));
	}
	catch (IndexOutOfBoundsException e) {
    System.err.println("Caught IndexOutOfBoundsException: " + e.getMessage());
	}
	catch (IOException e) {
    System.err.println("Caught IOException: " +  e.getMessage());
	}
	catch(Exception e){
		System.err.println("Caught Exception: " + e.getMessage());
	}
}

//This will open a File
public void openFile(String fileName)
{
	try
	{
		file = new File(fileName);
	}
	catch(IllegalArgumentException e){
	System.err.println("The file object is null");
	e.printStackTrace();
	}
}

//This will open an Image
public void openImage(String fileName)
{
	try{
	img = ImageIO.read(new File(fileName));
	}catch(IllegalArgumentException e){
	System.err.println("The file object is null");
	e.printStackTrace();
	}
	catch(IOException e){
	System.err.println("Error:" + e.getMessage());
	System.err.println("You might have entered a file that did not exist");
}
}

//Return the printwriter
public PrintWriter getPrinter()
{
	return printer;
}

//Writes to the printer
public void record(String input)
{
	printer.print(input);
}

//Saves the printer
public void savePrinter()
{
	printer.close();
	System.out.println("Printer was closed and save successfully ");
}

//Erases the printer
public void erasePrinter(String fileName)
{
	try
	{
		printer = new PrintWriter(new FileWriter(fileName));
	}
	catch (IndexOutOfBoundsException e) {
    System.err.println("Caught IndexOutOfBoundsException: " + e.getMessage());
	}
	catch (IOException e) {
    System.err.println("Caught IOException: " +  e.getMessage());
	}
	catch(Exception e){
		System.err.println("Caught Exception: " + e.getMessage());
	}
}

//Returns the file
public File getFile()
{
	return file;
}

//Returns the image
public BufferedImage getImage()
{
	return img;
}

//PreCondition: The parameter has to be a string and a valid png filename.
//PostCondition: Saves the image to the file
public void save(String fileName, String typeOfImage){
	try{
	BufferedImage bi = getImage();
	File outputfile = new File(fileName);
	ImageIO.write(bi, typeOfImage, outputfile);
	System.out.println("File was Saved Sucessfully");
	System.out.println("Go look at your decrypted image: " + fileName);
	}catch(IllegalArgumentException e)
	{
	System.err.println("The file is null");
	e.printStackTrace();
	}
	catch(IOException e){
	System.err.println("Error: " + e.getMessage());
	e.printStackTrace();
}
}




}
