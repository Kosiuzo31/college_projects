/**This is the class file that contains all the different types of sub classes of random, naive, and cutthroat*/
public class ComputerPlayer extends Player{
	
	public ComputerPlayer()
	{
		super("CPU");
	}

	public ComputerPlayer(String inputName)
	{
		super(inputName);
	}

	}