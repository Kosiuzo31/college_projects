/**This is the class that randomly generates a number and returns a number*/

import java.util.Random;
public class RandomComputerPlayer extends ComputerPlayer
{
	Random rand = new Random();

	public RandomComputerPlayer()
	{
		super("RandomCPU");
	}

	public RandomComputerPlayer(String inputName)
	{
		super(inputName);
	}
	
	//randomly gives you a free space between 1-9
	public int randomSpace()
	{
		boolean flip = true;
		int space = 0;
		while(flip)
		{
			space = rand.nextInt(9) + 1;
			if(checkAvailable(space))
			{
				flip = false;
			}
		}
		return space;

	}
}