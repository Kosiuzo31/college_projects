/**This is the class file that has all the information for all the different sub classes and checks if all the spots are available and checks if it won or not 
 * checks if its a tie or not and it also inserts X and O */
import java.io.PrintWriter;
import java.io.*;
import java.io.Writer;
public class Player {
	
	private String name;
	private PrintWriter out = null;
	public Player()
	{
		name = "";
	}
	
	//sets the name
	public Player(String inputName)
	{
		name = inputName;
	}
	
	public void openFile(String fileName)
	{
		try
		{
			out = new PrintWriter(new FileWriter(fileName));
		}
		catch (IndexOutOfBoundsException e) {
        System.err.println("Caught IndexOutOfBoundsException: " + e.getMessage());                      
    	} 
    catch (IOException e) {
        System.err.println("Caught IOException: " +  e.getMessage());                        
    	}
    	catch(Exception e){
    		System.err.println("Caught Exception: " + e.getMessage());
    	}
	}

	//Writes to the gamelog
	public void record(String input)
	{
		out.println(input);
	}

	public void closeFile()
	{
		System.out.println("gameplay log was closed and save successfully ");
		out.close();
	}

	//returns a name
	public String getName()
	{
		return name;
	}
	
	//sets the name
	public void setName(String inputName)
	{
		name = inputName;
	}
	
	//returns a name
	public void displayName()
	{
		System.out.println(name);
	}
	
	//checks if the spot entered is available or not and probably the most important method
	public boolean checkAvailable(int space)
	{
		String[][] tempArray = Board.board;
		switch(space)
		{
		
		case 1: if(tempArray[4][0] == " ")
			return true;
			else
			return false;
		case 2: if(tempArray[4][2] == " ")
			return true;
			else
			return false;
		case 3: if(tempArray[4][4] == " ")
			return true;
			else
			return false;
		case 4: if(tempArray[2][0] == " ")
			return true;
			else
			return false;
		case 5: if(tempArray[2][2] == " ")
			return true;
			else
			return false;
		case 6: if(tempArray[2][4] == " ")
			return true;
			else
			return false;
		case 7: if(tempArray[0][0] == " ")
			return true;
			else
			return false;
		case 8: if(tempArray[0][2] == " ")
			return true;
			else
			return false;
		case 9: if(tempArray[0][4] == " ")
			return true;
			else
			return false;
		default: 
			System.out.println("You have to enter 1-9");
			return false;
		}
		
	}
	
	//inserts the X into the space that is entered in
	public void insertX(int choice)
	{
		String tempArray[][] = Board.board;
		if(checkAvailable(choice))
		{
			switch(choice)
			{
			
			case 1: tempArray[4][0] = "X";
					break;
			case 2: tempArray[4][2] = "X";
					break;
			case 3: tempArray[4][4] = "X";
					break;
			case 4: tempArray[2][0] = "X";
					break;
			case 5:	tempArray[2][2] = "X";
					break;
			case 6: tempArray[2][4] = "X";
					break;
			case 7:	tempArray[0][0] = "X";
					break;
			case 8: tempArray[0][2] = "X";
					break;
			case 9: tempArray[0][4] = "X";
					break;
			
		}
		
	}
		else
			System.out.println("That space is taken choose another one");
	}
	
	//inserts O in the space that is entered
	public void insertO(int choice)
	{
		String tempArray[][] = Board.board;
		if(checkAvailable(choice))
		{
			switch(choice)
			{
			
			case 1: tempArray[4][0] = "O";
					break;
			case 2: tempArray[4][2] = "O";
					break;
			case 3: tempArray[4][4] = "O";
					break;
			case 4: tempArray[2][0] = "O";
					break;
			case 5:	tempArray[2][2] = "O";
					break;
			case 6: tempArray[2][4] = "O";
					break;
			case 7:	tempArray[0][0] = "O";
					break;
			case 8: tempArray[0][2] = "O";
					break;
			case 9: tempArray[0][4] = "O";
					break;
			
		}
		
	}
		else
			System.out.println("That space is taken choose another one");
	}

	//checks if it is a tie 
	public boolean checkCat()
	{
		for (int i =0; i< Board.board.length;i+=2 )
		{
			for (int j =0; j< Board.board[0].length; j+=2)
			{
				if (Board.board[i][j] == " ")
					return false;
			}
		}
		
		return true;
	}
	
	//checks if someone won 
	public boolean checkWin(String symbol)
	{
		if ((Board.board[0][0] == symbol) && (Board.board[0][0] == Board.board[0][2]) && (Board.board[0][2] == Board.board[0][4]))
			return true;
		else if ((Board.board[2][0] == symbol) && (Board.board[2][0] == Board.board[2][2]) && (Board.board[2][2] == Board.board[2][4]))
			return true;
		else if ((Board.board[4][0] == symbol) && (Board.board[4][0] == Board.board[4][2]) && (Board.board[4][2] == Board.board[4][4]))
			return true;
		else if ((Board.board[0][0] == symbol) && (Board.board[0][0] == Board.board[2][0]) && (Board.board[2][0] == Board.board[4][0]))
			return true;
		else if ((Board.board[0][2] == symbol) && (Board.board[0][2] == Board.board[2][2]) && (Board.board[2][2] == Board.board[4][2]))
			return true;
		else if ((Board.board[0][4] == symbol) && (Board.board[0][4] == Board.board[2][4]) && (Board.board[2][4] == Board.board[4][4]))
			return true;
		else if ((Board.board[0][0] == symbol) && (Board.board[0][0] == Board.board[2][2]) && (Board.board[2][2] == Board.board[4][4]))
			return true;
		else if ((Board.board[0][4] == symbol) && (Board.board[0][4] == Board.board[2][2]) && (Board.board[2][2] == Board.board[4][0]))
			return true;
		else
			return false;
		
	}
	
}
