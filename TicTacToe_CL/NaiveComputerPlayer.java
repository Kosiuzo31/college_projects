/**This is the class file that plays the first available spot from left to right called naive*/
public class NaiveComputerPlayer extends ComputerPlayer{
	public NaiveComputerPlayer()
	{
		super("CutThroatCPU");
	}
	public NaiveComputerPlayer(String inputName)
	{
		super(inputName);
	}
	
	//plays the first available space from left to right
	public int firstAvailableSpace()
	{
		int numberTracker = 1;
		for(int x = 4; x >=0; x-=2 )
		{
			for(int y = 0; y < Board.board[0].length; y+=2)
			{
				if (Board.board[x][y] == " ")
					return numberTracker;
				else
					numberTracker++;

			}
		}
		return numberTracker;
	}
}