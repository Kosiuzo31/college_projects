/** This class implements the board class and makes a tic-tac-toe defining the methods of make board and displayBoard **/

public class TicTacToeBoard implements Board {
	
	/**Added a default contructor just to have if I decide to add stuff to this class**/
	public TicTacToeBoard()
	{
		
	}
	/** Defined the makeBoard method to put "|" and "-" to make a tic-tac-toe board for users to input numbers"**/
	public void makeBoard()
	{
		//String [][] Board.board = new String [5][5];
		Board.board[0][1] = "|"; Board.board[0][3] = "|"; Board.board[2][1] = "|"; Board.board[2][3] = "|"; Board.board[4][1] = "|"; Board.board[4][3]="|";  
		//Skips two lines because the lines only go in the middle
		for (int i=1; i< Board.board.length;i+=2 )
		{
			for (int j = 0; j < Board.board[0].length; j++)
			{
				Board.board[i][j] = "-";
			}
			
		}
		//Goes to every other space so it will not touch the pipes and users can put input between them
		for (int i=0; i< Board.board.length;i+=2 )
		{
			for (int j = 0; j < Board.board[0].length; j+=2)
			{
				Board.board[i][j] = " ";
			}
			
		}
		
	}
	//Display all it does it show the board. This is useful to show input users have made.
	public void displayBoard()
	{
		for (int i=0; i< Board.board.length;i++ )
		{
			for (int j = 0; j < Board.board[0].length; j++)
			{
				System.out.print(Board.board[i][j]);
			}
			System.out.println();
		}
	}

	
	
}


