/**This is file that actually plays the whole game.*/

import java.util.Scanner;
public class TicTacToe {

	public static void main(String[] args) 
	{
		String p1 = null, p2= null;
		Scanner keyboard = new Scanner(System.in);
		TicTacToeBoard gameBoard = new TicTacToeBoard();
		Player rec = new Player();
		gameBoard.makeBoard();

		if (args.length < 2 )
		{
			System.out.println("You didnt enter enough parameters .");
			System.exit(0);
		}
		if(args.length == 2)
		{
			if((args[0].contains( "log")) || (args[1].contains("log")) )
			{
				System.out.println("If you have only two parameter -log can not be one of them");
				System.exit(0);
			}
		}
		if(args.length>3)
		{
			System.out.println("You entered too many parameters");
			System.exit(0);
		}

		gameBoard.displayBoard();
		
		if(args.length == 2)
		{
			p1 = args[0];
			p2 = args[1];

		}

		if(args.length == 3)
		{
			if(args[0].contains("log") )
			{
				p1 = args[1];
				p2 = args[2];
			}

			else if(args[1].contains( "log"))
			{
				p1 = args[0];
				p2 = args[2];
			}

			else if (args[2].contains("log"))
			{
				p1 = args[0];
				p2 = args[1];
			}
			else
			{
			System.out.println("You entered too many parameters");
			System.exit(0);
			}
			rec.openFile("gameplay.txt");
			System.out.println("Your gameplay will be logged");
			
		}
		System.out.println("Tic-tac-toe Game");
		System.out.println();
		gameBoard.displayBoard();
		
		//plays human vs human
		if(p1.equalsIgnoreCase("human") && p2.equalsIgnoreCase("human"))

		{

			HumanPlayer hum = new HumanPlayer("Human Plaue X");
			HumanPlayer hum1 = new HumanPlayer("Human Plaue O");
			boolean main = true;
			//it switches between two players
			while(main)
			{
				
				boolean play = true;
				//plays the first player
				while(play)
				{
					System.out.print(hum.getName() + " Enter an open spot to mark: ");
					int space = keyboard.nextInt();
					//checks if the space is available or not
					if(hum.checkAvailable(space))
					{
						hum.insertX(space);
						if(args.length>2)
						{
						rec.record(hum.getName() + " made a move to space: " + space);
						
						}
						gameBoard.displayBoard();
						//checks if it won or not
						if (hum.checkWin("X"))
						{

							System.out.println("Game Over!!!" + hum.getName() + " Wins!");
							if(args.length>2)
							{
								rec.record(hum.getName() + " Won the game");
								rec.closeFile();
							}
							System.exit(0);
						}
						//checks if it is a tie or not
						else if (hum.checkCat())
						{
							System.out.println("Game Over!!! It's a tie.");
							if(args.length>2)
							{
								rec.record("The Game ended in a tie");
								rec.closeFile();
							}
							System.exit(0);
						}
						
						play = false;
					}	
					//checks if its a legal entry
					else 
					{
						System.out.println("Please enter an open spot between 1 and 9");
					}
				}
				
				boolean play1 = true;
				//plays the second player
				while(play1)
				{
					
					System.out.print(hum1.getName() + " Enter an open spot to mark: ");
					int space = keyboard.nextInt();
					//checks if the space is available
					if(hum1.checkAvailable(space))
					{
						hum1.insertO(space);
						if(args.length>2)
						{
							rec.record(hum1.getName() + " made a move to space: " + space);
							
						}
						gameBoard.displayBoard();
						//checks if it won or not
						if (hum1.checkWin("O"))
						{
							System.out.println("Game Over!!!" + hum1.getName() + " Wins!");
							if(args.length>2)
							{
								rec.record(hum1.getName() + " Won the game");
								rec.closeFile();
							}
							System.exit(0);
						}
						//checks if it is a tie or not
						else if (hum.checkCat())
						{
							if(args.length>2)
							{
								rec.record("The Game ended in a tie");
								rec.closeFile();
							}
							System.out.println("Game Over!!! It's a tie.");
							System.exit(0);
						}
						
						play1 = false;
					}	
					//checks if its a legal entry or not
					else 
					{
						System.out.println("Please enter an open spot between 1 and 9");
					}
				}
				
					
				
				
			}
			


		}//ends here (human vs human)
		
		//plays human vs naive
		else if (p1.equalsIgnoreCase("human") && p2.equalsIgnoreCase("naive"))
		{
			
			HumanPlayer hum = new HumanPlayer("Human Plaue X");
			NaiveComputerPlayer comp = new NaiveComputerPlayer("Naive Plaue O");
			
			boolean main = true;
			while (main)
			{
				boolean play = true;
				//plays the first player
				while(play)
				{
					System.out.print(hum.getName() + " Enter an open spot to mark: ");
					int space = keyboard.nextInt();
					if(hum.checkAvailable(space))
					{
						hum.insertX(space);
						if(args.length>2)
						{
						rec.record(hum.getName() + " made a move to space: " + space);
						
						}
						gameBoard.displayBoard();
						if (hum.checkWin("X"))
						{
							if(args.length>2)
							{
								rec.record(hum.getName() + " Won the game");
								rec.closeFile();
							}
							System.out.println("Game Over!!!" + hum.getName() + " Wins!");
							System.exit(0);
						}
						else if (hum.checkCat())
						{
							if(args.length>2)
							{
								rec.record("The Game ended in a tie");
								rec.closeFile();
							}
							System.out.println("Game Over!!! It's a tie.");
							System.exit(0);
						}
						
						play = false;
					}	
					else 
					{
						System.out.println("Please enter an open spot between 1 and 9");
					}
				}
				
				boolean secondPlayer = true;
				//plays the second player
				while(secondPlayer)
				{
					int naiveSpace = comp.firstAvailableSpace();
					System.out.println(comp.getName() + " enter an open spot to mark: " + naiveSpace);
					comp.insertO(naiveSpace);
					if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + naiveSpace);
						
						}
					gameBoard.displayBoard();
					
					if (comp.checkWin("O"))
					{
						if(args.length>2)
							{
								rec.record(comp.getName() + " Won the game");
								rec.closeFile();
							}
						System.out.println("Game Over!!!" + comp.getName() + " Wins!");
						System.exit(0);
					}
					else if (comp.checkCat())
					{
						if(args.length>2)
							{
								rec.record("The game ended in a tie");
								rec.closeFile();
							}
						System.out.println("Game Over!!! It's a tie.");
						System.exit(0);
					}
					
					secondPlayer = false;
				
				}
				
			}
			
		}//ends here (human vs naive)
		
		//plays naive vs human
		else if (p1.equalsIgnoreCase("naive") && p2.equalsIgnoreCase("human"))
		{
			
			HumanPlayer hum = new HumanPlayer("Human Plaue O");
			NaiveComputerPlayer comp = new NaiveComputerPlayer("Naive Plaue X");
			
			boolean main = true;
			while (main)
			{
				boolean secondPlayer = true;
				while(secondPlayer)
				{
					int naiveSpace = comp.firstAvailableSpace();
					System.out.println(comp.getName() + " enter an open spot to mark: " + naiveSpace);
					comp.insertX(naiveSpace);
					if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + naiveSpace);
						
						}
					gameBoard.displayBoard();
					
					if (comp.checkWin("X"))
					{
						if(args.length>2)
							{
								rec.record(comp.getName() + " Won the game");
								rec.closeFile();
							}
						System.out.println("Game Over!!! " + comp.getName() + " Wins!");
						System.exit(0);
					}
					else if (comp.checkCat())
					{
						if(args.length>2)
							{
								rec.record("The game was a tie");
								rec.closeFile();
							}
						System.out.println("Game Over!!! It's a tie.");
						System.exit(0);
					}
					
					secondPlayer = false;
				
				}
				
				boolean play = true;
				while(play)
				{
					
					System.out.print(hum.getName() + " Enter an open spot to mark: ");
					int space = keyboard.nextInt();
					if(hum.checkAvailable(space))
					{
						hum.insertO(space);
						if(args.length>2)
						{
						rec.record(hum.getName() + " made a move to space: " + space);
						
						}
						gameBoard.displayBoard();
						if (hum.checkWin("O"))
						{
							if(args.length>2)
							{
								rec.record(hum.getName() + " Won the game");
								rec.closeFile();
							}
							System.out.println("Game Over!!!" + hum.getName() + " Wins!");
							System.exit(0);
						}
						else if (hum.checkCat())
						{
							if(args.length>2)
							{
								rec.record("The game was tie");
								rec.closeFile();
							}
							System.out.println("Game Over!!! It's a tie.");
							System.exit(0);
						}
						
						play = false;
					}	
					else 
					{
						System.out.println("Please enter an open spot between 1 and 9");
					}
				}
				
				
				
			}
			
		}//ends here (naive vs human)
		
		//plays human vs random
		else if (p1.equalsIgnoreCase("human") && p2.equalsIgnoreCase("random"))
		{
			
			HumanPlayer hum = new HumanPlayer("Human Plaue X");
			RandomComputerPlayer comp = new RandomComputerPlayer("Random Plaue O");
			
			boolean main = true;
			while (main)
			{
				boolean play = true;
				//plays the first player
				while(play)
				{
					System.out.print(hum.getName() + " Enter an open spot to mark: ");
					int space = keyboard.nextInt();
					if(hum.checkAvailable(space))
					{
						hum.insertX(space);
						if(args.length>2)
						{
						rec.record(hum.getName() + " made a move to space: " + space);
						
						}
						gameBoard.displayBoard();
						if (hum.checkWin("X"))
						{
							if(args.length>2)
							{
								rec.record(hum.getName() + " Won the game");
								rec.closeFile();
							}
							System.out.println("Game Over!!!" + hum.getName() + " Wins!");
							System.exit(0);
						}
						else if (hum.checkCat())
						{
							if(args.length>2)
							{
								rec.record("The game was tie");
								rec.closeFile();
							}
							System.out.println("Game Over!!! It's a tie.");
							System.exit(0);
						}
						
						play = false;
					}	
					else 
					{
						System.out.println("Please enter an open spot between 1 and 9");
					}
				}
				
				boolean secondPlayer = true;
				while(secondPlayer)
				{
					int randomSpace = comp.randomSpace();
					System.out.println(comp.getName() + " enter an open spot to mark: " + randomSpace);
					comp.insertO(randomSpace);
					if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + randomSpace);
						
						}
					gameBoard.displayBoard();
					
					if (comp.checkWin("O"))
					{
						if(args.length>2)
							{
								rec.record(comp.getName() + " Won the game");
								rec.closeFile();
							}
						System.out.println("Game Over!!!" + comp.getName() + " Wins!");
						System.exit(0);
					}
					else if (comp.checkCat())
					{
						if(args.length>2)
							{
								rec.record("The game ended in a tie ");
								rec.closeFile();
							}
						System.out.println("Game Over!!! It's a tie.");
						System.exit(0);
					}
					
					secondPlayer = false;
				
				}
				
			}
			
		}//ends here (human vs random)
		
		//plays random vs human
		else if (p1.equalsIgnoreCase("random") && p2.equalsIgnoreCase("human"))
		{
			
			HumanPlayer hum = new HumanPlayer("Human Plaue O");
			RandomComputerPlayer comp = new RandomComputerPlayer("Random Plaue X");
			
			boolean main = true;
			while (main)
			{
				boolean secondPlayer = true;
				while(secondPlayer)
				{
					int randomSpace = comp.randomSpace();
					System.out.println(comp.getName() + " enter an open spot to mark: " + randomSpace);
					comp.insertX(randomSpace);
					if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + randomSpace);
						
						}
					gameBoard.displayBoard();
					
					if (comp.checkWin("X"))
					{
						if(args.length>2)
							{
								rec.record(comp.getName() + " Won the game");
								rec.closeFile();
							}
						System.out.println("Game Over!!!" + comp.getName() + " Wins!");
						System.exit(0);
					}
					else if (comp.checkCat())
					{
						if(args.length>2)
							{
								rec.record("The game ended in a tie");
								rec.closeFile();
							}
						System.out.println("Game Over!!! It's a tie.");
						System.exit(0);
					}
					
					secondPlayer = false;
				
				}
				
				boolean play = true;
				while(play)
				{
					System.out.print(hum.getName() + " Enter an open spot to mark: ");
					int space = keyboard.nextInt();
					if(hum.checkAvailable(space))
					{
						hum.insertO(space);
						if(args.length>2)
						{
						rec.record(hum.getName() + " made a move to space: " + space);
						
						}
						gameBoard.displayBoard();
						if (hum.checkWin("O"))
						{
							if(args.length>2)
							{
								rec.record(hum.getName() + " Won the game");
								rec.closeFile();
							}
							System.out.println("Game Over!!!" + hum.getName() + " Wins!");
							System.exit(0);
						}
						else if (hum.checkCat())
						{
							if(args.length>2)
							{
								rec.record("The game ended in a tie");
								rec.closeFile();
							}
							System.out.println("Game Over!!! It's a tie.");
							System.exit(0);
						}
						
						play = false;
					}	
					else 
					{
						System.out.println("Please enter an open spot between 1 and 9");
					}
				}
				
				
				
			}
			
		}//ends here (random vs human)
		
		//plays random vs naive
		else if (p1.equalsIgnoreCase("random") && p2.equalsIgnoreCase("naive"))
		{
			
			RandomComputerPlayer comp2 = new RandomComputerPlayer("Random Plaue X");
			NaiveComputerPlayer comp = new NaiveComputerPlayer("Naive Plaue O");
			
			boolean main = true;
			while (main)
			{
				boolean play = true;
				//plays the first player
				while(play)
				{
					int randomSpace = comp2.randomSpace();
					System.out.println(comp2.getName() + " enter an open spot to mark: " + randomSpace);
					comp2.insertX(randomSpace);
					if(args.length>2)
						{
						rec.record(comp2.getName() + " made a move to space: " + randomSpace);
						
						}
					gameBoard.displayBoard();
					
					if (comp2.checkWin("X"))
					{
						if(args.length>2)
							{
								rec.record(comp2.getName() + " Won the game");
								rec.closeFile();
							}
						System.out.println("Game Over!!!" + comp2.getName() + " Wins!");
						System.exit(0);
					}
					else if (comp2.checkCat())
					{
						if(args.length>2)
							{
								rec.record("The game was tie");
								rec.closeFile();
							}
						System.out.println("Game Over!!! It's a tie.");
						System.exit(0);
					}
					
					play = false;
				}
				
				boolean secondPlayer = true;
				while(secondPlayer)
				{
					int naiveSpace = comp.firstAvailableSpace();
					System.out.println(comp.getName() + " enter an open spot to mark: " + naiveSpace);
					comp.insertO(naiveSpace);
					if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + naiveSpace);
						
						}
					gameBoard.displayBoard();
					
					if (comp.checkWin("O"))
					{
						if(args.length>2)
							{
								rec.record(comp.getName() + " Won the game");
								rec.closeFile();
							}
						System.out.println("Game Over!!!" + comp.getName() + " Wins!");
						System.exit(0);
					}
					else if (comp.checkCat())
					{
						if(args.length>2)
							{
								rec.record("The game was a tie");
								rec.closeFile();
							}
						System.out.println("Game Over!!! It's a tie.");
						System.exit(0);
					}
					
					secondPlayer = false;
				
				}
				
			}
			
		}//ends here (random vs naive)
		
		//plays naive vs random
		else if (p1.equalsIgnoreCase("naive") && p2.equalsIgnoreCase("random"))
		{
			RandomComputerPlayer comp2 = new RandomComputerPlayer("Random Plaue O");
			NaiveComputerPlayer comp = new NaiveComputerPlayer("Naive Plaue X");
			
			boolean main = true;
			while (main)
			{
				boolean secondPlayer = true;
				while(secondPlayer)
				{
					int naiveSpace = comp.firstAvailableSpace();
					System.out.println(comp.getName() + " enter an open spot to mark: " + naiveSpace);
					comp.insertX(naiveSpace);
					if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + naiveSpace);
						
						}
					gameBoard.displayBoard();
					
					if (comp.checkWin("X"))
					{
						if(args.length>2)
							{
								rec.record(comp.getName() + " Won the game");
								rec.closeFile();
							}
						System.out.println("Game Over!!!" + comp.getName() + " Wins!");
						System.exit(0);
					}
					else if (comp.checkCat())
					{
						if(args.length>2)
							{
								rec.record("The game ended in a tie");
								rec.closeFile();
							}
						System.out.println("Game Over!!! It's a tie.");
						System.exit(0);
					}
					
					secondPlayer = false;
				
				}
				
				
				boolean play = true;
				while(play)
				{
					int randomSpace = comp2.randomSpace();
					System.out.println(comp2.getName() + " enter an open spot to mark: " + randomSpace);
					comp2.insertO(randomSpace);
					if(args.length>2)
						{
						rec.record(comp2.getName() + " made a move to space: " + randomSpace);
						
						}
					gameBoard.displayBoard();
					
					if (comp2.checkWin("O"))
					{
						if(args.length>2)
							{
								rec.record(comp2.getName() + " Won the game");
								rec.closeFile();
							}
						System.out.println("Game Over!!!" + comp2.getName() + " Wins!");
						System.exit(0);
					}
					else if (comp2.checkCat())
					{
						if(args.length>2)
							{
								rec.record("The game ended in a tie");
								rec.closeFile();
							}
						System.out.println("Game Over!!! It's a tie.");
						System.exit(0);
					}
					
					play = false;
				}
				
				
				
			}
			
		}//ends here (naive vs random)
		
		//plays naive vs naive
		else if (p1.equalsIgnoreCase("naive") && p2.equalsIgnoreCase("naive"))
		{
			
			NaiveComputerPlayer comp2 = new NaiveComputerPlayer("Naive Plaue X");
			NaiveComputerPlayer comp = new NaiveComputerPlayer("Naive Plaue O");
			
			boolean main = true;
			while (main)
			{
				boolean play = true;
				//plays the first player
				while(play)
				{
					int naiveSpace = comp2.firstAvailableSpace();
					System.out.println(comp2.getName() + " enter an open spot to mark: " + naiveSpace);
					comp2.insertX(naiveSpace);
					if(args.length>2)
						{
						rec.record(comp2.getName() + " made a move to space: " + naiveSpace);
						
						}
					gameBoard.displayBoard();
					
					if (comp2.checkWin("X"))
					{
						if(args.length>2)
							{
								rec.record(comp2.getName() + " Won the game");
								rec.closeFile();
							}
						System.out.println("Game Over!!!" + comp2.getName() + " Wins!");
						System.exit(0);
					}
					else if (comp2.checkCat())
					{
						if(args.length>2)
							{
								rec.record("THe game ended in a tie");
								rec.closeFile();
							}
						System.out.println("Game Over!!! It's a tie.");
						System.exit(0);
					}
					
					play = false;
				}
				
				boolean secondPlayer = true;
				while(secondPlayer)
				{
					int naiveSpace = comp.firstAvailableSpace();
					System.out.println(comp.getName() + " enter an open spot to mark: " + naiveSpace);
					comp.insertO(naiveSpace);
					if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + naiveSpace);
						
						}
					gameBoard.displayBoard();
					
					if (comp.checkWin("O"))
					{
						if(args.length>2)
							{
								rec.record(comp.getName() + " Won the game");
								rec.closeFile();
							}
						System.out.println("Game Over!!!" + comp.getName() + " Wins!");
						System.exit(0);
					}
					else if (comp.checkCat())
					{
						if(args.length>2)
							{
								rec.record("The game ended in a tie");
								rec.closeFile();
							}
						System.out.println("Game Over!!! It's a tie.");
						System.exit(0);
					}
					
					secondPlayer = false;
				
				}
				
			}
			
		}//ends here (naive vs naive)
		
		//plays random vs random
		else if (p1.equalsIgnoreCase("random") && p2.equalsIgnoreCase("random"))
		{
			RandomComputerPlayer comp2 = new RandomComputerPlayer("Random Plaue O");
			RandomComputerPlayer comp = new RandomComputerPlayer("Random Plaue X");
			
			boolean main = true;
			while (main)
			{
				boolean secondPlayer = true;
				while(secondPlayer)
				{
					int randomSpace = comp.randomSpace();
					System.out.println(comp.getName() + " enter an open spot to mark: " + randomSpace);
					comp.insertX(randomSpace);
					if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + randomSpace);
						
						}
					gameBoard.displayBoard();
					
					if (comp.checkWin("X"))
					{
						if(args.length>2)
							{
								rec.record(comp.getName() + " Won the game");
								rec.closeFile();
							}
						System.out.println("Game Over!!!" + comp.getName() + " Wins!");
						System.exit(0);
					}
					else if (comp.checkCat())
					{
						if(args.length>2)
							{
								rec.record("The game ended in a tie");
								rec.closeFile();
							}
						System.out.println("Game Over!!! It's a tie.");
						System.exit(0);
					}
					
					secondPlayer = false;
				
				}
				
				
				boolean play = true;
				while(play)
				{
					int randomSpace = comp2.randomSpace();
					System.out.println(comp2.getName() + " enter an open spot to mark: " + randomSpace);
					comp2.insertO(randomSpace);
					if(args.length>2)
						{
						rec.record(comp2.getName() + " made a move to space: " + randomSpace);
						
						}
					gameBoard.displayBoard();
					
					if (comp2.checkWin("O"))
					{
						if(args.length>2)
							{
								rec.record(comp2.getName() + " Won the game");
								rec.closeFile();
							}
						System.out.println("Game Over!!!" + comp2.getName() + " Wins!");
						System.exit(0);
					}
					else if (comp2.checkCat())
					{
						if(args.length>2)
							{
								rec.record("The game ended in a tie");
								rec.closeFile();
							}
						System.out.println("Game Over!!! It's a tie.");
						System.exit(0);
					}
					
					play = false;
				}
				
				
				
			}
			
		}//ends here (random vs random)

		//plays human vs cutthroat
		else if (p1.equalsIgnoreCase("human") && p2.equalsIgnoreCase("cutthroat"))
		{
			
			HumanPlayer hum = new HumanPlayer("Human Plaue X");
			CutThroatComputerPlayer comp = new CutThroatComputerPlayer("CutThroat Plaue O");
			
			boolean main = true;
			while (main)
			{
				//Loop that control first player's turn
				boolean play = true;
				while(play)
				{

					System.out.print(hum.getName() + " Enter an open spot to mark: ");
					int space = keyboard.nextInt();
					if(hum.checkAvailable(space))
					{
						hum.insertX(space);
						if(args.length>2)
						{
						rec.record(hum.getName() + " made a move to space: " + space);
						
						}
						gameBoard.displayBoard();
						if (hum.checkWin("X"))
						{
							if(args.length>2)
							{
								rec.record(hum.getName() + " Won the game");
								rec.closeFile();
							}
							System.out.println("Game Over!!!" + hum.getName() + " Wins!");
							System.exit(0);
						}
						else if (hum.checkCat())
						{
							if(args.length>2)
							{
								rec.record("The game ended in a tie");
								rec.closeFile();
							}
							System.out.println("Game Over!!! It's a tie.");
							System.exit(0);
						}
						
						play = false;
					}	
					else 
					{
						System.out.println("Please enter an open spot between 1 and 9");
					}
				}

			

			boolean decision = true;
			boolean secondPlayer = true;
			while(secondPlayer)
				{
					while(decision)
					{

						if(comp.win("O") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.win("O"));
							comp.insertO(comp.win("O"));
							if(args.length>2)
							{
								rec.record(comp.getName() + " made a move to space: " + comp.win("O"));
							
							}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.block("X") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.block("X"));
							comp.insertO(comp.block("X"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.block("X"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.fork("O") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.fork("O"));
							comp.insertO(comp.fork("O"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.fork("O"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.blockOpponentFork("X") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.blockOpponentFork("X"));
							comp.insertO(comp.blockOpponentFork("X"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.blockOpponentFork("X"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}

						if(comp.center() !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.center());
							comp.insertO(comp.center());
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.center());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.oppositeCorner("X") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.oppositeCorner("X"));
							comp.insertO(comp.oppositeCorner("X"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.oppositeCorner("X"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.emptyCorner() !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.emptyCorner());
							comp.insertO(comp.emptyCorner());
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.emptyCorner());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.emptySide() !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.emptySide());
							comp.insertO(comp.emptySide());
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.emptySide());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}

				}
					
						if (comp.checkWin("O"))
						{
							if(args.length>2)
							{
								rec.record(comp.getName() + " Won the game");
								rec.closeFile();
							}
							System.out.println("Game Over!!!" + comp.getName() + " Wins!");
							System.exit(0);
						}
						else if (comp.checkCat())
						{
							if(args.length>2)
							{
								rec.record("The game ended in a tie");
								rec.closeFile();
							}
							System.out.println("Game Over!!! It's a tie.");
							System.exit(0);
						}
						play = true;
						secondPlayer = false;
				}//Where second player ends
			}
		}//ends here (human vs cutThroat)

		//plays cutthroat vs human
		else if (p1.equalsIgnoreCase("cutthroat") && p2.equalsIgnoreCase("human"))
		{
			
			HumanPlayer hum = new HumanPlayer("Human Plaue O");
			CutThroatComputerPlayer comp = new CutThroatComputerPlayer("CutThroat Plaue X");
			boolean decision = true;
			boolean secondPlayer = true;

			boolean main = true;
			while (main)
			{
				//took out boolean at the beginning so it can be changed in the second person's turn
				
				while(secondPlayer)
				{

					while(decision)
					{

						if(comp.win("X") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.win("X"));
							comp.insertX(comp.win("X"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.win("X"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.block("O") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.block("O"));
							comp.insertX(comp.block("O"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.block("O"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.fork("X") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.fork("X"));
							comp.insertX(comp.fork("X"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.fork("X"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.blockOpponentFork("O") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.blockOpponentFork("O"));
							comp.insertX(comp.blockOpponentFork("O"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.blockOpponentFork("O"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.oppositeCorner("O") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.oppositeCorner("O"));
							comp.insertX(comp.oppositeCorner("O"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.oppositeCorner("O"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.emptyCorner() !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.emptyCorner());
							comp.insertX(comp.emptyCorner());
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.emptyCorner());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.emptySide() !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.emptySide());
							comp.insertX(comp.emptySide());
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.emptySide());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.center() !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.center());
							comp.insertX(comp.center());
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.center());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}

					}
					
						if (comp.checkWin("X"))
						{
						System.out.println("Game Over!!!" + comp.getName() + " Wins!");
						if(args.length>2)
							{
								rec.record(comp.getName() + " Won the game");
								rec.closeFile();
							}
						System.exit(0);
						}
						else if (comp.checkCat())
						{
						System.out.println("Game Over!!! It's a tie.");
						if(args.length>2)
							{
								rec.record("The game ended in a tie");
								rec.closeFile();
							}
						System.exit(0);
						}
						secondPlayer = false;
				}

				boolean play = true;
				
				while(play)
				{
					
					
					System.out.print(hum.getName() + " Enter an open spot to mark: ");
					int space = keyboard.nextInt();
					if(hum.checkAvailable(space))
					{
						hum.insertO(space);
						if(args.length>2)
						{
						rec.record(hum.getName() + " made a move to space: " + space);
						
						}
						gameBoard.displayBoard();

						if (hum.checkWin("O"))
						{
							if(args.length>2)
							{
								rec.record(hum.getName() + " Won the game");
								rec.closeFile();
							}
							System.out.println("Game Over!!!" + hum.getName() + " Wins!");
							System.exit(0);
						}
						else if (hum.checkCat())
						{
							if(args.length>2)
							{
								rec.record("The game ended in a tie");
								rec.closeFile();
							}
							System.out.println("Game Over!!! It's a tie.");
							System.exit(0);
						}
						secondPlayer = true;
						decision = true;
						play = false;
					}	
					else 
					{
						System.out.println("Please enter an open spot between 1 and 9");
					}
				}
				
				
			}
				
		}//ends here(cutThroat vs human)
			
		//plays cutthroat vs naive
		else if (p1.equalsIgnoreCase("cutthroat") && p2.equalsIgnoreCase("naive"))
		{
			
			NaiveComputerPlayer comp2 = new NaiveComputerPlayer("Naive Plaue O");
			CutThroatComputerPlayer comp = new CutThroatComputerPlayer("CutThroat Plaue X");
			boolean decision = true;
			boolean secondPlayer = true;
			
			boolean main = true;
			while (main)
			{
				//took out boolean at the beginning so it can be changed in the second person's turn
				//boolean secondPlayer = true;
				while(secondPlayer)
				{

					while(decision)
					{

						if(comp.win("X") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.win("X"));
							comp.insertX(comp.win("X"));if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.win("X"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.block("O") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.block("O"));
							comp.insertX(comp.block("O"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.block("O"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.fork("X") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.fork("X"));
							comp.insertX(comp.fork("X"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.fork("X"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.blockOpponentFork("O") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.blockOpponentFork("O"));
							comp.insertX(comp.blockOpponentFork("O"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.blockOpponentFork("O"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.oppositeCorner("O") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.oppositeCorner("O"));
							comp.insertX(comp.oppositeCorner("O"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.oppositeCorner("O"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.emptyCorner() !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.emptyCorner());
							comp.insertX(comp.emptyCorner());
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.emptyCorner());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.emptySide() !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.emptySide());
							comp.insertX(comp.emptySide());
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.emptySide());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.center() !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.center());
							comp.insertX(comp.center());
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.center());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}

					}
					
						if (comp.checkWin("X"))
						{
							if(args.length>2)
							{
								rec.record(comp.getName() + " Won the game");
								rec.closeFile();
							}
						System.out.println("Game Over!!!" + comp.getName() + " Wins!");
						System.exit(0);
						}
						else if (comp.checkCat())
						{
							if(args.length>2)
							{
								rec.record("The game was a tie");
								rec.closeFile();
							}
						System.out.println("Game Over!!! It's a tie.");
						System.exit(0);
						}
						secondPlayer = false;
				}

				boolean play = true;

				while(play)
				{
					secondPlayer = true;
					decision = true;
					int naiveSpace = comp2.firstAvailableSpace();
					System.out.println(comp2.getName() + " enter an open spot to mark: " + naiveSpace);
					comp2.insertO(naiveSpace);
					if(args.length>2)
						{
						rec.record(comp2.getName() + " made a move to space: " + naiveSpace);
						
						}
					gameBoard.displayBoard();
					
					if (comp2.checkWin("O"))
					{
						if(args.length>2)
							{
								rec.record(comp2.getName() + " Won the game");
								rec.closeFile();
							}
						System.out.println("Game Over!!!" + comp2.getName() + " Wins!");
						System.exit(0);
					}
					else if (comp2.checkCat())
					{
						if(args.length>2)
							{
								rec.record("The game was a cat");
								rec.closeFile();
							}
						System.out.println("Game Over!!! It's a tie.");
						System.exit(0);
					}
					
					play = false;
				}
				
			}
		}//ends here(cutThroat vs naive)

		//plays cutthroat vs random
		else if (p1.equalsIgnoreCase("cutthroat") && p2.equalsIgnoreCase("random"))
		{
			
			RandomComputerPlayer comp2 = new RandomComputerPlayer("Random Plaue O");
			CutThroatComputerPlayer comp = new CutThroatComputerPlayer("CutThroat Plaue X");
			boolean decision = true;
			boolean secondPlayer = true;
			
			boolean main = true;
			while (main)
			{
				
				while(secondPlayer)
				{

					while(decision)
					{

						if(comp.win("X") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.win("X"));
							comp.insertX(comp.win("X"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.win("X"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.block("O") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.block("O"));
							comp.insertX(comp.block("O"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.block("O"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.fork("X") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.fork("X"));
							comp.insertX(comp.fork("X"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.fork("X"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.blockOpponentFork("O") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.blockOpponentFork("O"));
							comp.insertX(comp.blockOpponentFork("O"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.blockOpponentFork("O"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.oppositeCorner("O") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.oppositeCorner("O"));
							comp.insertX(comp.oppositeCorner("O"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.oppositeCorner("O"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.emptyCorner() !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.emptyCorner());
							comp.insertX(comp.emptyCorner());
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.emptyCorner());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.emptySide() !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.emptySide());
							comp.insertX(comp.emptySide());
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.emptySide());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.center() !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.center());
							comp.insertX(comp.center());
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.center());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}

				}
					
						
						
						if (comp.checkWin("X"))
						{
							if(args.length>2)
							{
								rec.record(comp.getName() + " Won the game");
								rec.closeFile();
							}
						System.out.println("Game Over!!!" + comp.getName() + " Wins!");
						System.exit(0);
						}
						else if (comp.checkCat())
						{
							if(args.length>2)
							{
								rec.record("The game was a cat");
								rec.closeFile();
							}
						System.out.println("Game Over!!! It's a tie.");
						System.exit(0);
						}
						secondPlayer = false;
				}//Where second player ends

				
				boolean play = true;
				while(play)
				{
					secondPlayer = true;
					decision = true;
					int randomSpace = comp2.randomSpace();
					System.out.println(comp2.getName() + " enter an open spot to mark: " + randomSpace);
					comp2.insertO(randomSpace);
					if(args.length>2)
						{
						rec.record(comp2.getName() + " made a move to space: " + randomSpace);
						
						}
					gameBoard.displayBoard();
					
					if (comp2.checkWin("O"))
					{
						if(args.length>2)
							{
								rec.record(comp2.getName() + " Won the game");
								rec.closeFile();
							}
						System.out.println("Game Over!!!" + comp2.getName() + " Wins!");
						System.exit(0);
					}
					else if (comp2.checkCat())
					{
						if(args.length>2)
							{
								rec.record("The game was a Cat");
								rec.closeFile();
							}
						System.out.println("Game Over!!! It's a tie.");
						System.exit(0);
					}
					
					play = false;
				}
				
			}
		}//ends here(cutThroat vs random)

		//plays naive vs cutthroat
		else if (p1.equalsIgnoreCase("naive") && p2.equalsIgnoreCase("cutthroat"))
		{
			
			NaiveComputerPlayer comp2 = new NaiveComputerPlayer("Naive Plaue X");
			CutThroatComputerPlayer comp = new CutThroatComputerPlayer("CutThroat Plaue O");
			boolean decision = true;
			boolean secondPlayer = true;
			
			boolean main = true;
			while (main)
			{
				//plays the first player
				boolean play = true;

				while(play)
				{
					secondPlayer = true;
					decision = true;
					int naiveSpace = comp2.firstAvailableSpace();
					System.out.println(comp2.getName() + " enter an open spot to mark: " + naiveSpace);
					comp2.insertX(naiveSpace);
					if(args.length>2)
						{
						rec.record(comp2.getName() + " made a move to space: " + naiveSpace);
						
						}
					gameBoard.displayBoard();
					
					if (comp2.checkWin("X"))
					{
						if(args.length>2)
							{
								rec.record(comp2.getName() + " Won the game");
								rec.closeFile();
							}
						System.out.println("Game Over!!!" + comp2.getName() + " Wins!");
						System.exit(0);
					}
					else if (comp2.checkCat())
					{
						if(args.length>2)
							{
								rec.record("The game was tie");
								rec.closeFile();
							}
						System.out.println("Game Over!!! It's a tie.");
						System.exit(0);
					}
					
					play = false;
				}

			while(secondPlayer)
				{
						System.out.println("It got in !!!!");
					while(decision)
					{

						if(comp.win("O") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.win("O"));
							comp.insertO(comp.win("O"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.win("O"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.block("X") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.block("X"));
							comp.insertO(comp.block("X"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.block("X"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.fork("O") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.fork("O"));
							comp.insertO(comp.fork("O"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.fork("O"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.blockOpponentFork("X") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.blockOpponentFork("X"));
							comp.insertO(comp.blockOpponentFork("X"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.blockOpponentFork("X"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}

						if(comp.center() !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.center());
							comp.insertO(comp.center());
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.center());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.oppositeCorner("X") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.oppositeCorner("X"));
							comp.insertO(comp.oppositeCorner("X"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.oppositeCorner("X"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.emptyCorner() !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.emptyCorner());
							comp.insertO(comp.emptyCorner());
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.emptyCorner());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.emptySide() !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.emptySide());
							comp.insertO(comp.emptySide());
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.emptySide());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}

				}
					
						
						
						if (comp.checkWin("O"))
						{
							if(args.length>2)
							{
								rec.record(comp.getName() + " Won the game");
								rec.closeFile();
							}
						System.out.println("Game Over!!!" + comp.getName() + " Wins!");
						System.exit(0);
						}
						else if (comp.checkCat())
						{
							if(args.length>2)
							{
								rec.record("The game was a tie");
								rec.closeFile();
							}
						System.out.println("Game Over!!! It's a tie.");
						System.exit(0);
						}
						play = true;
						secondPlayer = false;
				}//Where second player ends

			}
		}//ends here(naive vs cutthroat)

		//plays random vs cutthroat
		else if (p1.equalsIgnoreCase("random") && p2.equalsIgnoreCase("cutthroat"))
		{
			
			RandomComputerPlayer comp2 = new RandomComputerPlayer("Random Plaue X");
			CutThroatComputerPlayer comp = new CutThroatComputerPlayer("CutThroat Plaue O");
			boolean decision = true;
			boolean secondPlayer = true;
			
			boolean main = true;
			while (main)
			{
				
				boolean play = true;
				//plays the first player
				while(play)
				{
					secondPlayer = true;
					decision = true;
					int randomSpace = comp2.randomSpace();
					System.out.println(comp2.getName() + " enter an open spot to mark: " + randomSpace);
					comp2.insertX(randomSpace);
					if(args.length>2)
						{
						rec.record(comp2.getName() + " made a move to space: " + randomSpace);
						
						}
					gameBoard.displayBoard();
					
					if (comp2.checkWin("X"))
					{
						if(args.length>2)
							{
								rec.record(comp2.getName() + " Won the game");
								rec.closeFile();
							}
						System.out.println("Game Over!!!" + comp2.getName() + " Wins!");
						System.exit(0);
					}
					else if (comp2.checkCat())
					{
						if(args.length>2)
							{
								rec.record("The game was tie");
								rec.closeFile();
							}
						System.out.println("Game Over!!! It's a tie.");
						System.exit(0);
					}
					
					play = false;
				}

				
			while(secondPlayer)
				{
					while(decision)
					{

						if(comp.win("O") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.win("O"));
							comp.insertO(comp.win("O"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.win("O"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.block("X") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.block("X"));
							comp.insertO(comp.block("X"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.block("X"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.fork("O") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.fork("O"));
							comp.insertO(comp.fork("O"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.fork("O"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.blockOpponentFork("X") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.blockOpponentFork("X"));
							comp.insertO(comp.blockOpponentFork("X"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.blockOpponentFork("X"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}

						if(comp.center() !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.center());
							comp.insertO(comp.center());
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.center());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.oppositeCorner("X") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.oppositeCorner("X"));
							comp.insertO(comp.oppositeCorner("X"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.oppositeCorner("X"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.emptyCorner() !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.emptyCorner());
							comp.insertO(comp.emptyCorner());
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.emptyCorner());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.emptySide() !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.emptySide());
							comp.insertO(comp.emptySide());
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.emptySide());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}

				}
					
						if (comp.checkWin("O"))
						{
							if(args.length>2)
							{
								rec.record(comp.getName() + " Won the game");
								rec.closeFile();
							}
						System.out.println("Game Over!!!" + comp.getName() + " Wins!");
						System.exit(0);
						}
						else if (comp.checkCat())
						{
							if(args.length>2)
							{
								rec.record("The game was a tie");
								rec.closeFile();
							}
						System.out.println("Game Over!!! It's a tie.");
						System.exit(0);
						}
						play = true;
						secondPlayer = false;
				}//Where second player ends

				
				
				
			}
		}//ends here(random vs cutthroat)	

		//plays cutthroat vs cutthroat
		else if (p1.equalsIgnoreCase("cutthroat") && p2.equalsIgnoreCase("cutthroat"))
		{
			
			CutThroatComputerPlayer comp2 = new CutThroatComputerPlayer("CutThroat Plaue X");
			CutThroatComputerPlayer comp = new CutThroatComputerPlayer("CutThroat Plaue O");
			boolean decision = true;
			boolean secondPlayer = true;
			boolean decision1 = true;
			
			boolean main = true;
			while (main)
			{
				
				boolean play = true;
				//plays the first player
				while(play)
				{

					while(decision1)
					{

						if(comp2.win("X") !=0)
						{
							System.out.println(comp2.getName() + " enter an open spot to mark: " + comp2.win("X"));
							comp2.insertX(comp2.win("X"));
							if(args.length>2)
						{
						rec.record(comp2.getName() + " made a move to space: " + comp2.win("X"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp2.block("O") !=0)
						{
							System.out.println(comp2.getName() + " enter an open spot to mark: " + comp2.block("O"));
							comp2.insertX(comp2.block("O"));
							if(args.length>2)
						{
						rec.record(comp2.getName() + " made a move to space: " + comp2.block("O"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp2.fork("X") !=0)
						{
							System.out.println(comp2.getName() + " enter an open spot to mark: " + comp2.fork("X"));
							comp2.insertX(comp2.fork("X"));
							if(args.length>2)
						{
						rec.record(comp2.getName() + " made a move to space: " + comp2.fork("X"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp2.blockOpponentFork("O") !=0)
						{
							System.out.println(comp2.getName() + " enter an open spot to mark: " + comp2.blockOpponentFork("O"));
							comp2.insertX(comp2.blockOpponentFork("O"));
							if(args.length>2)
						{
						rec.record(comp2.getName() + " made a move to space: " + comp2.blockOpponentFork("O"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp2.oppositeCorner("O") !=0)
						{
							System.out.println(comp2.getName() + " enter an open spot to mark: " + comp2.oppositeCorner("O"));
							comp2.insertX(comp2.oppositeCorner("O"));
							if(args.length>2)
						{
						rec.record(comp2.getName() + " made a move to space: " + comp2.oppositeCorner("O"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp2.emptyCorner() !=0)
						{
							System.out.println(comp2.getName() + " enter an open spot to mark: " + comp2.emptyCorner());
							comp2.insertX(comp2.emptyCorner());
							if(args.length>2)
						{
						rec.record(comp2.getName() + " made a move to space: " + comp2.emptyCorner());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp2.emptySide() !=0)
						{
							System.out.println(comp2.getName() + " enter an open spot to mark: " + comp2.emptySide());
							comp2.insertX(comp2.emptySide());
							if(args.length>2)
						{
						rec.record(comp2.getName() + " made a move to space: " + comp2.emptySide());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp2.center() !=0)
						{
							System.out.println(comp2.getName() + " enter an open spot to mark: " + comp2.center());
							comp2.insertX(comp2.center());
							if(args.length>2)
						{
						rec.record(comp2.getName() + " made a move to space: " + comp2.center());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}

				}
					
						
						
						if (comp.checkWin("X"))
						{
							if(args.length>2)
							{
								rec.record(comp2.getName() + " Won the game");
								rec.closeFile();
							}
						System.out.println("Game Over!!!" + comp2.getName() + " Wins!");
						System.exit(0);
						}
						else if (comp.checkCat())
						{
							if(args.length>2)
							{
								rec.record("The game was a Cat");
								rec.closeFile();
							}
						System.out.println("Game Over!!! It's a tie.");
						System.exit(0);
						}
						secondPlayer = true;
						decision = true;
						play = false;
				}//Where second player ends

				
			while(secondPlayer)
				{
						System.out.println("It got in !!!!");
					while(decision)
					{

						if(comp.win("O") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.win("O"));
							comp.insertO(comp.win("O"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.win("O"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.block("X") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.block("X"));
							comp.insertO(comp.block("X"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.block("X"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.fork("O") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.fork("O"));
							comp.insertO(comp.fork("O"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.fork("O"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.blockOpponentFork("X") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.blockOpponentFork("X"));
							comp.insertO(comp.blockOpponentFork("X"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.blockOpponentFork("X"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}

						if(comp.center() !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.center());
							comp.insertO(comp.center());
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.center());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.oppositeCorner("X") !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.oppositeCorner("X"));
							comp.insertO(comp.oppositeCorner("X"));
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.oppositeCorner("X"));
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.emptyCorner() !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.emptyCorner());
							comp.insertO(comp.emptyCorner());
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.emptyCorner());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}
						if(comp.emptySide() !=0)
						{
							System.out.println(comp.getName() + " enter an open spot to mark: " + comp.emptySide());
							comp.insertO(comp.emptySide());
							if(args.length>2)
						{
						rec.record(comp.getName() + " made a move to space: " + comp.emptySide());
						
						}
							gameBoard.displayBoard();
							decision = false;
							break;
						}

				}
				
						if (comp.checkWin("O"))
						{
							if(args.length>2)
							{
								rec.record(comp.getName() + " Won the game");
								rec.closeFile();
							}
						System.out.println("Game Over!!!" + comp.getName() + " Wins!");
						System.exit(0);
						}
						else if (comp.checkCat())
						{
							if(args.length>2)
							{
								rec.record("The game was a Cat");
								rec.closeFile();
							}
						System.out.println("Game Over!!! It's a tie.");
						System.exit(0);
						}

						play = true;
						decision1 = true;
						secondPlayer = false;
				}//Where second player ends

				
				
				
			}
		}//ends here(cutthroat vs cutthroat)			
	}
}

