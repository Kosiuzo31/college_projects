/** This is a board interface that gives you a five by five array to make a tictactoe board.
	It also give you two methods that will be defined later, which are to make the board and display the board.
	**/
public interface Board {

	public static final String [][] board = new String [5][5];
	
	//This method will make the board 
	public void makeBoard();
	
	//This method will display the board 
	public void displayBoard();
	
	
	
}
