readme.txt
Developers: Kosi Uzodinma and Setul Patel

To run the file with a standard version of human versus human you will need to do two things

1. make compile
2. make run

If you want to go against a computer you will have to input one of the following in a certain format. 

random- A computer that will randomly place sports on the board
naive - A computer that plays the first available spot.
cutthroat- A computer that knows how to either beat you every time or tie with you every time.

Example inputs
1. java TicTacToe human random
2. java TicTacToe human naive
3. java TicTacToe human cutthroat
4. java TicTacToe random random

You can mix and match human, random, naive, and cutthroat in any combination you want.

If you happen to want to recompile this program you can use the following commands to clean and compile it again.
1. make clean
2. make compile