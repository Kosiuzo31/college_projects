/**This is the CutThroat class that plays hard*/
public class CutThroatComputerPlayer extends ComputerPlayer
{
	public CutThroatComputerPlayer()
	{
		super("CutThroatCPU");
	}

	public CutThroatComputerPlayer(String inputName)
	{
		super(inputName);
	}
//This checks if the CutThroat could win in the next move
//The inputLetter is needed to check if X or O
	public int win(String inputLetter)
	{
		if((Board.board[0][0] == inputLetter) && (Board.board[0][2] == inputLetter) && (Board.board[0][4] == " ") )
			return 9;
		else if((Board.board[2][0] == inputLetter) && (Board.board[2][2] == inputLetter) && (Board.board[2][4] == " ") )
			return 6;
		else if((Board.board[4][0] == inputLetter) && (Board.board[4][2] == inputLetter) && (Board.board[4][4] == " ") )
			return 3;
		else if((Board.board[0][2] == inputLetter) && (Board.board[0][4] == inputLetter) && (Board.board[0][0] == " ") )
			return 7;
		else if((Board.board[2][2] == inputLetter) && (Board.board[2][4] == inputLetter) && (Board.board[2][0] == " ") )
			return 4;
		else if((Board.board[4][2] == inputLetter) && (Board.board[4][4] == inputLetter) && (Board.board[4][0] == " ") )
			return 1;
		else if((Board.board[0][0] == inputLetter) && (Board.board[2][0] == inputLetter) && (Board.board[4][0] == " ") )
			return 1;
		else if((Board.board[0][2] == inputLetter) && (Board.board[2][2] == inputLetter) && (Board.board[4][2] == " ") )
			return 2;
		else if((Board.board[0][4] == inputLetter) && (Board.board[2][4] == inputLetter) && (Board.board[4][4] == " ") )
			return 3;
		else if((Board.board[2][0] == inputLetter) && (Board.board[4][0] == inputLetter) && (Board.board[0][0] == " ") )
			return 7;
		else if((Board.board[2][2] == inputLetter) && (Board.board[4][2] == inputLetter) && (Board.board[0][2] == " ") )
			return 8;
		else if((Board.board[2][4] == inputLetter) && (Board.board[4][4] == inputLetter) && (Board.board[0][4] == " ") )
			return 9;
		else if((Board.board[2][2] == inputLetter) && (Board.board[4][4] == inputLetter) && (Board.board[0][0] == " ") )
			return 7;
		else if((Board.board[0][0] == inputLetter) && (Board.board[2][2] == inputLetter) && (Board.board[4][4] == " ") )
			return 3;
		else if((Board.board[4][0] == inputLetter) && (Board.board[2][2] == inputLetter) && (Board.board[0][4] == " ") )
			return 9;
		else if((Board.board[0][4] == inputLetter) && (Board.board[2][2] == inputLetter) && (Board.board[4][0] == " ") )
			return 1;
		else if((Board.board[0][0] == inputLetter) && (Board.board[4][4] == inputLetter) && (Board.board[2][2] == " ") )
			return 5;
		else if((Board.board[0][4] == inputLetter) && (Board.board[4][0] == inputLetter) && (Board.board[2][2] == " ") )
			return 5;
		else if((Board.board[0][0] == inputLetter) && (Board.board[4][0] == inputLetter) && (Board.board[2][0] == " ") )
			return 4;
		else if((Board.board[0][4] == inputLetter) && (Board.board[4][4] == inputLetter) && (Board.board[2][4] == " ") )
			return 6;
		else if((Board.board[2][0] == inputLetter) && (Board.board[2][4] == inputLetter) && (Board.board[2][2] == " ") )
			return 5;
		else if((Board.board[0][2] == inputLetter) && (Board.board[4][2] == inputLetter) && (Board.board[2][2] == " ") )
			return 5;
		else if((Board.board[0][0] == inputLetter) && (Board.board[0][4] == inputLetter) && (Board.board[0][2] == " ") )
			return 8;
		else if((Board.board[4][0] == inputLetter) && (Board.board[4][4] == inputLetter) && (Board.board[4][2] == " ") )
			return 2;

		return 0;

	}

	//This checks if the CutThroat would lose in the next move
	//The inputLetter is needed to check if X or O
	public int block(String inputLetter)
	{
		if((Board.board[0][0] == inputLetter) && (Board.board[0][2] == inputLetter) && (Board.board[0][4] == " ") )
			return 9;
		else if((Board.board[2][0] == inputLetter) && (Board.board[2][2] == inputLetter) && (Board.board[2][4] == " ") )
			return 6;
		else if((Board.board[4][0] == inputLetter) && (Board.board[4][2] == inputLetter) && (Board.board[4][4] == " ") )
			return 3;
		else if((Board.board[0][2] == inputLetter) && (Board.board[0][4] == inputLetter) && (Board.board[0][0] == " ") )
			return 7;
		else if((Board.board[2][2] == inputLetter) && (Board.board[2][4] == inputLetter) && (Board.board[2][0] == " ") )
			return 4;
		else if((Board.board[4][2] == inputLetter) && (Board.board[4][4] == inputLetter) && (Board.board[4][0] == " ") )
			return 1;
		else if((Board.board[0][0] == inputLetter) && (Board.board[2][0] == inputLetter) && (Board.board[4][0] == " ") )
			return 1;
		else if((Board.board[0][2] == inputLetter) && (Board.board[2][2] == inputLetter) && (Board.board[4][2] == " ") )
			return 2;
		else if((Board.board[0][4] == inputLetter) && (Board.board[2][4] == inputLetter) && (Board.board[4][4] == " ") )
			return 3;
		else if((Board.board[2][0] == inputLetter) && (Board.board[4][0] == inputLetter) && (Board.board[0][0] == " ") )
			return 7;
		else if((Board.board[2][2] == inputLetter) && (Board.board[4][2] == inputLetter) && (Board.board[0][2] == " ") )
			return 8;
		else if((Board.board[2][4] == inputLetter) && (Board.board[4][4] == inputLetter) && (Board.board[0][4] == " ") )
			return 9;
		else if((Board.board[2][2] == inputLetter) && (Board.board[4][4] == inputLetter) && (Board.board[0][0] == " ") )
			return 7;
		else if((Board.board[0][0] == inputLetter) && (Board.board[2][2] == inputLetter) && (Board.board[4][4] == " ") )
			return 3;
		else if((Board.board[4][0] == inputLetter) && (Board.board[2][2] == inputLetter) && (Board.board[0][4] == " ") )
			return 9;
		else if((Board.board[0][4] == inputLetter) && (Board.board[2][2] == inputLetter) && (Board.board[4][0] == " ") )
			return 1;
		else if((Board.board[0][0] == inputLetter) && (Board.board[4][4] == inputLetter) && (Board.board[2][2] == " ") )
			return 5;
		else if((Board.board[0][4] == inputLetter) && (Board.board[4][0] == inputLetter) && (Board.board[2][2] == " ") )
			return 5;
		else if((Board.board[0][0] == inputLetter) && (Board.board[4][0] == inputLetter) && (Board.board[2][0] == " ") )
			return 4;
		else if((Board.board[0][4] == inputLetter) && (Board.board[4][4] == inputLetter) && (Board.board[2][4] == " ") )
			return 6;
		else if((Board.board[2][0] == inputLetter) && (Board.board[2][4] == inputLetter) && (Board.board[2][2] == " ") )
			return 5;
		else if((Board.board[0][2] == inputLetter) && (Board.board[4][2] == inputLetter) && (Board.board[2][2] == " ") )
			return 5;
		else if((Board.board[0][0] == inputLetter) && (Board.board[0][4] == inputLetter) && (Board.board[0][2] == " ") )
			return 8;
		else if((Board.board[4][0] == inputLetter) && (Board.board[4][4] == inputLetter) && (Board.board[4][2] == " ") )
			return 2;

		return 0;

	}

	//plays a fork method
	public int fork(String inputLetter)
	{
		if((Board.board[4][0] == inputLetter) && (Board.board[4][4] == inputLetter) && (Board.board[0][4] == " ") )
			return 9;
		else if((Board.board[0][0] == inputLetter) && (Board.board[4][0] == inputLetter) && (Board.board[0][4] == " ") )
			return 9;
		else if((Board.board[0][0] == inputLetter) && (Board.board[4][0] == inputLetter) && (Board.board[4][4] == " ") )
			return 3;
		else if((Board.board[0][0] == inputLetter) && (Board.board[0][4] == inputLetter) && (Board.board[4][4] == " ") )
			return 3;
		else if((Board.board[0][4] == inputLetter) && (Board.board[4][4] == inputLetter) && (Board.board[0][0] == " ") )
			return 7;
		else if((Board.board[4][0] == inputLetter) && (Board.board[4][4] == inputLetter) && (Board.board[0][0] == " ") )
			return 7;
		else if((Board.board[0][4] == inputLetter) && (Board.board[4][4] == inputLetter) && (Board.board[4][0] == " ") )
			return 1;
		else if((Board.board[0][0] == inputLetter) && (Board.board[0][4] == inputLetter) && (Board.board[4][0] == " ") )
			return 1;

		return 0;
		
	}

	//plays the blocking a fork method
	public int blockOpponentFork(String inputLetter)
	{
		if((Board.board[0][0] == inputLetter) && (Board.board[4][4] == inputLetter) && (Board.board[2][0] == " "))
			return 4;
		else if((Board.board[0][0] == inputLetter) && (Board.board[4][4] == inputLetter) && (Board.board[2][4] == " "))
			return 6;
		else if((Board.board[0][4] == inputLetter) && (Board.board[4][0] == inputLetter) && (Board.board[2][0] == " "))
			return 4;
		else if((Board.board[0][4] == inputLetter) && (Board.board[4][0] == inputLetter) && (Board.board[2][4] == " "))
			return 6;

		return 0;

	}

	//checks and plays the center
	public int center()
	{
		if(this.checkAvailable(5))
		{
		return 5;
		}
		else 
		return 0;
	}

	//checks and plays the opposite corner
	public int oppositeCorner(String inputLetter)
	{
		String[][] tempArray = Board.board;
		
		if( (tempArray[0][0] == inputLetter) && (tempArray[4][4] == " "))
			return 3;
		else if((tempArray[0][4] == inputLetter) && (tempArray[4][0] == " ") )
			return 1;
		else if((tempArray[4][0] == inputLetter) && (tempArray[0][4] == " "))
			return 9;
		else if((tempArray[4][4] == inputLetter) && (tempArray[0][0] == " "))
			return 7;
			
			return 0;
	}

	//checks and plays empty corner
	public int emptyCorner()
	{
		if(Board.board[0][0] == " ")
			return 7;
		else if(Board.board[0][4] == " ")
			return 9;
		else if(Board.board[4][0] == " ")
			return 1;
		else if (Board.board[4][4] == " ")
			return 3;
		return 0;
	}

	//checks and plays empty side
	public int emptySide()
	{
		if(Board.board[0][2] == " ")
			return 8;
		else if(Board.board[2][0] == " ")
			return 4;
		else if(Board.board[2][4] == " ")
			return 6;
		else if (Board.board[4][2] == " ")
			return 2;
		return 0;
	}
}