import java.util.Scanner;
/* 
* CSCI1301.java 
* Author: Kosi Uzodinma 
* Submission Date: March,07,2014 
* 
* Purpose: Writing a program to play the game Goomba.
* 
* Statement of Academic Honesty: 
* 
* The following code represents my own work. I have neither 
* received nor given inappropriate assistance. I have not copied 
* or modified code from any source other than the course webpage 
* or the course textbook. I recognize that any unauthorized  
* assistance or plagiarism will be handled in accordance with 
* the University of Georgia's Academic Honesty Policy and the 
* policies of this course. I recognize that my work is based 
* on an assignment created by the Department of Computer 
* Science at the University of Georgia. Any publishing 
* or posting of source code for this project is strictly 
* prohibited unless you have written consent from the Department 
* of Computer Science at the University of Georgia. 
*/

public class Goomba 
{
private static final boolean testingMode = true;
	
	public static void main(String[] args) 
	{
		//declaring variables for the program
		Scanner keyboardScanner = new Scanner(System.in);
		int guess = 15;
		int game = 0;
		String difficultyAnswer, letter, secretWord, playAgain, spaces, updatedWord, displayWord = "";
		Scanner keyboard = new Scanner(System.in);
		char letterGuess;
		
		//Creating loop to run Goomba 25 times
		do
		{
		//If the number of games is over 25 it will execute the program
		if (game > 25)
			{
				System.out.println("You played over 25 games");
				System.exit(0);
			}
		//Prompting the user to enter the difficulty
		System.out.println("Enter your difficulty: Easy (e), Intermediate (i), or Hard (h)");
		difficultyAnswer = keyboard.next();
		difficultyAnswer = difficultyAnswer.substring(0,1);
		//Make if statement to determine if they choose one of the three choices
		if ( difficultyAnswer.equalsIgnoreCase("e") || difficultyAnswer.equalsIgnoreCase("i") || difficultyAnswer.equalsIgnoreCase("h") )
		{				
			//Decisions statements for easy mode
			if(difficultyAnswer.equalsIgnoreCase("e"))
			{
				//Reset display word so it would to dashes
				displayWord = "";
				// Setting the number guesses the user is allowed
				guess = 15;
				secretWord = RandomWord.newWord(); // declared it again to generate a new secret word
				//As long as testingMode is true it will print out the secret word
				
				if (testingMode)
				{
					System.out.println();
					System.out.println("The secret word is: " + secretWord);
					System.out.println();
				}
				
				//loop that makes secret word into a display word of dashes
				while( (displayWord.length()<secretWord.length()) )
				{
				displayWord = displayWord + "-";
				}
				
				//Displaying the display word to the user
				System.out.println("The word is: " + displayWord);
				System.out.println();
				
				//Creating the loop that lets the user enter another guess and space to check until the word matches the secret word or guesses run out
				do 
				{
				//Prompting the user to enter the letter he wants
				System.out.print("Please enter the letter you want to guess: ");
				letter = keyboard.next();
				//turned the string entered into the first char entered
				letterGuess = letter.charAt(0);
				System.out.println();
				//prompting the user to enter the spaces they want to look for the letter they entered
				System.out.println("Please enter the spaces you want to check (separated by spaces):");
				System.out.println();
				//created extra nextline so it would take the space of the next
				keyboard.nextLine();
				//variable for user to enter the spaces they want to guess for
				spaces = keyboard.nextLine();
				//Created four variables to keep the charAt of 0,2,4, and 6 of the string the user entered for the variable spaces
				int index1 =0 ,index2 =0 ,index3=0 ,index4 =0;
				//matched updated word to display word to help with decision statement later in the program
				updatedWord = displayWord;
				
				//make four if statements to determine if the user actually entered four numbers
				if (spaces.length() >= 1)
				{
				index1 = Character.getNumericValue(spaces.charAt(0));
				}
				if (spaces.length() >= 3)
				{
				index2 = Character.getNumericValue(spaces.charAt(2));
				}
				if (spaces.length() >= 5)
				{
				index3 = Character.getNumericValue(spaces.charAt(4));
				}
				if (spaces.length() >= 7)
				{
				index4 = Character.getNumericValue(spaces.charAt(6));
				}
				
				// Decision statement to determine if the user was following instructions 
				//To fix index make an and statment changing all the charAt spaces saying greating then the left of 7 or whatever the difficulty is
				if ( (Character.isLetter(letterGuess) == false) || (spaces.length() > 7) || (spaces.length() != 7) || (Character.isDigit(spaces.charAt(0)) == false) ||  
					(Character.isDigit(spaces.charAt(2)) == false) || (Character.isDigit(spaces.charAt(4)) == false) || (Character.isDigit(spaces.charAt(6)) == false)
					|| (index1>=secretWord.length()) || (index2>=secretWord.length()) || (index3>=secretWord.length()) || (index4>=secretWord.length()))
						{
							System.out.println();
							System.out.println("Your input is not valid. Try again.");
							System.out.println();
							System.out.println("Guesses Remaining: " + guess);
							System.out.println();
							continue;
						}
				
				// Made four if statements to determine if the number they guessed contained the letter they chose
				if (secretWord.charAt(index1) == letterGuess)
						{
							displayWord = displayWord.substring(0,index1) + letterGuess + displayWord.substring(index1 +1);
						}
				if (secretWord.charAt(index2) == letterGuess)
				{
					displayWord = displayWord.substring(0,index2) + letterGuess + displayWord.substring(index2 +1);
				}
				if (secretWord.charAt(index3) == letterGuess)
				{
					displayWord = displayWord.substring(0,index3) + letterGuess + displayWord.substring(index3 +1);
				}	
				if (secretWord.charAt(index4) == letterGuess)
				{
					displayWord = displayWord.substring(0,index4) + letterGuess + displayWord.substring(index4 +1);
				}
				
				//decision statement that determined the user did not choose the right spaces
				if ( (secretWord.charAt(index4) != letterGuess) && (secretWord.charAt(index3) != letterGuess) && (secretWord.charAt(index2) != letterGuess) && (secretWord.charAt(index1) != letterGuess) )
				{
					System.out.println();
					System.out.println("Your letter was not found in the spaces you provided");
					System.out.println();
					guess --;
					System.out.println("Guesses Remaining: " + guess);
					System.out.println();
				}
				
				//Decision statement to determine if the user chose correctly
				if ((updatedWord == displayWord) && ((secretWord.charAt(index4) == letterGuess) && 
					(secretWord.charAt(index3) == letterGuess) && (secretWord.charAt(index2) == letterGuess) && (secretWord.charAt(index1) == letterGuess)) )
				{
					System.out.println();
					System.out.println("Your guess is in the word" );
					System.out.println();
					System.out.println("The updated word is: " + displayWord);
					System.out.println();
					System.out.println("Guesses Remaining: " + guess);
					System.out.println();
				}
				
				 
				//Decision statement to determine if the user chose correctly
				if (updatedWord != displayWord)
				{
				System.out.println();
				System.out.println("Your guess is in the word!");
				System.out.println("");
				System.out.println("The updated word is: " + displayWord);
				System.out.println();
				System.out.println("Gueseses Remaining: " + guess);
				System.out.println();
				}
				
				//Decision statement that tells the user if they won and if they want to play again
				if ( displayWord.equalsIgnoreCase(secretWord) )
				{
					System.out.println("You have guessed the word! Congratulations");
					System.out.println("Would you like to play again? Yes(y) or No(n)");
					playAgain = keyboard.next();
					playAgain = playAgain.substring(0,1);
					if (playAgain.equalsIgnoreCase ("y"))
					{
						System.out.println();
						game ++;
					}
					if (playAgain.equalsIgnoreCase("n"))
					{
						System.out.println();
						System.out.println("Thanks for playing");
						System.exit(0);
					}
				}
				
				//Decision statement to tell the user they lost and ask them if they want to play again
				if (guess <=0 )
					{
						System.out.println("You have failed to guess the word.. :(");
						System.out.println();
						System.out.println("Would you like to play again? Yes(y) or No(n)");
						System.out.println();
						playAgain = keyboard.next();
						playAgain = playAgain.substring(0,1);
						if (playAgain.equalsIgnoreCase ("y"))
						{
							System.out.println();
							game ++;
						}
						if (playAgain.equalsIgnoreCase("n"))
						{
							System.out.println();
							System.out.println("Thanks for playing");
							System.exit(0);
						}
						
					}
					
				
					// loop statement that repeats until the number of guesses < 0 or display word matches the updated word
				}
					while ( (guess > 0) && ( (displayWord.equalsIgnoreCase(secretWord) == false)) );
			}
			
			// if statement that set the rules for intermediate difficulty 
			if(difficultyAnswer.equalsIgnoreCase("i"))
			{
				//Reseting display word
				displayWord = "";
				guess = 12;
				secretWord = RandomWord.newWord();
				//As long as testingMode is true it will print out the secret word
				if (testingMode == true)
				{
					System.out.println();
					System.out.println("The secret word is: " + secretWord);
					System.out.println();
				}
				//loop to create display word
				while( (displayWord.length()<secretWord.length()) )
				{
				displayWord = displayWord + "-";
				}
				
				System.out.println("The word is: " + displayWord);
				System.out.println();
				//loop for user to keep guessing until guesses run or display word equals secret word
				do 
				{
				System.out.print("Please enter the letter you want to guess: ");
				letter = keyboard.next();
				letterGuess = letter.charAt(0);
				System.out.println();
				System.out.println("Please enter the spaces you want to check (separated by spaces):");
				System.out.println();
				keyboard.nextLine();
				spaces = keyboard.nextLine();
				int index1 =0 ,index2 =0 ,index3=0 ,index4 =0;
				updatedWord = displayWord;
				
				// if statements so the only correct input is three numbers with spaces
				if (spaces.length() > 1)
				{
				index1 = Character.getNumericValue(spaces.charAt(0));
				}
				if (spaces.length() > 3)
				{
				index2 = Character.getNumericValue(spaces.charAt(2));
				}
				if (spaces.length() > 4)
				{
				index3 = Character.getNumericValue(spaces.charAt(4));
				}
				
				//if statements to handle improper instructions by the user
				if ( (Character.isLetter(letterGuess) == false) || (spaces.length() >5) || (spaces.length() != 5) || (Character.isDigit(spaces.charAt(0)) == false) ||  
					(Character.isDigit(spaces.charAt(2)) == false) || (Character.isDigit(spaces.charAt(4)) == false) || (index1 >=secretWord.length())|| (index2 >=secretWord.length())
					|| (index3>=secretWord.length()))
						{
							System.out.println();
							System.out.println("Your input is not valid. Try again.");
							System.out.println();
							System.out.println("Guesses Remaining: " + guess);
							System.out.println();
							continue;
						}
				
				//If statements to match correct space guesses with correct letter guess
				if (secretWord.charAt(index1) == letterGuess)
						{
							displayWord = displayWord.substring(0,index1) + letterGuess + displayWord.substring(index1 +1);
						}
				if (secretWord.charAt(index2) == letterGuess)
				{
					displayWord = displayWord.substring(0,index2) + letterGuess + displayWord.substring(index2 +1);
				}
				if (secretWord.charAt(index3) == letterGuess)
				{
					displayWord = displayWord.substring(0,index3) + letterGuess + displayWord.substring(index3 +1);
				}	
				
				//If statement for when the guesses are incorrect
				if ( (secretWord.charAt(index3) != letterGuess) && (secretWord.charAt(index2) != letterGuess) && (secretWord.charAt(index1) != letterGuess) )
				{
					System.out.println();
					System.out.println("Your letter was not found in the spaces you provided");
					System.out.println();
					guess --;
					System.out.println("Guesses Remaining: " + guess);
					System.out.println();
				}
				
				//if statement for when gusses are correct
				if ((updatedWord == displayWord) && ((secretWord.charAt(index3) == letterGuess) && (secretWord.charAt(index2) == letterGuess) && 
						(secretWord.charAt(index1) == letterGuess)) )
				{
					System.out.println();
					System.out.println("Your guess is in the word" );
					System.out.println();
					System.out.println("The updated word is: " + displayWord);
					System.out.println();
					System.out.println("Guesses Remaining: " + guess);
					System.out.println();
				}
				
				//If statements for correct gusses
				if (updatedWord != displayWord)
				{
				System.out.println();
				System.out.println("Your guess is in the word!");
				System.out.println("");
				System.out.println("The updated word is: " + displayWord);
				System.out.println();
				System.out.println("Gueseses Remaining: " + guess);
				System.out.println();
				}
				
				//If statement for when the user wins the game and instruction to do after
				if ( displayWord.equalsIgnoreCase(secretWord) )
				{
					System.out.println();
					System.out.println("You have guessed the word! Congratulations");
					System.out.println("Would you like to play again? Yes(y) or No(n)");
					playAgain = keyboard.next();
					playAgain = playAgain.substring(0,1);
					if (playAgain.equalsIgnoreCase ("y"))
					{
						System.out.println();
						game ++;
					}
					if (playAgain.equalsIgnoreCase("n"))
					{
						System.out.println();
						System.out.println("Thanks for playing");
						System.exit(0);
					}
					
				}
				
				//If statement when the user's gusses runs out
				if (guess <=0 )
					{
						System.out.println("You have failed to guess the word.. :(");
						System.out.println();
						System.out.println("Would you like to play again? Yes(y) or No(n)");
						System.out.println();
						playAgain = keyboard.next();
						playAgain = playAgain.substring(0,1);
						if (playAgain.equalsIgnoreCase ("y"))
						{
							System.out.println();
							game ++;
						}
						if (playAgain.equalsIgnoreCase("n"))
						{
							System.out.println();
							System.out.println("Thanks for playing");
							System.exit(0);
						}
						
					
				}
					//If statement to end loop of guessing for the display word
				}
					while ( (guess > 0) && ( (displayWord.equalsIgnoreCase(secretWord) == false)) );
			}
			
			//If statement setting the rules of hard mode
			if(difficultyAnswer.equalsIgnoreCase("h"))
			{
				displayWord = "";
				guess = 10;
				secretWord = RandomWord.newWord();
				//As long as testingMode is true it will print out the secret word
				if (testingMode)
				{
					System.out.println();
					System.out.println("The secret word is: " + secretWord);
					System.out.println();
				}
				
				//loop for display word
				while( (displayWord.length()<secretWord.length()) )
				{
				displayWord = displayWord + "-";
				}
				
				System.out.println("The word is: " + displayWord);
				System.out.println();
				
				//loop to allow user to keep guessing for word
				do 
				{
				System.out.print("Please enter the letter you want to guess: ");
				letter = keyboard.next();
				letterGuess = letter.charAt(0);
				System.out.println();
				System.out.println("Please enter the spaces you want to check (separated by spaces):");
				System.out.println();
				keyboard.nextLine();
				spaces = keyboard.nextLine();
				int index1 =0 ,index2 =0;
				updatedWord = displayWord;
				//if statements to make sure the user enters 2 numbers as valid input
				if (spaces.length() > 1)
				{
				index1 = Character.getNumericValue(spaces.charAt(0));
				}
				if (spaces.length() > 2)
				{
				index2 = Character.getNumericValue(spaces.charAt(2));
				}
				
				//if statement to ensure proper syntax by the user
				if ( (Character.isLetter(letterGuess) == false) || (index1 >= secretWord.length()) || (index2 >= secretWord.length()) || (spaces.length() >3) || (spaces.length() < 3) || (Character.isDigit(spaces.charAt(0)) == false) ||  
					(Character.isDigit(spaces.charAt(2)) == false))
						{
							System.out.println();
							System.out.println("Your input is not valid. Try again.");
							System.out.println();
							System.out.println("Guesses Remaining: " + guess);
							System.out.println();
							continue;
						}
				
				//If statements to check spaces entered by the user.
				if (secretWord.charAt(index1) == letterGuess)
						{
							displayWord = displayWord.substring(0,index1) + letterGuess + displayWord.substring(index1 +1);
						}
				if (secretWord.charAt(index2) == letterGuess)
				{
					displayWord = displayWord.substring(0,index2) + letterGuess + displayWord.substring(index2 +1);
				}
				
				if ( (secretWord.charAt(index1) != letterGuess) && (secretWord.charAt(index2) != letterGuess) )
				{
					System.out.println();
					System.out.println("Your letter was not found in the spaces you provided");
					System.out.println();
					guess --;
					System.out.println("Guesses Remaining: " + guess);
					System.out.println();
				}
				//If statement for when the user does guess correctly
				if ((updatedWord == displayWord) && ((secretWord.charAt(index2) == letterGuess) && 
						(secretWord.charAt(index1) == letterGuess)) )
				{
					System.out.println();
					System.out.println("Your guess is in the word" );
					System.out.println();
					System.out.println("The updated word is: " + displayWord);
					System.out.println();
					System.out.println("Guesses Remaining: " + guess);
					System.out.println();
				}
				
				//if statement for when the user guesses correctly
				if (updatedWord != displayWord)
				{
				System.out.println();
				System.out.println("Your guess is in the word!");
				System.out.println("");
				System.out.println("The updated word is: " + displayWord);
				System.out.println();
				System.out.println("Gueseses Remaining: " + guess);
				System.out.println();
				}
				
				//If statement when the user wins the game
				if ( displayWord.equalsIgnoreCase(secretWord) )
				{
					System.out.println();
					System.out.println("You have guessed the word! Congratulations");
					System.out.println("Would you like to play again? Yes(y) or No(n)");
					playAgain = keyboard.next();
					playAgain = playAgain.substring(0,1);
					if (playAgain.equalsIgnoreCase ("y"))
					{
						System.out.println();
						game ++;
					}
					if (playAgain.equalsIgnoreCase("n"))
					{
						System.out.println();
						System.out.println("Thanks for playing");
						System.exit(0);
					}
					
				}
				//If statement for when the user loses the game
				if (guess <=0 )
				{
					System.out.println();
					System.out.println("You have failed to guess the word.. :(");
					System.out.println();
					System.out.println("Would you like to play again? Yes(y) or No(n)");
					System.out.println();
					playAgain = keyboard.next();
					playAgain = playAgain.substring(0,1);
					if (playAgain.equalsIgnoreCase ("y"))
					{
						System.out.println();
						game ++;
					}
					if (playAgain.equalsIgnoreCase("n"))
					{
						System.out.println();
						System.out.println("Thanks for playing");
						System.exit(0);
					}
					
				}

				}
					while ( (guess > 0) && ( (displayWord.equalsIgnoreCase(secretWord) == false)) );
			}
}
	//else statement for when the user enters the wrong difficulty
	else
	{
		System.out.println();
		System.out.println("Invalid difficulty. Try Again...... ");
		System.out.println("");
	}
		

}

	//loop variable controlling how long the variable will run
	while (game < 25);
}
}
