#ifndef LINKED_LIST_H
#define LINKED_LIST_H
#include <iostream>
using std::cout;
using std::endl;
using std::string;
// forward declation of LinkedList
template<typename T>
class LinkedList;
/**
 * Represents a node in a doubly-linked list.
 */
template<typename T>
class Node {
    friend class LinkedList<T>;
    T data;
    Node<T> *prev;
    Node<T> *next;
    Node()
    {
        prev = nullptr;
        next = nullptr;
    }
}; // Node
/**
 * A doubly-linked list data structure.
 */
template<typename T>
class LinkedList {
public:
    Node<T> *head;
    Node<T> *tail;

    
    LinkedList()
    {
        head = new Node<T>();
        tail = new Node<T>();
        tail->prev = head;
        tail ->next = nullptr;
        head->next = tail;
        head->prev = nullptr;
    }
    
    /**
     * Adds an item to the end of this list.
     *
     * @param data the item to append
     */
    LinkedList<T>* copy() const 
    {
        LinkedList<T> *temp = new LinkedList<T>();
        int counter = 0;
        while(counter < size())
        {
            temp->append(get(counter));
            counter ++;
        }
        return temp;
    }
    void append(T data) 
    {
        if(size() == 0)
        {
            Node<T> *temp = new Node<T>;
            temp->data = data;
            head->next = temp;
            temp->prev = head;
            temp->next = tail;
            tail->prev = temp;
        }
        else
            insert(size(),data);
    }
    
    /**
     * Removes all elements from this list.
     */
    void clear(void)
    {
        int counter = 0;
        head = head->next;
        Node<T> *del = head;
        while(counter<size())
        {
            head = head->next;
        	delete del;
        	del = head;
        }

        
    }
    /**
     * Returns the item at the given index location.
     *
     * @param i the index of the item to return
     */
    T get(int i) const
    {
        if(empty())
            return -1;
        int x = 0;
        Node<T> *temp = head;
        while(x<=i)
        {
            temp = temp->next;
            x++;
        }
        return temp->data;
    }
    /**
     * Adds an item to the list at the specified index location.
     *
     * @param i the index where to insert the item
     * @param data the item to insert
     */
    void insert(int i, T data) 
    {
        Node<T> *temp = head->next;
        Node<T> *temp2 = new Node<T>;
        temp2->data = data;
        int x = 0;
        while(x<i)
        {
            temp = temp->next;
            x++;
        }
        temp->prev->next = temp2;
        temp2->prev = temp->prev;
        temp2->next = temp;
        temp->prev = temp2;
        
    }
    /**
     * Adds an item to the beginning of this list.
     *
     * @param data the item to prepend
     */
    void prepend(T data)
    {
        if(size() == 0)
        {
            Node<T> *temp = new Node<T>;
            temp->data = data;
            head->next = temp;
            temp->prev = head;
            temp->next = tail;
            tail->prev = temp;
        }
        else
            insert(0,data);
    }
    /**
     * Sets the value of element at the given index.
     *
     * @param i the index of the element to set
     * @param data the value to set
     */
    void set(int i, T data)
    {
        int x = 0;
        Node<T> *temp = head;
        while(x<=i)
        {
            temp = temp->next;
            x++;
        }
        temp->data = data;
    }
    /**
     * Returns the number of elements in this list.
     */
    const int size() const
    {
        int counter =0;
        Node<T> *temp = head->next;
        while(temp->next !=nullptr)
        {
            counter++;
            temp = temp->next; 
        }
        return counter;
    }
    
    /**
     * Returns whether or not this linked list is empty. */
    const bool empty() const {
        return size() == 0;
    } // empty
    
    T delete_pop(){
        Node<T> *temp = head->next;
        T pop_value = temp->data;
        head->next = temp->next;
        delete temp;
        return pop_value;
    }
    
    
}; // LinkedList
#endif /** LINKED_LIST_H */

