#ifndef ARRAY_QUEUE_H
#define ARRAY_QUEUE_H
#include "Queue.h"
/**
* An array-based queue implementation.
*/
template<typename T>
class ArrayQueue : public Queue<T> {
// instance/state variables
public:
	int maxSize = 10;
	int *queueStack = new T[maxSize];
	int head=0;
	int tail=0;
	int counter=0;

// Default Constructor
ArrayQueue(void) = default;

// Copy Constructor
ArrayQueue(const ArrayQueue<T> & other) {
// TODO implement a copy constructor
    delete[] queueStack;
    queueStack = new T[other.maxSize];
    for(int i =0; i<other.maxSize; i++)
    {
     queueStack[i] = other.queueStack[i];   
    }
    maxSize = other.maxSize;
    head = other.head;
    tail = other.tail;
    counter = other.counter;
} // ArrayQueue

// Destructor
virtual ~ArrayQueue(void) {
// TODO implement the destructor
	delete[] queueStack;
} // ~ArrayQueue

void enqueue(T data){
if(isFull())
	increaseArray();
queueStack[tail] = data;
tail++;
counter++;
}

T dequeue(void){
if(size()==0)
	return -1;
else
{
T value = queueStack[head];
head++;
counter--;
return value;
}

}
T peek(void) const{
if(size()==0)
	return -1;
else
	return queueStack[head];
}
const int size(void) const{
return counter;
}
bool empty() const{
	return size() == 0;

}
void increaseArray()
{
    T *newArray = new T [maxSize+1];
    for(int i =0; i<maxSize; i++)
    {
        newArray[i] = queueStack[i];
    }
    delete[] queueStack;
    queueStack = newArray;
    maxSize++;
}
bool isFull()
{
    return tail == maxSize;
}
// TODO implement other functions
}; // ArrayQueue
#endif /* ARRAY_QUEUE_H */

