#ifndef LINKED_STACK_H
#define LINKED_STACK_H
#include "Stack.h"
#include <list>
#include "LinkedList.h"
/**
* A stack implementation based on a doubly linked-list.
*/
template<typename T>
class LinkedStack : public Stack<T> {
// instance/state variables
public:
	LinkedList<T> *linkedStack;
	
// Default Constructor
LinkedStack(void)
{
	linkedStack = new LinkedList<T>;
}
// Copy Constructor
LinkedStack(const LinkedStack<T> & other) {
	linkedStack = other.linkedStack->copy();
		

// TODO implement a copy constructor
} // LinkedStack
  // Destructor
  ~LinkedStack(void) {
  	linkedStack->clear();
// TODO implement the destructor
} // ˜LinkedStack
void push(T data){
	linkedStack->prepend(data);
}
T pop(void){
	if(empty())
		return -1;
	else
		return linkedStack->delete_pop();
}
T peek(void) const{
	if(empty())
		return -1;
	else 
		return linkedStack->get(0);
}
const int size(void) const{
	return linkedStack->size();
}
const bool empty() const {
    return linkedStack->size() == 0;
  } 

// TODO implement other functions
}; // LinkedStack
#endif /* LINKED_STACK_H */

