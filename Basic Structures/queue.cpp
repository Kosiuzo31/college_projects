#include <iostream>
#include <cstdlib>
#include "ArrayQueue.h"
#include "LinkedQueue.h"
using std::cout;
using std::endl;
using std::string;
int main(int argc, const char * argv[]) {
cout << "Testing LinkedQueue Implementations..." << endl;
Queue<int> *q1_L = new LinkedQueue<int>();

cout << "Sizeq1_L: " << q1_L->size() << endl;
cout << "emptyq1_L: " << q1_L->empty() <<endl;
cout << q1_L->dequeue() <<endl; //Testing if the Queue is empty and returns -1
for(int i=0; i<2; i++)
{
	q1_L->enqueue(673+i);
}

cout << "Sizeq1_L: " << q1_L->size() << endl;
cout << "Peekq1_L: " << q1_L->peek() << endl;
for(int i=0; i<2; i++)
{
	cout << "q1_L value" << i << ":"<< q1_L->dequeue() <<endl;
}
q1_L->enqueue(34);
q1_L->enqueue(48);
q1_L->dequeue();
cout << "Sizeq1_L: " << q1_L->size() << endl;
cout << "Peekq1_L: " << q1_L->peek() << endl;

delete q1_L;


//Testing copyContructor for ArrayQueue
ArrayQueue<int> *q1_C = new ArrayQueue<int>();
q1_C->enqueue(99); q1_C->enqueue(100);
ArrayQueue<int> q2_C = *q1_C;
q2_C.enqueue(50);
q2_C.dequeue();
q1_C->enqueue(45);
cout << "Sizeq1_C: " << q1_C->size() << endl;
cout << "Sizeq2_C: " << q2_C.size() << endl;
cout << "peeking in original list: " << q1_C->peek() << endl;
cout << "peeking in copy list: " << q2_C.peek() << endl;
delete q1_C;

//Testing copyContructor for LinkedQueue
LinkedQueue<int> *q1_CL = new LinkedQueue<int>();
q1_CL->enqueue(99); q1_CL->enqueue(100);
LinkedQueue<int> q2_CL = *q1_CL;
q2_CL.enqueue(50);
q2_CL.dequeue();
q1_CL->enqueue(45);
cout << "Sizeq1_CL: " << q1_CL->size() << endl;
cout << "Sizeq2_CL: " << q2_CL.size() << endl;
cout << "peeking in original list: " << q1_CL->peek() << endl;
cout << "peeking in copy list: " << q2_CL.peek() << endl;
delete q1_CL;

cout << "Testing ArrayQueue Implementations..." << endl;
Queue<int> *q1 = new ArrayQueue<int>();
cout << "Sizeq1: " << q1->size() << endl;
cout << "emptyq1: " << q1->empty() <<endl;
cout << q1->dequeue() <<endl; //Testing if the Queue is empty and returns -1
for(int i=0; i<2; i++)
{
	q1->enqueue(673+i);
}
cout << "Sizeq1: " << q1->size() << endl;
cout << "Peekq1: " << q1->peek() << endl;
for(int i=0; i<2; i++)
{
	cout << "q1 value" << i << ":"<< q1->dequeue() <<endl;
}
cout << "q1 value:"<< q1->dequeue() <<endl;
for(int i=0; i<2; i++)
{
	q1->enqueue(675+i);
}
for(int i=0; i<1; i++)
{
	cout << "q1 value" << i << ":"<< q1->dequeue() <<endl;
}
cout << "Sizeq1: " << q1->size() << endl;
cout << "Peekq1: " << q1->peek() << endl; 
delete q1;

return EXIT_SUCCESS;
} // main
