#include <iostream>
#include <cstdlib>
#include "ArrayStack.h"
#include "LinkedStack.h"
#include "LinkedList.h"
using std::cout;
using std::endl;
using std::string;
int main(int argc, const char * argv[]) {
// LINKED-LINKED LIST TEST CASES
cout << "Testing LinkedList Implementations..." << endl;
LinkedList<int> *ll = new LinkedList<int>();
cout << "size: " << ll->size() << endl;
cout << "value0: " << ll->get(0) << endl; //Checking if it return 
for(int i =0; i<3; i++)
	ll->append(45+i);
for(int i =0; i<3; i++)
	ll->prepend(423+i);
for(int i = 0; i<6; i++)
	cout << "value" << i << ":"<< ll->get(i) << endl;
ll->clear();
cout << "size: " << ll->size() << endl;
ll->append(1);
cout << "value0: " << ll->get(0) << endl;
ll->set(0,600);
cout << "value0afterset: " << ll->get(0) << endl;
ll->prepend(567);
cout << "value0afterprepend: " << ll->get(0) << endl;
ll->insert(0,500);
cout << "size: " << ll->size() << endl;
cout << "value0: " << ll->get(0) << endl;
cout << "value1: " << ll->get(1) << endl;
cout << "value2: " << ll->get(2) << endl;
LinkedList<int> *ll2 = ll->copy(); //Testing copy method
ll->clear();
cout << "size: " << ll2->size() << endl;
cout << "value0: " << ll2->get(0) << endl;
cout << "value1: " << ll2->get(1) << endl;
cout << "value2: " << ll2->get(2) << endl;


delete ll;

cout << "Testing LinkedStack Implementations..." << endl;
Stack<int> *s1_L = new LinkedStack<int>();
cout << "SizeS1_L: " << s1_L->size() << endl;
cout << "emptyS1_L: " << s1_L->empty() <<endl;
cout << "S1 value:"<< s1_L->pop() <<endl; //Testing if the stack is empty and returns -1
for(int i=0; i<2; i++)
{
	s1_L->push(673+i);
}
cout << "SizeS1_L: " << s1_L->size() << endl;
cout << "PeekS1_L: " << s1_L->peek() << endl;
for(int i=0; i<1; i++)
{
cout << "S1 value" << i << ":"<< s1_L->pop() <<endl;
}
cout << "SizeS1_L: " << s1_L->size() << endl;
cout << "PeekS1_L: " << s1_L->peek() << endl;

delete s1_L;



cout << "Testing ArrayStack Implementations..." << endl;
Stack<int> *s1 = new ArrayStack<int>();
cout << "SizeS1: " << s1->size() << endl;
cout << "empty: " << s1->empty() <<endl;
cout << s1->pop() <<endl; //Testing if the stack is empty and returns -1
for(int i=0; i<2; i++)
{
	s1->push(673+i);
}
cout << "SizeS1: " << s1->size() << endl;
cout << "Peek: " << s1->peek() << endl;
for(int i=0; i<2; i++)
{
	cout << s1->pop() <<endl;
}
cout << "SizeS1: " << s1->size() << endl;
cout << "PeekS1: " << s1 ->peek() << endl;
delete s1;

cout <<"TESTING copy for ArrayStack" << endl;
//Testing copyContructor
ArrayStack<int> * s1_C = new ArrayStack<int>();
s1_C->push(99); s1_C->push(100);
ArrayStack<int> s2_C = *s1_C;
s2_C.pop();
s1_C->push(45);
cout << "peeking in original list: " << s1_C->peek() << endl;
cout << "peeking in copy list: " << s2_C.peek() << endl;
delete s1_C;

cout <<"TESTING copy for LinkedStack" << endl;
//Testing copyContructor
LinkedStack<int> * s1_CL = new LinkedStack<int>();
s1_CL->push(99); s1_CL->push(100);
LinkedStack<int> s2_CL = *s1_CL;
s2_CL.pop();
s1_CL->push(45);
cout << "SIZEs1_CL:" << s1_CL->size() <<endl;
cout << "SIZEs2_CL:" << s2_CL.size() <<endl;
cout << "peeking in original list: " << s1_CL->peek() << endl;
cout << "peeking in copy list: " << s2_CL.peek() << endl;
delete s1_CL;




return EXIT_SUCCESS;
} // main
