#ifndef LINKED_QUEUE_H
#define LINKED_QUEUE_H
#include "Queue.h"
#include "LinkedList.h"
#include <list>
/**
* An queue implementation based on a doubly linked list.
*/
template<typename T>
class LinkedQueue : public Queue<T> {
// instance/state variables
public:
	LinkedList<T> *linkedQueue;

// Default Constructor
LinkedQueue(void)
{
	linkedQueue = new LinkedList<T>;
}

// Copy Constructor
LinkedQueue(const LinkedQueue<T> & other) {
// TODO implement a copy constructor
	linkedQueue = other.linkedQueue->copy();
} // LinkedQueue
// Destructor
virtual ~LinkedQueue(void) {
	linkedQueue->clear();
// TODO implement the destructor
} // ~LinkedQueue

// TODO implement other functions
void enqueue(T data){
linkedQueue->append(data);
}
T dequeue(void){
	if(empty())
		return -1;
	else
		return linkedQueue->delete_pop();
}
T peek(void) const{
	if(empty())
		return -1;
	else
		return linkedQueue->get(0);
}
const int size(void) const{
return linkedQueue->size();
}
bool empty() const{
	return size() == 0;
}

}; // LinkedQueue
#endif /* LINKED_QUEUE_H */