#ifndef ARRAY_STACK_H
#define ARRAY_STACK_H
#include "Stack.h"
#include <iostream>
#include <stdio.h>

using namespace std;
/**
 * An array-based stack implementation.
 */
template<typename T>
class ArrayStack : public Stack<T> {
    // instance/state variables

public:
    int maxSize = 10;
    int top = 0;
    T *arrayStack = new T[maxSize]; 
    
    // Default Constructor
    ArrayStack() = default;
    
    // Copy Constructor
    ArrayStack(const ArrayStack<T> & other) {
        //TODO implement a copy constructor
        delete [] arrayStack;
        arrayStack = new T[other.maxSize];
        for(int i =0; i<other.maxSize; i++)
        {
         arrayStack[i] = other.arrayStack[i];   
        }
        top = other.top;
        maxSize = other.maxSize;
        }
    // Destructor
    ~ArrayStack(void) {// TODO implement the destructor
        delete[] arrayStack;
    } // ~ArrayStack
    // TODO implement other functions
    
    void push(T data)
    {
        if(isFull())
        {
            increaseArray();
        }
        arrayStack[top] = data;
        top++;
    }
    T pop(void)
    {
        if(empty())
            return -1;
        else
        {
            T value = arrayStack[top-1];
            top--;
            return value;}
        
    }
    const bool empty() const
    {
        return top == 0;
    }
    bool isFull()
    {
        
        return top == maxSize;
    }
    void increaseArray()
    {
        T *newArray = new T [maxSize+1];
        for(int i =0; i<maxSize; i++)
        {
            newArray[i] = arrayStack[i];
        }
        delete[] arrayStack;
        arrayStack = newArray;
        maxSize++;
    }
    T peek(void) const
    {
        if(empty())
            return -1;
        
        return arrayStack[top-1];
    }
    
    const int size(void) const
    {
        if(!empty())
            return top;
        else 
            return 0;
    }   
    
}; // ArrayStack


#endif /* ARRAY_STACK_H */


