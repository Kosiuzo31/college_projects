import java.util.Arrays;
import java.util.Scanner;
public class Bagels 
{
	public static void main(String[] args)
	{
		//Created string answer to use as a controlling expression of the loop for my program
		String answer = "";
		//A loop that will continually run my program for reset
		do
		{
		Scanner keyboard = new Scanner(System.in);
		//Instantiations of the classes made
		Player newPlayer = new Player();
		Engine engineGame = new Engine();
		Validator validatorGame = new Validator();
		//boolean expression for loop controlling play again 
		boolean controlPlay =true;
		//Variable to keep number of moves
		int gameMoves=0;
		//Prompting the user to play the game
		if (answer =="")
		{
		System.out.println("Welcome!");
		}
		System.out.print("Enter the number of digits to use: ");
		int digitsToUse = keyboard.nextInt();
		engineGame.setNumDigits(digitsToUse);
		System.out.print("Enter the player's name: ");
		String name = keyboard.next();
		newPlayer.setName(name);
		System.out.println();
		//Generating new secret number for the game
		engineGame.generateNewSecret();
		//Loop for play again option
		while(controlPlay == true)
		{
		//Number of Games the user has played
		System.out.println("Starting game # " + (newPlayer.getGamesCompleted()+1));
		//Controlling boolean expression for repeated guesses
		boolean controlGuess = true;
		//loop expression for guesses
		while(controlGuess==true)
		{
		//Receiving guess to change if it matches
		System.out.print("Enter your guess: ");
		//Made it into a string of the guess and put it in a array
		String playerGuess = newPlayer.askForGuess();
		int [] convertedString = new int[digitsToUse];
		convertedString = engineGame.convertNumToDigitArray(playerGuess);
		//Validating guess with secret number
		validatorGame.validateGuess(engineGame.getSecretNumber(), convertedString , engineGame.getNumDigits());
		//Increment game moves
		gameMoves++;
		//Decision statement for when guess matches secret number
		if (Arrays.equals(convertedString, engineGame.getSecretNumber()))
		{
			//Turning controlling expression off to stop loop for guesses 
			controlGuess =false;
			//Information after the user wins and giving the user their stats
			System.out.println("Congratulations! You won in " + gameMoves + " moves");
			newPlayer.updateStats(gameMoves);
			System.out.println();
			System.out.println("Statistics for " + newPlayer.getName() + ":");
			System.out.println("Game completed: " + newPlayer.getGamesCompleted());
			System.out.println("Number of digits: " + digitsToUse);
			System.out.println("Fastest win: " + newPlayer.getFastestWin() + " guesses");
			//Asking the user if they want to play again
			System.out.println("p - Play again");
			System.out.println("r - Reset game");
			System.out.println("q - Quit");
			System.out.println("");
			System.out.print("What would you like to do? ");
			answer = keyboard.next();
			System.out.println();
			//Decision statement for quit
			if ( answer.equalsIgnoreCase("q") )
			{
				System.out.println();
				System.out.println("GoodBye!");
				System.exit(0);
			}
			//Decision statement for play again
			if (answer.equalsIgnoreCase("p") )
			{
				gameMoves = 0;
				engineGame.generateNewSecret();
				break;
			}
			//Decision statement for repeat
			if (answer.equalsIgnoreCase("r"))
			{
				//Turning off controlling expression to play again
				controlPlay =false;
				//Breaking out of the loop to start the whole game over again
				break;
			}
			
		}
		
		}
		
		}
		
		//Controlling expression for the whole program to reset.
		}
		while(answer.equals("r"));
		
		
	
	}
}		
	
