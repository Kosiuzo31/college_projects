import java.util.Random;


public class Engine 
{
	//Data members for the Engine Class
	private int numDigits;
	private int[] secretNumber; 
	private Random randomNumberGenerator = new Random();
	//method to convert a string to an array
	public int[] convertNumToDigitArray (String number)
	{
		int [] numberArray = new int [number.length()];
		for (int i = 0; i < numberArray.length; i++)
		{
		numberArray [i] = number.charAt(i)-48;
		}
		
		return numberArray;
	}
	//Getter method for numDigits
	public int getNumDigits()
	{
		return this.numDigits;
	}
	//Getter method for the secret number
	public int[] getSecretNumber()
	{
		return this.secretNumber;
	}
	//Void method to generate a new secret number
	public void generateNewSecret()
	{
		int LowerRange = (int) Math.pow( 10, (numDigits-1));
		int UpperRange = (int) ((Math.pow( 10, numDigits)) - 1);
		int randomNumber = this.randomNumberGenerator.nextInt (UpperRange-LowerRange)+LowerRange  ;
		String secondNumber = "" + randomNumber;
		this.secretNumber=this.convertNumToDigitArray(secondNumber);
	}
	//Setter method for numDigits
	public void setNumDigits(int digits)
	{
		this.numDigits = digits;
	}
	//Void method to set secretNumber
	public void setSecretNumber(int [] secretNumber)
	{
		
		this.setNumDigits(numDigits);
		for(int i =0; i<secretNumber.length; i++)
		{
			this.secretNumber[i] = secretNumber[i];
		}
	}
}
