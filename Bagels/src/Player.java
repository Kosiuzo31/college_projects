/* 
* CSCI1301.java 
* Author: Kosi Uzodinma 
* Submission Date: April 04,2014 
* 
* Purpose: Writing a program to play a game similar to Mastermind
* 
* Statement of Academic Honesty: 
* 
* The following code represents my own work. I have neither 
* received nor given inappropriate assistance. I have not copied 
* or modified code from any source other than the course webpage 
* or the course textbook. I recognize that any unauthorized  
* assistance or plagiarism will be handled in accordance with 
* the University of Georgia's Academic Honesty Policy and the 
* policies of this course. I recognize that my work is based 
* on an assignment created by the Department of Computer 
* Science at the University of Georgia. Any publishing 
* or posting of source code for this project is strictly 
* prohibited unless you have written consent from the Department 
* of Computer Science at the University of Georgia. 
*/
import java.util.Scanner;
public class Player 
{
	//Date member for the Player class
	private String name;
	private int fastestWin;
	private int gamesCompleted;
	Scanner keyboard = new Scanner(System.in);
	//Method that ask for guess from user
	public String askForGuess()
	{
		String guess = keyboard.nextLine();
		return guess;
	}
	//Getter method for the name
	public String getName()
	{
		return this.name;
	}
	//Getter method to get fastest win
	public int getFastestWin()
	{
		return fastestWin;
	}
	//Getter method to get games completed
	public int getGamesCompleted()
	{
		return gamesCompleted;
	}
	//Mutator method for the name 
	public void setName(String newName)
	{
		this.name = newName;
	}
	// Void method to update stats of the game
	public void updateStats(int gameStats)
	{
		if ( (fastestWin>gameStats) || (fastestWin ==0) )
		{
			fastestWin=gameStats;
		}	
		gamesCompleted++;
		
	}
	
	
}
